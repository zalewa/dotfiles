;;; early-init.el -- Emacs early configuration file -*- lexical-binding: t -*-
;;; Commentary:

;; Emacs early configuration file.  Configuration items are placed here to
;; improve the startup time.
;;
;; It should be assumed this config is compatible with whatever Emacs version
;; was latest at the time of the last edit of this file.  Compatibility with
;; older Emacsen is not maintained.

;;; Code:

;;; Init optimization
(setq gc-cons-threshold most-positive-fixnum ; 2^61 bytes
      gc-cons-percentage 0.6)
(add-hook 'emacs-startup-hook
  (lambda ()
    (setq gc-cons-threshold (* 100 1024 1024)
          gc-cons-percentage 0.1)))

;;; GUI
;; Disable the frame windowing elements before they have a chance
;; at being drawn.
(push '(menu-bar-lines . 0) default-frame-alist)
(push '(tool-bar-lines . 0) default-frame-alist)
(push '(vertical-scroll-bars . nil) default-frame-alist)
(push '(background-color . "#111111") default-frame-alist) ; color from the default graphics theme
(push '(foreground-color . "#fff") default-frame-alist) ; color from the default graphics theme

(provide 'early-init)
;;; early-init.el ends here
