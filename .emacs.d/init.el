;;; init.el -- Emacs configuration file -*- lexical-binding: t -*-
;;; Commentary:

;; Emacs configuration file.  It's meant to bootstrap a fresh Emacs installation
;; and may require a functioning Internet connection to do so.
;;
;; It should be assumed this config is compatible with whatever Emacs version
;; was latest at the time of the last edit of this file.  Compatibility with
;; older Emacsen is not maintained.
;;
;; This config file is compatible with Windows and Linux Operating Systems.
;; The enabled features may differ between the systems due to their own
;; limitations or the inconvenience of providing the necessary 3rd-party
;; dependencies.
;;
;; Local (per setup) modifications can be placed in `lisp/init-local.el` file.

;;; Code:

;;; Clear the appearance properties set by early-init.
;; Otherwise they may override the theme, e.g. on (make-frame).
(dolist (key '(background-color foreground-color))
  (setq default-frame-alist (assq-delete-all key default-frame-alist)))

;;; Package loading
;; Configure the package load path.
(add-to-list 'load-path (expand-file-name "lisp" user-emacs-directory))

;; Setup the package archives and use-package.
(require 'package)
(defun my/add-package-archive (name url priority)
  "Add a package archive NAME under the URL with a PRIORITY."
  (add-to-list 'package-archives (cons name url) t)
  (add-to-list 'package-archive-priorities (cons name priority) t))

(my/add-package-archive "melpa" "https://melpa.org/packages/" 0)
(my/add-package-archive "melpa-stable" "https://stable.melpa.org/packages/" 10)

;; Pin by default to melpa-stable unless a package says otherwise.
(setq use-package-always-pin "melpa-stable")

(if (not (package-installed-p 'use-package))
    (progn
      (package-refresh-contents)
      (package-install 'use-package)))

(eval-when-compile
  (require 'use-package))

;;; benchmark-init
(when (equal (getenv "EMACS_BENCHMARK") "y")
  (use-package benchmark-init
    :ensure t
    :config
    ;; To disable collection of benchmark data after init is done.
    (add-hook 'after-init-hook #'benchmark-init/deactivate)))

;;; Mode switchers
(defun my/turn-off-line-numbers-mode ()
  "Unconditionally turn off the `display-line-numbers-mode'."
  (display-line-numbers-mode -1))

(defun my/turn-off-whitespace-mode ()
  "Hide the whitespace shown by `whitespace-mode'.

This is done by clearing the `whitespace-style', essentially
turning the mode off."
  (setq-local whitespace-style nil))

;;; Packages
(use-package ag
  :ensure t
  :defer t)
(use-package avy
  ;; Jump to line, word, character or symbol by interactive on-screen coordinates.
  :ensure t
  :bind (("C-c j c" . avy-goto-char)
         ("C-c j l" . avy-goto-line)
         ("C-c j s" . avy-goto-symbol-1)
         ("C-c j w" . avy-goto-subword-1)))
(use-package dired
  :hook ((dired-mode . my/turn-off-line-numbers-mode)
         (dired-mode . my/turn-off-whitespace-mode)))
(use-package diminish :ensure t)
(use-package doc-view
  :init
  :hook (doc-view-mode . my/turn-off-line-numbers-mode))
(use-package dtrt-indent
  :ensure t
  :diminish dtrt-indent-mode
  :config
  (dtrt-indent-global-mode 1)
  (add-to-list 'dtrt-indent-hook-mapping-list
               '(web-mode javascript web-mode-code-indent-offset)))
(use-package eldoc :diminish eldoc-mode)
(use-package expand-region
  :ensure t
  :bind (("C-=" . er/expand-region)
         ("C-+" . er/contract-region)))
(use-package helm
  :ensure t
  :bind (("C-c f" . helm-occur)
         ("C-c g" . helm-imenu)
         ("C-h f" . describe-function)
         ([remap find-file] . helm-find-files)
         ([remap execute-extended-command] . helm-M-x)
         ([remap switch-to-buffer] . helm-mini))
  :config
  ;; Doing (helm-mode) in :config has an unfortunate side-effect where
  ;; helm-mode is not initialized until helm is used in some way
  ;; explicitly. C-h f will not open helm until another keybind such as C-x
  ;; f is used first. This can be solved by moving (helm-mode) to :init.
  (helm-mode 1))
(use-package highlight-symbol
  :ensure t
  :diminish highlight-symbol-mode)
(use-package image-mode
  :hook (image-mode . my/turn-off-line-numbers-mode))
(use-package media-thumbnail
  :ensure t
  :commands (media-thumbnail-dired-mode)
  :diminish media-thumbnail-dired-mode
  :hook (dired-mode . media-thumbnail-dired-mode)
  :pin "melpa"
  :when (window-system)
  :init
  (make-directory (locate-user-emacs-file ".cache/media-thumbnail/") t)
  :config
  (setq media-thumbnail-image-width 16
        media-thumbnail-cache-dir (locate-user-emacs-file ".cache/media-thumbnail/")))
(use-package re-builder
  :config
  (setq reb-re-syntax 'string))
(use-package recentf
  ;; recent files
  :ensure t
  ;; get rid of `find-file-read-only' and replace it with something
  ;; more useful.
  :bind ("C-x C-r" . helm-recentf)
  :config
  (setq recentf-max-saved-items 50)
  (recentf-mode 1))
(use-package smartparens
  :ensure t
  :diminish smartparens-mode
  :config
  (smartparens-global-mode 1))
(use-package string-inflection :ensure t)
(use-package sudo-edit :ensure t :defer t)
(use-package whitespace :diminish global-whitespace-mode)
(use-package wgrep :ensure t)
(use-package wgrep-ag :ensure t)

;;; Packages -- org-mode
(use-package ox-html
  :defer t
  :config
  (setq org-html-validation-link nil  ; don't add the "Validate" link to exports
        ))
(use-package ox-latex
  :defer t
  :config
  (add-to-list 'org-latex-classes
               '("org-plain-latex"
                 "\\documentclass{article}
           [NO-DEFAULT-PACKAGES]
           [PACKAGES]
           [EXTRA]"
                 ("\\section{%s}" . "\\section*{%s}")
                 ("\\subsection{%s}" . "\\subsection*{%s}")
                 ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
                 ("\\paragraph{%s}" . "\\paragraph*{%s}")
                 ("\\subparagraph{%s}" . "\\subparagraph*{%s}")))
  (setq org-latex-src-block-backend 'minted)
  (add-to-list 'org-latex-packages-alist '("" "minted"))
  (setq org-latex-minted-options
        '(("frame" "lines")))
  (setq org-latex-pdf-process
        '("%latex -interaction nonstopmode -shell-escape -output-directory %o %f"
          "%latex -interaction nonstopmode -shell-escape -output-directory %o %f"
          "%latex -interaction nonstopmode -shell-escape -output-directory %o %f")))

;;; Packages -- IDE
(use-package company
  :ensure t
  :unless (eq system-type 'windows-nt)
  :defer t
  :diminish company-mode
  :config
  (setq company-idle-delay nil          ; disable auto-popup
        company-minimum-prefix-length 1 ; how many characters until code completion
        ))
(use-package company-quickhelp
  :ensure t
  :unless (eq system-type 'windows-nt)
  :after company
  :config
  (setq company-quickhelp-delay 0       ; show help immediately
        ))
(use-package compile
  :config
  (font-lock-add-keywords 'compilation-mode
                          '(("\\(PASSED\\|UP-TO-DATE\\)" . compilation-info-face)
                            ("FAILED" . compilation-error-face))))
(use-package dap-mode
  :ensure t
  :unless (eq system-type 'windows-nt)
  :after lsp-mode
  :config
  (require 'dap-cpptools))
(use-package flycheck
  :ensure t
  :init
  (setq flycheck-disabled-checkers '(c/c++-clang python-pylint)
        flycheck-highlighting-mode 'lines)
  (if (eq system-type 'windows-nt)
    (setq flycheck-python-flake8-executable "python"
          flycheck-python-pycompile-executable "python")
    (setq flycheck-python-flake8-executable "python3"
          flycheck-python-pycompile-executable "python3"))
  :config
  (global-flycheck-mode 1))
(use-package helm-lsp
  :ensure t
  :unless (eq system-type 'windows-nt)
  :after (helm lsp-mode))
(use-package helm-projectile
  :ensure t
  :after (helm projectile)
  :init
  (setq helm-projectile-fuzzy-match nil)
  :config
  (helm-projectile-on)
  (define-key projectile-mode-map [remap projectile-ag] nil))
(use-package lsp-java
  :ensure t
  :unless (eq system-type 'windows-nt)
  :after lsp-mode
  :config
  (setq lsp-java-jdt-download-url "https://download.eclipse.org/jdtls/milestones/1.12.0/jdt-language-server-1.12.0-202206011637.tar.gz"))
(use-package lsp-mode
  :ensure t
  :unless (eq system-type 'windows-nt)
  :hook ((c-mode . lsp)
         (c++-mode . lsp)
         (java-mode . lsp)
         (js2-mode . lsp)
         (kotlin-mode . lsp)
         (python-mode . lsp)
         (typescript-mode . lsp)
         (web-mode . lsp))
  :init
  (setq lsp-keymap-prefix "C-c l") ; must happen before lsp-mode is loaded
  :config
  (company-mode 1)
  (company-quickhelp-mode 1)
  (yas-global-mode)
  (setq lsp-enable-on-type-formatting nil ; disable the auto-messing-up feature
        lsp-file-watch-threshold 20000
        lsp-idle-delay 0.1
        lsp-headerline-breadcrumb-enable t
        )
  (define-key lsp-mode-map (kbd "C-c SPC") #'company-capf) ; auto-complete popup
  (unless window-system
    (setq lsp-ui-doc-enable nil)))
(use-package lsp-treemacs
  :ensure t
  :unless (eq system-type 'windows-nt)
  :after (lsp-mode treemacs)
  :pin "melpa")
(use-package lsp-ui
  :ensure t
  :unless (eq system-type 'windows-nt)
  :after lsp-mode)
(use-package magit
  :ensure t
  :defer t
  :hook (magit-section-mode . my/turn-off-whitespace-mode)
  :config
  (setq magit-delete-by-moving-to-trash nil))
(use-package projectile
  :ensure t
  :bind-keymap ("C-c p" . projectile-command-map)
  :bind (("<f5>" . projectile-compile-project)
         ("<f6>" . projectile-test-project)
         ("<f7>" . projectile-run-project))
  :init
  (defvar savehist-additional-variables)
  (add-hook 'savehist-mode-hook
            (lambda nil
              (add-to-list 'savehist-additional-variables 'projectile-project-command-history)))
  :config
  (require 'helm)
  (setq projectile-completion-system 'helm
        projectile-enable-caching t)
  (projectile-mode 1))
(use-package pyvenv :ensure t :defer t)
(use-package treemacs
  :ensure t
  :defer t
  :hook (treemacs-mode . (lambda ()
                          (when (display-graphic-p)
                            (text-scale-adjust -4))))
  :bind (("C-x t t" . treemacs-select-window))
  :config
  (setq treemacs-is-never-other-window t
        treemacs-space-between-root-nodes nil
        treemacs-width 20)
  (treemacs-resize-icons 10))
(use-package yasnippet
  :ensure t
  :unless (eq system-type 'windows-nt)
  :diminish yas-minor-mode
  :defer t)

;;; Packages -- coding languages modes
(use-package cmake-mode
  :ensure t
  :defer t
  :config
  (setq-default cmake-tab-width 4))
(use-package docker-compose-mode :ensure t :defer t)
(use-package dockerfile-mode :ensure t :defer t)
(use-package lisp-mode
  :commands emacs-lisp-mode
  :config
  (add-hook 'emacs-lisp-mode-hook
    (lambda()
      (setq indent-tabs-mode nil))))
(use-package enh-ruby-mode
  :ensure t
  :mode "\\.rb$"
  :interpreter "ruby"
  :config
  (setq enh-ruby-deep-indent-paren nil))
(use-package glsl-mode :ensure t :defer t :pin "melpa")
(use-package groovy-mode :ensure t :defer t)
(use-package js2-mode
  :ensure t
  :mode "\\.js\\'"
  :config
  (setq js2-bounce-indent-p t))
(use-package kotlin-mode
  :ensure t
  :defer t
  :pin "melpa"
  :hook (kotlin-mode . (lambda()
                         (setq indent-tabs-mode nil))))
(use-package less-css-mode :ensure t :defer t)
(use-package lua-mode
  :ensure t
  :defer t
  :hook (lua-mode . (lambda()
                      (setq indent-tabs-mode nil))))
(use-package mhtml-mode
  :defer t
  :config
  (add-hook 'mhtml-mode-hook
    (lambda()
      (setq tab-width 2
            indent-tabs-mode nil
            ))))
(use-package php-mode :ensure t :disabled t)
(use-package tide
  :ensure t
  :init
  (defun my/setup-tide-mode ()
    (tide-setup)
    (tide-hl-identifier-mode 1)
    (flycheck-add-mode 'typescript-tslint 'web-mode))
  :hook ((typescript-mode . my/setup-tide-mode)
         (web-mode . (lambda ()
                       (when (string-equal "tsx" (file-name-extension buffer-file-name))
                         (my/setup-tide-mode))))))
(use-package web-mode
  :ensure t
  :mode "\\.tsx\\'"
  :config
  (whitespace-mode 1)
  (setq web-mode-enable-auto-indentation nil))
(use-package typescript-mode :ensure t :defer t)
(use-package yaml-mode
  :ensure t
  :defer t)

;;; General settings
;; no more BEEP! on Windows
(if (eq system-type 'windows-nt)
    (set-message-beep 'silent))
(global-display-line-numbers-mode 1)
(global-whitespace-mode 1)
(savehist-mode 1)
(show-paren-mode 1)
(winner-mode 1)
(prefer-coding-system 'utf-8)
(setq-default frame-resize-pixelwise t
              fill-column 80
              tab-width 4)

(setq create-lockfiles nil
      dired-listing-switches "-agGh --group-directories-first"
      make-backup-files nil
      read-process-output-max (* 1024 1024)
      )

;;; Key bindings
(windmove-default-keybindings)

;;; Custom faces
(defvar font-lock-operator-face 'font-lock-operator-face)

(defface font-lock-operator-face
  ()
  "Used for operators."
  :group 'font-lock-faces)

(defvar font-lock-end-statement-face 'font-lock-end-statement-face)

(defface font-lock-end-statement-face
  ()
  "Used for end statement symbols."
  :group 'font-lock-faces)

(defvar font-lock-operator-keywords
  '(("\\([][|.+=&/%*,<>(){}?:^~-]+\\)" 1 font-lock-operator-face)
    ("!" 0 font-lock-negation-char-face)
    (";" 0 font-lock-end-statement-face)))


;;; Initial window position and size in graphical environment
(if (display-graphic-p)
    (let ((workarea-x (nth 1 (assq 'workarea (frame-monitor-attributes))))
          (workarea-y (nth 2 (assq 'workarea (frame-monitor-attributes)))))
      (setq initial-frame-alist
            (append `((width . 106) ; chars
                      (height . 35) ; lines
                      (left . ,workarea-x)
                      (top . ,workarea-y))
                    initial-frame-alist))
      (setq default-frame-alist
            (append `((width . 106)
                      (height . 35)
                      (left . ,workarea-x)
                      (top . ,workarea-y))
                    default-frame-alist))))

;;; emacsclient
;; Don't ask to kill buffer when launched from emacsclient.
(defun my/server-remove-kill-buffer-hook ()
  (remove-hook 'kill-buffer-query-functions #'server-kill-buffer-query-function))

(add-hook 'server-visit-hook #'my/server-remove-kill-buffer-hook)

;;; Copy file path functions
(defun my/get-current-buffer-file-path ()
  (if (equal major-mode 'dired-mode)
      default-directory
    (buffer-file-name)))

(defun my/copy-file-path ()
  (interactive)
  (kill-new (my/get-current-buffer-file-path)))

(defun my/copy-dir-path ()
  (interactive)
  (kill-new (file-name-directory (my/get-current-buffer-file-path))))

;;; c-mode
(c-add-style "zalewa"
             '("stroustrup"
               (tab-width . 4)
               (c-basic-offset . 4)
               (indent-tabs-mode . t)
               (c-offsets-alist
                (inline-open . 0)
                (arglist-cont-nonempty . +)
                (template-args-cont . +)
                (stream-op . +)
                (innamespace . 0)
                (inlambda . 0)
                )))
(c-add-style "doomseeker"
             '("zalewa"
               (indent-tabs-mode . t)
               ))
(setq c-default-style "zalewa")

(defun my/c-mode-common-hook ()
 (local-set-key (kbd "C-c o") 'ff-find-other-file)
 (font-lock-add-keywords nil font-lock-operator-keywords t))

(add-hook 'c-mode-common-hook #'my/c-mode-common-hook)

;;; org-mode
;; Generate predictable anchor IDs for the headlines
(defun my/org-add-predictable-ids-to-headlines ()
  "Add CUSTOM_ID properties to all headlines in the current buffer."
  (interactive)
  (org-map-entries
   (lambda ()
     (let ((title (nth 4 (org-heading-components))))
       (when title
         (unless (org-entry-get nil "CUSTOM_ID")
           (org-set-property "CUSTOM_ID"
                             (replace-regexp-in-string " " "-" (downcase title)))))))))

(defun my/org-export-add-predictable-ids (backend)
  "Add CUSTOM_ID properties to headlines before exporting."
  (my/org-add-predictable-ids-to-headlines)
  ;; Continue with the export process
  nil)

(add-hook 'org-export-before-processing-functions #'my/org-export-add-predictable-ids)

;;; python-mode
(font-lock-add-keywords 'python-mode font-lock-operator-keywords t)

;;; php-mode
;; Load the php-imenu index function
(autoload 'php-imenu-create-index "php-imenu" nil t)
;; Add the index creation function to the php-mode-hook
(defun my/php-imenu-setup ()
  (setq imenu-create-index-function (function php-imenu-create-index))
  ;; uncomment if you prefer speedbar:
  ;(setq php-imenu-alist-postprocessor (function reverse))
  (imenu-add-menubar-index))

(defun my/php-mode-setup ()
  (my/php-imenu-setup))

(add-hook 'php-mode-hook #'my/php-mode-setup)

;;; Project-specific settings
(defun my/doomseeker-style ()
  (c-set-style "doomseeker"))

(defun my/style-selector ()
  (let ((case-fold-search t))
    (cond ((eq buffer-file-name nil) nil)
          ((string-match "doomseeker" buffer-file-name) (my/doomseeker-style))
          ((string-match "doomlister" buffer-file-name) (my/doomseeker-style)) )))

(defun my/prog-mode-setup ()
  (my/style-selector)
  (highlight-symbol-mode 1))

(add-hook 'prog-mode-hook #'my/prog-mode-setup)

;;; Appearance - fonts and themes
(cond
 ((find-font (font-spec :name "Source Code Pro"))
  (add-to-list 'default-frame-alist '(font . "Source Code Pro-13")))
 ((find-font (font-spec :name "DejaVu Sans Mono"))
  (add-to-list 'default-frame-alist '(font . "DejaVu Sans Mono-15"))))

(load-theme 'zalewa-dark t (not (display-graphic-p)))
(load-theme 'zalewa-borland t (display-graphic-p))
(load-theme 'zalewa-green t t)
(load-theme 'zalewa-light t t)
(load-theme 'zalewa-red t t)


(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(column-number-mode t)
 '(compilation-always-kill t)
 '(compilation-scroll-output 'first-error)
 '(compilation-skip-threshold 2)
 '(dired-dwim-target t)
 '(highlight-symbol-idle-delay 0.5)
 '(nxml-child-indent 4)
 '(nxml-outline-child-indent 4)
 '(php-mode-coding-style 'default)
 '(safe-local-variable-values
   '((encoding . utf-8)))
 '(whitespace-style
   '(face tabs spaces trailing space-before-tab newline indentation empty space-after-tab space-mark tab-mark newline-mark)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

(load "init-local" t)

(provide 'init)
;;; init.el ends here
