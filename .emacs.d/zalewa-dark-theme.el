(deftheme zalewa-dark
  "Created 2019-12-17.")

(custom-theme-set-faces
 'zalewa-dark
 '(default ((t (:inherit nil :stipple nil :background "#111111" :foreground "white" :inverse-video nil :box nil :strike-through nil :overline nil :underline nil :slant normal :weight normal :width normal))))
 '(cursor ((t (:background "white"))))
 '(escape-glyph ((t (:foreground "dark slate gray"))))
 '(font-lock-comment-face ((t (:foreground "chocolate1" :slant italic))))
 '(font-lock-constant-face ((t (:foreground "magenta"))))
 '(font-lock-end-statement-face ((t (:foreground "orange red"))))
 '(font-lock-keyword-face ((t (:foreground "yellow" :weight bold))))
 '(font-lock-negation-char-face ((t (:foreground "dark orange"))))
 '(font-lock-operator ((t (:foreground "black"))))
 '(font-lock-operator-face ((t (:foreground "dark orange"))))
 '(font-lock-string-face ((t (:foreground "lime green"))))
 '(font-lock-type-face ((t (:foreground "orange" :weight bold))))
 '(font-lock-variable-name-face ((t (:foreground "LightGoldenrod"))))
 '(highlight-symbol-face ((t (:background "gray24"))))
 '(region ((t (:background "blue3"))))
 '(show-paren-match ((t (:background "dark violet"))))
 '(whitespace-indentation ((t (:foreground "dark slate gray"))))
 '(whitespace-newline ((t (:foreground "dark slate gray" :weight normal))))
 '(whitespace-space ((t (:foreground "dark slate gray"))))
 '(whitespace-tab ((t (:foreground "dark slate gray")))))

(provide-theme 'zalewa-dark)
