(deftheme zalewa-green
  "Created 2020-05-02.")

(custom-theme-set-faces
 'zalewa-green
 '(cursor ((t (:background "white"))))
 '(escape-glyph ((t (:foreground "dark slate gray"))))
 '(font-lock-comment-face ((t (:foreground "cadet blue" :slant italic))))
 '(font-lock-constant-face ((t (:foreground "lime green"))))
 '(font-lock-end-statement-face ((t (:foreground "orange red"))))
 '(font-lock-keyword-face ((t (:foreground "PaleGreen2" :weight bold))))
 '(font-lock-negation-char-face ((t (:foreground "SpringGreen1"))))
 '(font-lock-operator-face ((t (:foreground "light goldenrod"))))
 '(font-lock-string-face ((t (:foreground "green"))))
 '(font-lock-type-face ((t (:foreground "PaleGreen1" :weight bold))))
 '(font-lock-variable-name-face ((t (:foreground "LightGoldenrod"))))
 '(highlight-symbol-face ((t (:background "gray24"))))
 '(region ((t (:background "blue3"))))
 '(show-paren-match ((t (:background "dark violet"))))
 '(whitespace-indentation ((t (:foreground "grey36"))))
 '(whitespace-newline ((t (:foreground "grey36" :weight normal))))
 '(whitespace-space ((t (:foreground "grey36"))))
 '(whitespace-tab ((t (:foreground "grey36"))))
 '(default ((t (:inherit nil :stipple nil :background "#002a11" :foreground "spring green" :inverse-video nil :box nil :strike-through nil :overline nil :underline nil :slant normal :weight normal :width normal)))))

(provide-theme 'zalewa-green)
