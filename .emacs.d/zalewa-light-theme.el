(deftheme zalewa-light
  "Created 2019-12-17.")

(custom-theme-set-faces
 'zalewa-light
 '(default ((t (:inherit nil :stipple nil :background "antique white" :foreground "gray10" :inverse-video nil :box nil :strike-through nil :overline nil :underline nil :slant normal :weight normal :width normal))))
 '(cursor ((t (:background "deep sky blue"))))
 '(escape-glyph ((t (:foreground "light gray"))))
 '(font-lock-comment-face ((t (:foreground "gray48" :slant italic))))
 '(font-lock-constant-face ((t (:foreground "blue" :weight bold))))
 '(font-lock-end-statement-face ((t (:foreground "orange red"))))
 '(font-lock-keyword-face ((t (:foreground "firebrick" :weight bold))))
 '(font-lock-negation-char-face ((t (:foreground "red"))))
 '(font-lock-operator ((t (:foreground "blue"))))
 '(font-lock-operator-face ((t (:foreground "red"))))
 '(font-lock-string-face ((t (:foreground "forest green"))))
 '(font-lock-type-face ((t (:foreground "black" :weight bold))))
 '(font-lock-variable-name-face ((t (:foreground "gray10"))))
 '(highlight-symbol-face ((t (:background "gray88"))))
 '(region ((t (:background "cyan"))))
 '(show-paren-match ((t (:background "wheat"))))
 '(whitespace-indentation ((t (:foreground "light gray"))))
 '(whitespace-newline ((t (:foreground "light gray" :weight normal))))
 '(whitespace-space ((t (:foreground "light gray"))))
 '(whitespace-tab ((t (:foreground "light gray")))))

(provide-theme 'zalewa-light)
