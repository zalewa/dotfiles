(deftheme zalewa-red
  "Created 2021-09-08.")

(custom-theme-set-faces
 'zalewa-red
 '(cursor ((t (:background "orange"))))
 '(escape-glyph ((t (:foreground "dark slate gray"))))
 '(font-lock-comment-face ((t (:foreground "cadet blue" :slant italic))))
 '(font-lock-constant-face ((t (:foreground "gold"))))
 '(font-lock-end-statement-face ((t (:foreground "orange red"))))
 '(font-lock-keyword-face ((t (:foreground "light salmon" :weight bold))))
 '(font-lock-negation-char-face ((t (:foreground "light salmon" :weight bold))))
 '(font-lock-operator-face ((t (:foreground "light salmon"))))
 '(font-lock-string-face ((t (:foreground "orange"))))
 '(font-lock-type-face ((t (:foreground "yellow" :weight bold))))
 '(font-lock-variable-name-face ((t (:foreground "LightGoldenrod"))))
 '(highlight-symbol-face ((t (:background "#800"))))
 '(region ((t (:background "dark blue"))))
 '(show-paren-match ((t (:background "dark violet"))))
 '(whitespace-indentation ((t (:foreground "red4"))))
 '(whitespace-newline ((t (:foreground "red4"))))
 '(whitespace-space ((t (:foreground "red4"))))
 '(whitespace-tab ((t (:foreground "red4"))))
 '(default ((t (:inherit nil :stipple nil :background "#400000" :foreground "#ff1010" :inverse-video nil :box nil :strike-through nil :overline nil :underline nil :slant normal :weight normal :width normal))))
 '(font-lock-function-name-face ((t (:foreground "sandy brown")))))

(provide-theme 'zalewa-red)
