#!/usr/bin/python
import sys

def avg(l):
	try:
		return sum(l) / float(len(l))
	except Exception:
		return None


def median(l):
	try:
		l_new = list(l)
		l_new.sort()
		return l_new[len(l_new)/2]
	except Exception:
		return None


filepath = sys.argv[1]
lengths = []
with open(filepath, "r") as f:
	for l in f:
		lengths.append(len(l))
lengths.sort()

print "Number of lines: ", len(lengths)
print "Average length: ", avg(lengths)
print "Median: ", median(lengths)
print "Shortest line: ", lengths[0]
print "Longest line: ", lengths[-1]