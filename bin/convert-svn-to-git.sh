#!/bin/bash

# svn2git:
# https://github.com/nirvdrum/svn2git

URL=http://svn.example.org/
USERNAME=$USER

set -e

rm -rf svn2git_repo svn2git_repo.git
mkdir svn2git_repo

cd svn2git_repo
echo '[SCRIPT] svn2git'
svn2git "$URL" --username "$USERNAME" --no-minimize-url -v --tags stables --authors ../authors.txt
cd ..
echo '[SCRIPT] git clone --bare'
git clone --bare --no-hardlinks svn2git_repo svn2git_repo.git
cd svn2git_repo.git
echo '[SCRIPT] git filter-branch'
git filter-branch -f --index-filter "git diff-tree --diff-filter=D --name-only -r -z $GIT_COMMIT..HEAD | xargs -0 --no-run-if- git rm -rf --cached --ignore-unmatch" --prune-empty -d /dev/shm/dh -- HEAD
echo '[SCRIPT] rm -rf ./refs/original'
rm -rf ./refs/original/
echo '[SCRIPT] git reflog expire'
git reflog expire --all --expire=now
echo '[SCRIPT] git repack -ad'
git repack -ad
echo '[SCRIPT] git gc'
git gc --aggressive --prune=now
