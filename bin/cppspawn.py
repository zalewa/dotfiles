#!/usr/bin/python
import datetime
import uuid
import sys

classname = sys.argv[1]
fname = sys.argv[1].lower()
gword = "id{0}".format(str(uuid.uuid4()).replace("-", "_"))

tabs = False
gpl = False
lgpl = False
dtemplate = False
header_ext = "h"
for arg in sys.argv[2:]:
    if arg == "t":
        tabs = True
    elif arg == "gpl":
        gpl = True
    elif arg == "lgpl":
        lgpl = True
    elif arg == "hpp":
        header_ext = "hpp"
    elif arg == "doomseeker":
        lgpl = True
        tabs = True
        dtemplate = True

if gpl and lgpl:
    raise Exception("lgpl and gpl cannot be both enabled")


def indent(level=1):
    global tabs
    if tabs:
        return "\t" * level
    else:
        return " " * (4 * level)


def gpl_header(filename):
    header = """
//------------------------------------------------------------------------------
// {0}
//------------------------------------------------------------------------------
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
// 02110-1301, USA.
//
//------------------------------------------------------------------------------
// Copyright (C) {1} "Zalewa" <zalewapl@gmail.com>
//------------------------------------------------------------------------------
"""
    return header.format(filename, _year()).lstrip()


def lgpl_header(filename):
    header = """
//------------------------------------------------------------------------------
// {0}
//------------------------------------------------------------------------------
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
// 02110-1301  USA
//
//------------------------------------------------------------------------------
// Copyright (C) {1} "Zalewa" <zalewapl@gmail.com>
//------------------------------------------------------------------------------
"""
    return header.format(filename, _year()).lstrip()


def _year():
    return datetime.datetime.now().year


def privdata_header(classname):
    if dtemplate:
        return "{0}DPtr<{1}> d;\n".format(indent(), classname)
    else:
        return "{0}class PrivData;\n" \
               "{0}PrivData *d;\n".format(indent())


def privdata_pointer_cpp(f, classname):
    f.write("class {0}::PrivData\n".format(classname))
    f.write("{\n")
    f.write("public:\n".format(indent()))
    f.write("};\n")
    f.write("\n\n")
    f.write("{0}::{0}()\n".format(classname))
    f.write("{\n")
    f.write("{0}d = new PrivData();\n".format(indent()))
    f.write("}\n")
    f.write("\n")
    f.write("{0}::~{0}()\n".format(classname))
    f.write("{\n")
    f.write("{0}delete d;\n".format(indent()))
    f.write("}\n")


def privdata_dtemplate_cpp(f, classname):
    f.write("DClass<{0}>\n".format(classname))
    f.write("{\n")
    f.write("public:\n".format(indent()))
    f.write("};\n")
    f.write("DPointered({0})\n".format(classname))
    f.write("\n")
    f.write("{0}::{0}()\n".format(classname))
    f.write("{\n")
    f.write("}\n")
    f.write("\n")
    f.write("{0}::~{0}()\n".format(classname))
    f.write("{\n")
    f.write("}\n")

header_name = "{}.{}".format(fname, header_ext)
with open(header_name, "w") as f:
    if gpl:
        f.write(gpl_header(header_name))
    if lgpl:
        f.write(lgpl_header(header_name))
    f.write("#ifndef {0}\n".format(gword))
    f.write("#define {0}\n".format(gword))
    f.write("\n")
    if dtemplate:
        f.write('#include "dptr.h"\n')
        f.write('\n')
    f.write("class {0}\n".format(classname))
    f.write("{\n")
    f.write("public:\n")
    f.write("{0}{1}();\n" \
            "{0}~{1}();\n" \
            "\n" \
            "private:\n".format(indent(), classname))
    f.write(privdata_header(classname))
    f.write("};\n")
    f.write("\n")
    f.write("#endif\n")


with open("{0}.cpp".format(fname), "w") as f:
    if gpl:
        f.write(gpl_header("{0}.cpp".format(fname)))
    if lgpl:
        f.write(lgpl_header("{0}.cpp").format(fname))
    f.write('#include "{0}"\n'.format(header_name))
    f.write("\n")
    if dtemplate:
        privdata_dtemplate_cpp(f, classname)
    else:
        privdata_pointer_cpp(f, classname)
