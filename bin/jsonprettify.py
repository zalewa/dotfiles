import json
import sys

with open(sys.argv[1], "r+b") as f:
	print json.dumps(json.loads(f.read()), indent=2)
