#!/usr/bin/python
import sys

tokens = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. " \
         "Curabitur accumsan tellus nec maximus gravida. Integer " \
         "porttitor ipsum eu eros egestas dignissim. Nullam luctus " \
         "fermentum ligula eget sodales. Ut ultrices magna eleifend, " \
         "porttitor ligula et, lacinia sem. Nam metus erat, malesuada " \
         "vel augue in, vulputate consequat metus. Aliquam consequat, " \
         "sapien a egestas mollis, elit eros sollicitudin felis, vitae " \
         "viverra orci mi non nunc. Nulla iaculis molestie lacus."
tokens = tokens.split(" ")

lines = int(sys.argv[1])
words_per_line = 10
try:
    words_per_line = int(sys.argv[2])
except Exception:
    pass


idx_word = 0
for _ in xrange(lines):
    remaining_words = words_per_line
    words = []
    while len(words) < words_per_line:
        words.extend(tokens[idx_word:idx_word + remaining_words])
        if len(words) < words_per_line:
            idx_word = 0
            remaining_words = words_per_line - len(words)
        else:
            idx_word += remaining_words
    print " ".join(words)
