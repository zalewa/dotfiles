#!/usr/bin/env python3
"""Get pictures from Reddit conveniently and unstupidly.

Plebbit (Reddit) doesn't want to give direct image links to WWW browser
user-agents anymore, so it's difficult to right-click on them and "Save"
or "Open image in new tab". Instead, it always opens its own preview
page and offer a crappy, low-quality preview WEBP picture instead of
whatever the user has actually uploaded.

The link to the image on the plebbit's page is usually some crap like this:

  https://preview.redd.it/pepper-says-hello-v0-ybluwvpoyvob1.jpg?width=1080&crop=smart&auto=webp&s=972b42e6da7fe2629d4c0dbed4a1002bde8cf22b

But the actual, original image can still be retrieved from this link:

  https://i.redd.it/ybluwvpoyvob1.jpg

(try to wget it)

This script converts the crap link to the good link and downloads the image.

"""
import argparse
import logging
import urllib.request
from urllib.parse import urlparse


def parse_args():
    argp = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.RawTextHelpFormatter)
    argp.add_argument("urls", nargs="+", help="Link to Plebbit media")
    return argp.parse_args()


def _setup_main_logger():
    format_tmpl = "%(asctime)s - %(levelname)-8s - %(name)s - %(message)s"
    formatter = logging.Formatter(format_tmpl)

    handler = logging.StreamHandler()
    handler.setLevel(logging.DEBUG)
    handler.setFormatter(formatter)

    root_logger = logging.getLogger()
    root_logger.addHandler(handler)
    root_logger.setLevel(logging.DEBUG)

    return logging.getLogger("plebbit")


def main():
    logger = _setup_main_logger()
    args = parse_args()
    nbad = 0
    for url in args.urls:
        logger.debug("processing %s ...", url)
        parsed_url = urlparse(url)
        if parsed_url.netloc.lower() not in ("preview.redd.it", "i.redd.it"):
            logger.warning("not a plebbit url, skipping: %s", url)
            nbad += 1
            continue

        filename = parsed_url.path[parsed_url.path.rindex("/")+1:]
        if parsed_url.netloc == "preview.redd.it":
            filename_parts = filename.split("-")
            file_id = filename_parts[-1]
            # fuck the -v0- crap
            filename_parts = filename_parts[:-2]
            filename = "-".join(filename_parts) + f"-{file_id}"
            download_url = f"https://i.redd.it/{file_id}"
        else:
            download_url = url
        logger.debug("GET %s -> %s ...", download_url, filename)
        urllib.request.urlretrieve(download_url, filename)
    if nbad > 0:
        logger.error("failed to download %s URLs", nbad)
    return 1 if nbad == len(args.urls) else 0


if __name__ == "__main__":
    exit(main())
