#!/usr/bin/env python
import string
import sys

if len(sys.argv) <= 1:
    print "Usage: {0} <file> [file] ...".format(sys.argv[0])
    exit(1)
for fpath in sys.argv[1:]:
    lines = []
    with open(fpath, "r") as f:
        for line in f:
            stripped = string.rstrip(line)
            if line.endswith("\r\n"):
                stripped += "\r\n"
            elif line.endswith("\n"):
                stripped += "\n"
            elif line.endswith("\r"):
                stripped += "\r"
            lines.append(stripped)
    with open(fpath, "w") as f:
        f.write("".join(lines))
