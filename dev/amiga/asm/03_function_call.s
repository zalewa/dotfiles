main:
	clr.l d0
	clr.l d1
	clr.l d2
	move.l sp,d0
	bsr sub1
	move.l #$fd,d5
	rts

sub1:
	move.l sp,d1
	bsr sub2
	move.l #$fe,d4
	rts

sub2:
	move.l sp,d2
	move.l #$ff,d3
	rts
	
