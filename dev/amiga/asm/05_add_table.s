main:
	clr.l d0
	bsr subtract
	rts

adding1:
	move.w table,d0
	add.w table+2,d0
	add.w table+4,d0
	add.w table+6,d0
	add.w table+8,d0
	rts

adding2:
	move.l #table,a0
	move.w (a0),d0
	add.w 2(a0),d0
	add.w 4(a0),d0
	add.w 6(a0),d0
	add.w 8(a0),d0
	rts

adding3:
	move.l #table,a0
	move #5,d1	; number of entries
	.loop:
		add (a0)+,d0
		subq #1,d1
		bne .loop
	rts

subtract:
	move.l #sub_table,a0
	move.w sub_table_len,d1
	move.w (a0)+,d0

	; end right away if there's only one number
	subq #1,d1
	beq .end
	
	.loop:
		sub (a0)+,d0
		subq #1,d1
		bne .loop
	.end:
		rts

table:
	dc.w 2,4,6,8,10

sub_table_len:
	dc.w 1
sub_table:
	dc.w 50,8,4,6
