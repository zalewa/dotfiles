main:
	; The extra instructions here in main enable
	; checking that branch, return and stack push
	; instructions work.

	clr.l d7
	move.l #$aabbccdd,d6
	bsr.b sort_start
	move.l #$cafebabe,d7
	rts

sort_start:
	; movem.l pushes contents of registers onto the stack
	movem.l d0-d7/a0-a6,-(sp)

sort:
	; Load the address of the table to a0
	move.l #table,a0

	; Also, load the length of the table to d0
	move.w table_len,d0

	; We compare two elements of the table - the current
	; one and the next one. We need to decrement the loop
	; counter here by 1 to avoid going past the table.
	subq #1,d0

	; d1 holds the "did I sort anything?" flag
	clr d1

	.loop:
		move (a0),d3	; load value at a0 to d3
		cmp.w 2(a0),d3	; compare the next value to d3
		ble.w .skip	; don't swap if elements are in the right order
		; swap elements
		move 2(a0),(a0)
		move d3,2(a0)
		moveq #1,d1
		
	.skip:
		addq #2,a0	; go to next element
		dbra d0,.loop	; decrement loop iterator and cont. loop if >0
	tst d1		; If table order was changed in this iteration ...
	bne.b sort	; ... continue sorting.
	movem.l (sp)+,d0-d7/a0-a6	; pop registers from stack
	rts

table_len:
	dc.w 9
table:
	dc.w 3,6,9,5
	dc.w 10,8,6,4
