; Program A - Convert whole long  word
hexlong:
	lea buffer,a0
	move.l #$a1b2c3d4,d1
	move #7,d3
loop:
	rol.l #4,d1
	move d1,d2
	bsr nibble
	move.b d2,(a0)+
	dbra d3,loop
	rts


; Program B - Convert just a byte
hex_to_ascii:
	lea buffer,a0	; load output buffer address
	move #$4a,d1	; some example vaue
	bsr byte	; convert the byte to ascii
	rts

byte:
	move d1,d2
	lsr #4,d2	; shift the byte to access the first nibble
	bsr nibble	; convert it
	move.b d2,(a0)+	; save the converted value at buffer
	move d1,d2	; reload the byte to convert the second nibble
	bsr nibble	; the nibble is already at the lower 4-bits
	move.b d2,(a0)+	; save the converted value at buffer, at the next pos
	rts


; Functions
nibble:
	and #$0f,d2
	add #$30,d2	; add offset to reach ascii '0'
	cmp #$3a,d2	; are we still within '1' - '9'?
	bcs ok		; if yes, return
	add #7,d2	; if not, up it to 'A'
ok:
	rts

; Declarations
buffer:
	blk.b 9,0	; enough space for a long word + a null byte

