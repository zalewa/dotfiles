; This program loops for a while and pools the special key
; register at $bfec01 to read the key press event.
; Upon exit, the read key is in d0.

main:
	clr.l d0
	move.l #$f,d2
loop:
	move.l #$ffff,d1
loop_inner:
	move.b $bfec01,d0
	dbra d1,loop_inner
	dbra d2,loop
	rts
