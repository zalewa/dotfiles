; Program reads time once, then executes a "lengthy routine"
; and then reads the time again. The start time is subtracted
; from the end time. The result - the delta time - is in d7.

TIME_LOW = $bfe801
TIME_MID = $bfe901
TIME_HIG = $bfea01

timing:
	bsr gettime
	move.l d7,d6
	bsr routine
	bsr gettime
	move.l d7,d5
	sub.l d6,d7
	rts

routine:
	move #$ff,d1
	.loop:
		move #$ffff,d0
		.loop_inner:
			dbra d0,.loop_inner
		dbra d1,.loop
	rts

gettime:
	clr.l d7
	move.b TIME_HIG,d7
	lsl.l #8,d7
	move.b TIME_MID,d7
	lsl.l #8,d7
	move.b TIME_LOW,d7
	rts

