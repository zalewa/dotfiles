; For mouse:
; - Vertical movement is in LO byte
; - Horizontal movement is in HI byte

; The values in the ports are relative to their previous states.
; To see if something has changed, it's necessary to subtract the
; current value from the previous one and use the result.


; BUTTONS has buttons for both game ports:
;           BUTTON1 bit | BUTTON2 bit
; GAMEPORT1     6       |
; GAMEPORT2     7       |
BUTTONS = $bfe001

GAMEPORT1 = $dff00a ; JOY0DAT
GAMEPORT2 = $dff00c ; JOY1DAT



main:
	jmp main_statey


main_loopy:
	clr.l d6
	clr.l d7
loop:
	move GAMEPORT1,d6
	move GAMEPORT2,d7
	;jmp loop 	; enable this jmp only in debugger
	rts


main_statey:
	; learn the initial values so that we have something to compare against
	move GAMEPORT1,gameport1_state
	move GAMEPORT2,gameport2_state
	clr.l d7
	clr.l d6

	.statey_loop:
		bsr readmouse
		bsr readjoy
		btst #6,BUTTONS
		bne .statey_loop
	rts


readmouse:
	move GAMEPORT1,d0
	cmp gameport1_state,d0
	beq .readmouse_done
	move d0,d5
	move gameport1_state,d7
	sub d0,d7
	move d0,gameport1_state
.readmouse_done:
	rts

; As far as I can tell this methodology gives myriad
; of different values depending on the delta movement
; of the joystick's handle. It probably makes sense.
readjoy:
	move GAMEPORT2,d0
	cmp gameport2_state,d0
	beq .readjoy_done
	move d0,d4
	move gameport2_state,d6
	sub d0,d6
	move d0,gameport2_state
.readjoy_done:
	rts

gameport1_state:
	dc.w 0
gameport2_state:
	dc.w 0

	
