TOP	= $2c
BOTTOM	= $12c
HALF	= (TOP+BOTTOM)/2

BORDER_TOP = $40
BORDER_BOTTOM = $ef


CIAAPRA	= $bfe001
VHPOSR	= $dff006
COLOR00	= $dff180


main:
	; d6 - rasterline shift
	; d7 - rasterline position
	move #1,d6
	move #HALF,d7

mainloop:

waitraster1:
	cmp.b VHPOSR,d7
	bne waitraster1
	move.w linecolor,COLOR00

waitraster2:
	cmp.b VHPOSR,d7
	beq waitraster2
	move.w bg,COLOR00

	add.b d6,d7
	cmp #0,d6
	blt bounce

waitraster3:
	cmp.b VHPOSR,d7
	beq waitraster3

bounce:
	cmp.b #BORDER_TOP,d7
	blo reverse
	cmp.b #BORDER_BOTTOM,d7
	bhi reverse
	bra waitmouse

reverse:
	neg d6
	add.b d6,d7


waitmouse:
	btst #6,CIAAPRA
	bne mainloop
	rts

bg:
	dc.w $aaa
linecolor:
	dc.w $fff
