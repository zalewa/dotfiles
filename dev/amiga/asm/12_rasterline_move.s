; TOP and BOTTOM values are really here to just calculate the HALF
TOP	= $2c
BOTTOM	= $12c
HALF	= (TOP+BOTTOM)/2

; These are the values from which the lines will bounce.
BORDER_TOP	= $40
BORDER_BOTTOM	= $ef


; Hardware registers.
CIAAPRA	= $bfe001
VHPOSR	= $dff006
COLOR00	= $dff180


; Initialize the the program.
main:
	; d6 - rasterline shift
	; d7 - rasterline position
	move #1,d6
	move #HALF,d7

; We'll bounce back here after each "frame".
mainloop:

; Wait for the line to highlight
waitraster1:
	cmp.b VHPOSR,d7
	bne.b waitraster1
	move.w linecolor,COLOR00

; Highlight the line
waitraster2:
	cmp.b VHPOSR,d7
	beq.b waitraster2
	move.w bg,COLOR00

; Increase or decrease the next position of the line.
	add.b d6,d7

; If the line is going downward, it's now necessary to wait for the next
; raster line or else the raster line will match immediatelly at the next
; mainloop iteration without waiting for the full video frame to be drawn.
	cmp #0,d6
	blt.b bounce

waitraster3:
	cmp.b VHPOSR,d7
	beq.b waitraster3

; Bounce will reverse the direction of the line once it reaches
; BORDER_TOP or BORDER_BOTTOM.
bounce:
	cmp.b #BORDER_TOP,d7
	blo.b reverse
	cmp.b #BORDER_BOTTOM,d7
	bhi.b reverse
	bra.b waitmouse

reverse:
	neg d6
	add.b d6,d7


; If mouse button is pressed - quit. Otherwise restart the main loop.
waitmouse:
	btst #6,CIAAPRA
	bne.b mainloop
	rts


bg:
	dc.w $aaa
linecolor:
	dc.w $fff
