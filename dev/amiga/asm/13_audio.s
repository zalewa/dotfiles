; Audio playback example.
;
; Comments by Robikz, 2019:
;
; It will play the sample exactly once. It does so by utilizing the
; audio interrupt to detect when the sample has *started* playing and
; then immediately switching the audio registers to a silent sample.
;
; That said, this is rather nasty. The silent sample is short and it is
; still being played by the hardware. This means that each time that it
; ends, it causes the interrupt to be called. CPU will jump to this
; interrupt and waste cycles.
;
; A proposed approach that would be better and would avoid using an
; interrupt entirely would be to put AUDxxxx move instructions to a
; beginning of a copperlist, poke the audio values there whenever
; necessary and just let the copper poke them.
;
; Another alternative is to poke the values from CPU at VBLANK and
; then don't poke new values until the next VBLANK. This however
; means that a sample cannot be shorter than 20ms.
;
; Between VBLANK and the top of the screen there are $2c scanlines
; that can be wasted. We can poke the sounds, wait 4 scanlines,
; then poke nullsounds.
;
; See here: http://eab.abime.net/showthread.php?t=16265

; Calculation knowledge.
; Source: http://www.winnicki.net/amiga/memmap/AUDxPER.html
; Samples:
;
;                 3579546
; AUDxPER = --------------------
;            Samples-per-second
;
; Notes:
;
;                          3579546
; AUDxPER =  -------------------------------------
;            Bytes-Length * Frequency to be played
;
; Length:
;
;     uSec = .279365 * Period * Length

CIAAPRA = $bfe001

DMACON = $dff096
DMACONR = $dff002
INTENA = $dff09a
INTENAR = $dff01c
INTREQ = $dff09c
INTREQR = $dff01e

AUD0LCH = $dff0a0
AUD0LEN = $dff0a4
AUD0PER = $dff0a6
AUD0VOL = $dff0a8


main:
	move.l 4.w,a6			;Exec library base address in a6
	sub.l a4,a4
	btst #0,297(a6)			;68000 CPU? (AttnFlags+1)
	beq.s .yes68k
	lea GetVBR(PC),a5		;else fetch vector base address to a4
	jsr -30(a6)			;enter Supervisor mode
.yes68k:
	; Store and off system
	move DMACONR,old_dmacon
	move INTENAR,old_intena
	move #$7fff,INTENA
	move #$7fff,INTREQ
	move #$7fff,INTREQ
	move #$7fff,DMACON
	move #$e000,INTENA

	; Play audio
	move.l #sample,AUD0LCH
	move #(sample_e-sample)/2,AUD0LEN
	move #324,AUD0PER
	move #64,AUD0VOL
	move #$8000+$200+1,DMACON

	; Setup audio interrupt
	lea AUDint(PC),a0
	move.l $70(a4),old_interrupt
	move.l a0,$70(a4)
	move #$c080,INTENA


waitmouse:
	btst #6,CIAAPRA
	bne.b waitmouse
exit:
	move old_intena(pc),d0
	or #$8000,d0
	move d0,INTENA

	move old_dmacon(pc),d0
	or #$8000,d0
	move d0,DMACON

	move #1,DMACON

	; Old interrupt is restored after the audio DMA is turned off
	move.l old_interrupt(pc),$70(a4)

	clr.l d0

	rts

AUDint:
	btst #7,INTREQR+1
	beq .notaudio

	movem.l d0/a0,-(sp)
	move audint_called,d0
	add #1,d0
	move d0,audint_called

	move.l nullsample,AUD0LCH
	move #(nullsample_e-nullsample)/2,AUD0LEN

	move.w #$80,INTREQ
	move.w #$80,INTREQ

	movem.l (sp)+,d0/a0
.notaudio:
	rte

old_intena:
	dc.w 0
old_dmacon:
	dc.w 0
old_interrupt:
	dc.l 0
audint_called:
	dc.w 0

GetVBR:
	dc.w $4e7a,$c801		;hex for "movec VBR,a4"
	rte				;return from Supervisor mode

	SECTION data,DATA_C
squarelen:
	dc.w 1
squaretab:
	dc.b -100,100

sinelen:
	dc.w 4
sinetab:
	dc.b -40,-70,-40,0,40,70,40,0

bliplen:
	dc.w 5
bliptab:
	dc.b 40,70,90,100,90,70,40,0,-4,0

; The attached '11k' sample is a converted PC sample.
; The conversion was done by:
;	1. ffmpeg -i source.wav -ar 11025 11k.wav
;	2. Open 11k.wav in Audacity
;	3. Export as a RAW header-less, signed 8-bit PCM
sample:
	incbin "sample_11k"
sample_e:
	dc.b 0	; guard byte for uneven samples
	EVEN
nullsample:
	dc.w 0,0
nullsample_e:
