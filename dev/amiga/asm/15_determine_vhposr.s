main:
  move #$ffff,d1
  move #$0,d2

mainloop:
  move $dff005,d0

  cmp d0,d1
  blo not_min
  move d0,d1

not_min:
  cmp d0,d2
  bhi not_max
  move d0,d2

not_max:
waitmouse:
  btst #6,$bfe001
  bne mainloop
  rts
