; TOP and BOTTOM values are really here to just calculate the HALF
TOP	= $1c
BOTTOM	= $138
HALF	= (TOP+BOTTOM)/2

; These are the values from which the lines will bounce.
BORDER_TOP	= $1c
BORDER_BOTTOM	= $137

; exec.library
EXEC_BASE = 4
EXEC_OldOpenLibrary = 408
EXEC_CloseLibrary = 414

; graphics.library
GRAPHICS_COPPER_POINTER = 38


; Hardware registers.
CIAAPRA	= $bfe001
VHPOSR	= $dff006
COP1LCH = $dff080
INTENA	= $dff09a
INTENAR	= $dff01c
COLOR00	= $dff180


; Initialize the the program.
main:
	; Get graphics library and store the old copper pointer.
	move.l EXEC_BASE,a6
	clr.l d0
	move.l #gfxname,a1
	jsr -EXEC_OldOpenLibrary(a6)
	move.l d0,a1	; Move the address to the graphics.library
	move.l GRAPHICS_COPPER_POINTER(a1),d4
	jsr -EXEC_CloseLibrary(a6)

	; d4 - old copper pointer
	; d5 - INTENA placeholder
	; d6 - rasterline shift
	; d7 - rasterline position
	move VHPOSR-1,d2
	move $dff090,d3
	move INTENAR,d5
	move #1,d6
	move #HALF,d7

	move #$7fff,INTENA ; disable all interrupts
	move.l #copper,COP1LCH

; We'll bounce back here after each "frame".
mainloop:

waitraster0:
	cmp #TOP,VHPOSR-1
	bne waitraster0

	move spancolor,COLOR00

; Wait for the line to highlight
waitraster1:
	cmp VHPOSR-1,d7
	bne waitraster1

	move linecolor,COLOR00

; Highlight the line
waitraster2:
	cmp VHPOSR-1,d7
	beq waitraster2

	move bg,COLOR00

; Increase or decrease the next position of the line.
	add d6,d7

; If the line is going downward, it's now necessary to wait for the next
; raster line or else the raster line will match immediatelly at the next
; mainloop iteration without waiting for the full video frame to be drawn.
	cmp #0,d6
	blt bounce

waitraster3:
	cmp VHPOSR-1,d7
	beq waitraster3

; Bounce will reverse the direction of the line once it reaches
; BORDER_TOP or BORDER_BOTTOM.
bounce:
	cmp #BORDER_TOP,d7
	blo reverse
	cmp #BORDER_BOTTOM,d7
	bhi reverse
	bra waitmouse

reverse:
	neg d6
	add d6,d7


; If mouse button is pressed - quit. Otherwise restart the main loop.
waitmouse:
	btst #6,CIAAPRA
	bne mainloop

exit:
	move.l d4,COP1LCH
	bset #15,d5
	move d5,INTENA
	rts


bg:
	dc.w $aaa
linecolor:
	dc.w $fff
spancolor:
	dc.w $0f0

gfxname:
	dc.b "graphics.library",0


	SECTION copper,DATA_C
copper:
  ; $(dff)100 - use BPLCON0
	; $0204 - turn off all the bitplanes, 2 - keep the color burst flag on
	;         4 - set interlace mode
	; $ffff, $fffe - wait for an impossible position to stop processing
	;                the copper list
	dc.w $100,$0204
	dc.w $ffff,$fffe
