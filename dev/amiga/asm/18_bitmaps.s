; Screen size
SCREEN_WIDTH = 320
SCREEN_HEIGHT = 256
BPLSIZE = SCREEN_WIDTH*SCREEN_HEIGHT/8


; TOP and BOTTOM values are really here to just calculate the HALF
TOP	= $1c
BOTTOM	= $138
HALF	= (TOP+BOTTOM)/2

; These are the values from which the lines will bounce.
BORDER_TOP	= $59
BORDER_BOTTOM	= $fe-5

; exec.library
EXEC_BASE = 4
EXEC_OldOpenLibrary = 408
EXEC_CloseLibrary = 414

; graphics.library
GRAPHICS_COPPER_POINTER = 38


; Hardware registers.
CIAAPRA	= $bfe001

; Chipset
CS_BASE = $dff000
VHPOSR	= $dff006
COP1LCH = $dff080
INTENA	= $dff09a
INTENAR	= $dff01c
BPL1PTH = $dff0e0
BPL1PTL = $dff0e2
COLOR00	= $180
COLOR01 = $182



; Initialize the the program.
main:
	; Get graphics library and store the old copper pointer.
	move.l EXEC_BASE,a6
	clr.l d0
	move.l #gfxname,a1
	jsr -EXEC_OldOpenLibrary(a6)
	move.l d0,a1	; Move the address to the graphics.library
	move.l GRAPHICS_COPPER_POINTER(a1),d4
	jsr -EXEC_CloseLibrary(a6)

	; d4 - old copper pointer
	; d5 - INTENA placeholder
	; d6 - rasterline shift
	; d7 - rasterline position
	move INTENAR,d5
	move #1,d6
	move #HALF,d7

	move #$7fff,INTENA ; disable all interrupts
	move.l #copper,COP1LCH

	; Insert the 'screen' address to the copperlist.
	move.l #copper_bitplane,a0
	move.l #screen,d0
	move.w d0,6(a0)
	move.w #16,d1
	lsr.l d1,d0
	move.w d0,2(a0)

	lea screen,a0
	move.w #BPLSIZE-1,d0
	move.w #(320/8)-1,d1
	move.w #$aa,d2
.loop:
	;move.b VHPOSR+1,(a0)+
	move.b d2,(a0)+
	dbf d1,.noswap
	eor.b #$ff,d2
	move.w #(320/8)-1,d1
.noswap:
	dbf d0,.loop

; We'll bounce back here after each "frame".
mainloop:

waitraster0:
	cmp #TOP,VHPOSR-1
	bne waitraster0
waitraster0_end:
	cmp #TOP,VHPOSR-1
	beq waitraster0_end

	move.l #lineras1,a0
	move.l #5,d1
	move.b d7,d0
.loop:
	addi #1,d0
	move.b d0,(a0)
	add.l #8,a0
	dbf d1,.loop

; Increase or decrease the next position of the line.
	add d6,d7

; If the line is going downward, it's now necessary to wait for the next
; raster line or else the raster line will match immediatelly at the next
; mainloop iteration without waiting for the full video frame to be drawn.
	cmp #0,d6
	blt bounce

waitraster3:
	cmp VHPOSR-1,d7
	beq waitraster3

; Bounce will reverse the direction of the line once it reaches
; BORDER_TOP or BORDER_BOTTOM.
bounce:
	cmp #BORDER_TOP,d7
	blo reverse
	cmp #BORDER_BOTTOM,d7
	bhi reverse
	bra waitmouse

reverse:
	neg d6
	add d6,d7


; If mouse button is pressed - quit. Otherwise restart the main loop.
waitmouse:
	btst #6,CIAAPRA
	bne mainloop

exit:
	move.l d4,COP1LCH
	bset #15,d5
	move d5,INTENA
	rts


bg:
	dc.w $004
linecolor:
	dc.w $fff

gfxname:
	dc.b "graphics.library",0


	SECTION copper,DATA_C
copper:
	dc.w $1fc,0 ; slow fetch mode, AGA compatibility

	dc.w $100,$0204

	; Configure the display window
	dc.w $8e,$2c81
	dc.w $90,$2cc1
	dc.w $92,$38
	dc.w $94,$d0
	; Configure bitplane modulos
	dc.w $108,0
	dc.w $10a,0

	; Configure bitplane pointer
	dc.w COLOR01,$0f0
copper_bitplane:
	dc.w $e0,0
	dc.w $e2,0
	dc.w $100,$1204

	; top frame	
	dc.w COLOR00,$349
	dc.w $2b07,$fffe
	dc.w COLOR00,$56c
	dc.w $2c07,$fffe

	; center
	dc.w COLOR00,$123
lineras1:
	dc.w $8007,$fffe
	dc.w COLOR00,$055
	dc.w $8107, $fffe
	dc.w COLOR00,$0aa
	dc.w $8207, $fffe
	dc.w COLOR00,$0ff
	dc.w $8307, $fffe
	dc.w COLOR00,$0aa
	dc.w $8407, $fffe
	dc.w COLOR00,$055
	dc.w $8507, $fffe
	dc.w COLOR00,$123
	

	; bottom frame
	dc.w $ffdf,$fffe
	dc.w $2c07,$fffe
	dc.w COLOR00,$56c
	dc.w $2d07,$fffe
	dc.w COLOR00,$349
	dc.w $ffff,$fffe

	EVEN
screen:
	dcb BPLSIZE/2
