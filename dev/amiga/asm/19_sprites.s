; Screen size
SCREEN_WIDTH = 320
SCREEN_HEIGHT = 256
BPLSIZE = SCREEN_WIDTH*SCREEN_HEIGHT/8


; TOP and BOTTOM values are really here to just calculate the HALF
TOP	= $1c
BOTTOM	= $138
HALF	= (TOP+BOTTOM)/2

; These are the values from which the lines will bounce.
BORDER_TOP	= $59
BORDER_BOTTOM	= $fe-5

SPRITE_TICKRATE = 50/12 ; 1/4 of a second for a PAL Amiga


; exec.library
EXEC_BASE = 4
EXEC_OldOpenLibrary = 408
EXEC_CloseLibrary = 414

; graphics.library
GRAPHICS_COPPER_POINTER = 38


; Hardware registers.
CIAAPRA	= $bfe001

; Chipset
CS_BASE = $dff000
VPOSR	= $dff004
VHPOSR	= $dff006
COP1LCH = $dff080
DMACON	= $dff096
DMACONR	= $dff002
INTENA	= $dff09a
INTENAR	= $dff01c
INTREQ	= $dff09c
BPL1PTH = $dff0e0
BPL1PTL = $dff0e2

COLOR00	= $180
COLOR01 = $182
COLOR17 = $1a2
COLOR18 = $1a4
COLOR19 = $1a6

SPRITE0	= $120



; Initialize the the program.
main:
; Get graphics library and store the original copper pointer.
	move.l EXEC_BASE,a6
	clr.l d0
	move.l #gfxname,a1
	jsr -EXEC_OldOpenLibrary(a6)
	move.l d0,a1	; Move the address to the graphics.library
	move.l GRAPHICS_COPPER_POINTER(a1),orig_cop1lch
	; d0 still points to the library pointer, so close it now
	jsr -EXEC_CloseLibrary(a6)

; Store register values so that they can be restored upon exit
	move INTENAR,orig_intena
	move DMACONR,orig_dmacon

; Set flag registers
	move #$7fff,INTENA ; disable all interrupts
	move #$7fff,INTREQ ; clear interrupt requests
	move #$7fff,INTREQ ; twice for greater compatibility with A4000
	move #$7fff,DMACON
	move #$87e0,DMACON

	; Wait for vertical blank before continuing.
	move.w #BOTTOM,d0
	bsr WaitRaster

	move.l #copper,COP1LCH
	
copperinit:
; Insert the 'screen' address to the copperlist.
	move.l #copper_bitplane,a0
	move.l #screen,d0
	move.w d0,6(a0)
	move.w #16,d1
	lsr.l d1,d0
	move.w d0,2(a0)

; Fill in the screen with a checker-board pattern.
	lea screen,a0
	move.w #BPLSIZE-1,d0
	move.w #(320/8)-1,d1
	move.w #$aa,d2
.loop:
	move.b d2,(a0)+
	dbf d1,.noswap
	eor.b #$ff,d2
	move.w #(320/8)-1,d1
.noswap:
	dbf d0,.loop

; Assign sprite address to the copper list.
	move.l #sprite,d1
	move.w d1,copper_sprite+4+2
	move.l #16,d2
	lsr.l d2,d1
	move.w d1,copper_sprite+2

; Assign null sprite addresses to the copper list.
	move.l #copper_sprite+8+2,a0
	move.l #16,d2
	move.w #7-1,d0
	move.l #nullsprite,d1
	lsr.l d2,d1
	move.l #nullsprite,d2
.sprite_loop:
	move.w d1,(a0)
	move.w d2,4(a0)
	add.l #8,a0
	dbf d0,.sprite_loop

; Work:
; d5 - sprite movement tickrate
; d6 - copperbar shift
; d7 - copperbar position
	move #SPRITE_TICKRATE,d5
	move #1,d6
	move #HALF-64,d7


; We'll bounce back here after each "frame".
mainloop:
	move.w #TOP,d0
	bsr WaitRaster
	move.w #TOP,d0
	bsr WaitRasterEnd

; Shift sprite right
	subi.b #1,d5
	bne .nospritemove
	move.b #SPRITE_TICKRATE,d5
	addi.b #1,sprite+1
.nospritemove:

; Update wait positions in the copperlist for the copper bar.
	move.l #lineras1,a0
	move.l #5,d1
	move.b d7,d0
.loop:
	addi #1,d0
	move.b d0,(a0)
	add.l #8,a0
	dbf d1,.loop

; Increase or decrease the next position of the line.
	add d6,d7


; Bounce will reverse the direction of the line once it reaches
; BORDER_TOP or BORDER_BOTTOM.
bounce:
	cmp #BORDER_TOP,d7
	blo reverse
	cmp #BORDER_BOTTOM,d7
	bhi reverse
	bra waitmouse

reverse:
	neg d6
	add d6,d7


; If mouse button is pressed - quit. Otherwise restart the main loop.
waitmouse:
	btst #6,CIAAPRA
	bne mainloop

exit:
	move.l orig_cop1lch,COP1LCH
	move orig_intena,d0
	bset #15,d0
	move d0,INTENA
	move orig_dmacon,d0
	bset #15,d0
	move d0,DMACON
	rts

;; Routines


WaitRaster:
	; Wait for rasterline at d0.w. Modifies d0-d2/a0.
	move.l #$1ff00,d2
	lsl.l #8,d0
	and.l d2,d0
	lea VPOSR,a0
.loop:
	move.l (a0),d1
	and.l d2,d1
	cmp.l d1,d0
	bne.s .loop
	rts


WaitRasterEnd:
	; Wait for rasterline at d0.w. Modifies d0-d2/a0.
	move.l #$1ff00,d2
	lsl.l #8,d0
	and.l d2,d0
	lea VPOSR,a0
.loop:
	move.l (a0),d1
	and.l d2,d1
	cmp.l d1,d0
	beq.s .loop
	rts



;; Variables


orig_cop1lch:
	dc.l 0
orig_dmacon:
	dc.w 0
orig_intena
	dc.w 0

gfxname:
	dc.b "graphics.library",0

;; Sprites

	SECTION copper,DATA_C
sprite:
	dc.w $2c40,$3c00 ; Vstart.b,Hstart/2.b,V.stop.b,%A000SEH
	dc.w %0000111111110000,%0000000000000000
	dc.w %0001111111111000,%0000000000000000
	dc.w %0011001111001100,%0000110000110000
	dc.w %0111001111001110,%0000110000110000
	dc.w %1111001111001111,%0000110000110000
	dc.w %1111111111111111,%0000000000000000
	dc.w %1111111111111111,%0000000000000000
	dc.w %1111111111111111,%0000000000000000
	dc.w %1111111111111111,%0000000000000000
	dc.w %1111111111111111,%0001000000001000
	dc.w %1111111111111111,%0001110000111000
	dc.w %1111111111111111,%0000111111110000
	dc.w %0111111111111110,%0000011111100000
	dc.w %0011111111111100,%0000000000000000
	dc.w %0001111111111000,%0000000000000000
	dc.w %0000111111110000,%0000000000000000
	dc.w 0,0

nullsprite:
	dc.w $2a20,$2b00
	dc.w 0,0
	dc.w 0,0

;; Copper


copper:
	dc.w $1fc,0 ; slow fetch mode, AGA compatibility

	dc.w $100,$0204

	; Configure the display window
	dc.w $8e,$2c81
	dc.w $90,$2cc1
	dc.w $92,$38
	dc.w $94,$d0
	; Configure bitplane modulos
	dc.w $108,0
	dc.w $10a,0
	dc.w $102,0

	dc.w COLOR17,$f00
	dc.w COLOR18,$0ff
	dc.w COLOR19,$fff
	; Configure all 8 sprite pointers
copper_sprite:
	dc.w SPRITE0,0
	dc.w SPRITE0+2,0
	dc.w SPRITE0+4,0
	dc.w SPRITE0+6,0
	dc.w SPRITE0+8,0
	dc.w SPRITE0+10,0
	dc.w SPRITE0+12,0
	dc.w SPRITE0+14,0
	dc.w SPRITE0+16,0
	dc.w SPRITE0+18,0
	dc.w SPRITE0+20,0
	dc.w SPRITE0+22,0
	dc.w SPRITE0+24,0
	dc.w SPRITE0+26,0
	dc.w SPRITE0+28,0
	dc.w SPRITE0+30,0

	; Configure bitplane pointer
	dc.w COLOR01,$0f0
copper_bitplane:
	dc.w $e0,0
	dc.w $e2,0
	dc.w $100,$1204

	; top frame	
	dc.w COLOR00,$349
	dc.w $2b07,$fffe
	dc.w COLOR00,$56c
	dc.w $2c07,$fffe

	; center
	dc.w COLOR00,$123
lineras1:
	dc.w $8007,$fffe
	dc.w COLOR00,$055
	dc.w $8107, $fffe
	dc.w COLOR00,$0aa
	dc.w $8207, $fffe
	dc.w COLOR00,$0ff
	dc.w $8307, $fffe
	dc.w COLOR00,$0aa
	dc.w $8407, $fffe
	dc.w COLOR00,$055
	dc.w $8507, $fffe
	dc.w COLOR00,$123
	

	; bottom frame
	dc.w $ffdf,$fffe
	dc.w $2c07,$fffe
	dc.w COLOR00,$56c
	dc.w $2d07,$fffe
	dc.w COLOR00,$349
	dc.w $ffff,$fffe

	EVEN
screen:
	dcb BPLSIZE/2
