; Screen size
SCREEN_WIDTH = 320
SCREEN_HEIGHT = 256
SCREEN_TOP = $2c
SCREEN_VBLANK = $138
BPLSIZE = SCREEN_WIDTH*SCREEN_HEIGHT/8

PICW      = 144 ; Pic width increased to the next 16-aligned value (next word).
PICH      = 75
PICDEPTH  = 3   ; Bitplane count.
PICMARGIN = (SCREEN_WIDTH-PICW)/2
PICBYPL   = PICW/8           ; Bytes-Per-Bitplane-Line
PICBYWIDTH= PICBYPL*PICDEPTH ; Picture's Byte-Width 


; These are the values from which the rasterbar will bounce.
RASTERBAR_TOP	= $2c
RASTERBAR_BOTTOM= $85

SPRITE_TICKRATE = 50/12 ; 1/4 of a second for a PAL Amiga


; exec.library
EXEC_BASE = 4
EXEC_OldOpenLibrary = 408
EXEC_CloseLibrary = 414

; graphics.library
GRAPHICS_COPPER_POINTER = 38


; Hardware registers.
CIAAPRA	= $bfe001

; Chipset
CS_BASE = $dff000
VPOSR	= $dff004
VHPOSR	= $dff006
COP1LCH = $dff080
DMACON	= $dff096
DMACONR	= $dff002
INTENA	= $dff09a
INTENAR	= $dff01c
INTREQ	= $dff09c
BPL1PTH = $dff0e0
BPL1PTL = $dff0e2

COLOR00	= $180
COLOR01 = $182
COLOR17 = $1a2
COLOR18 = $1a4
COLOR19 = $1a6

SPRITE0	= $120



; Program entry-point
main:
	movem.l d1-a6,-(sp)
; Get graphics library and store the original copper pointer.
	move.l EXEC_BASE,a6
	clr.l d0
	move.l #gfxname,a1
	jsr -EXEC_OldOpenLibrary(a6)
	move.l d0,a1	; Move the address to the graphics.library
	move.l GRAPHICS_COPPER_POINTER(a1),orig_cop1lch
	; d0 still points to the library pointer, so close it now
	jsr -EXEC_CloseLibrary(a6)

; Store register values so that they can be restored upon exit
	move INTENAR,orig_intena
	move DMACONR,orig_dmacon

; Wait for vertical blank before continuing.
; This should happen before we set the DMA and INT registers.
	move.w #SCREEN_VBLANK,d0
	bsr WaitRaster

; Set DMA/INT registers
	move #$7fff,INTENA ; disable all interrupts
	move #$7fff,INTREQ ; clear interrupt requests
	move #$7fff,INTREQ ; twice for greater compatibility with A4000
	move #$7fff,DMACON
	move #$87e0,DMACON

	bsr Init
	move.l #copper,COP1LCH
	bsr Run

exit:
; Restore original copper list.
	move.l orig_cop1lch,COP1LCH
; Restore original INTENA register.
	move orig_intena,d0
	bset #15,d0
	move d0,INTENA
; Restore original DMACON register.
	move orig_dmacon,d0
	bset #15,d0
	move d0,DMACON
	movem.l (sp)+,d1-a6
	clr d0  ; Program's exitcode.
	rts

;; Routines

Init:
	movem.l d0-a6,-(sp)
; Insert the 'screen' address to the copperlist.
	move.l #copper_bitplane,a0
	move.l #screen,d0
	move.w d0,6(a0)
	swap d0
	move.w d0,2(a0)

; Insert the 'pic' bitplanes addresses to the copperlist.
	lea copper_pic,a0
	lea pic,a1
	move #3-1,d1
.picloop:
	move.l a1,d0
	move.w d0,6(a0)
	swap d0
	move.w d0,2(a0)
	addq #8,a0
	lea PICBYPL(a1),a1
	dbf d1,.picloop

; Fill in the screen with a checker-board pattern.
	lea screen,a0
	move.w #BPLSIZE-1,d0
	move.w #(320/8)-1,d1
	move.w #$aa,d2
.loop:
	move.b d2,(a0)+
	dbf d1,.noswap
	eor.b #$ff,d2
	move.w #(320/8)-1,d1
.noswap:
	dbf d0,.loop

; Assign sprite address to the copper list.
	move.l #sprite,d1
	move.w d1,copper_sprite+4+2
	swap d1
	move.w d1,copper_sprite+2

; Assign null sprite addresses to the copper list.
	move.l #copper_sprite+8+2,a0
	move.w #7-1,d0
	move.l #nullsprite,d1
	swap d1
	move.l #nullsprite,d2
.sprite_loop:
	move.w d1,(a0)
	move.w d2,4(a0)
	addq.l #8,a0
	dbf d0,.sprite_loop

	movem.l (sp)+,d0-a6
	rts


Run:
	movem.l d0-a6,-(sp)

; Work data:
; d5 - sprite movement tickrate
; d6 - copperbar shift
; d7 - copperbar position
	move #SPRITE_TICKRATE,d5
	move #1,d6
	move #(RASTERBAR_BOTTOM+RASTERBAR_TOP)/2,d7


; We'll bounce back here after each "frame".
mainloop:
	move.w #SCREEN_TOP,d0
	bsr WaitRaster
	move.w #SCREEN_TOP,d0
	bsr WaitRasterEnd

; Shift sprite right
	subi.b #1,d5
	bne .nospritemove
	move.b #SPRITE_TICKRATE,d5
	addi.b #1,sprite+1
.nospritemove:

; Update wait positions in the copperlist for the copper bar.
	lea lineras1,a0
	move.l #5,d1
	move.b d7,d0
.loop:
	addi #1,d0
	move.b d0,(a0)
	add.l #8,a0
	dbf d1,.loop

; Increase or decrease the next position of the line.
	add d6,d7


; Bounce will reverse the direction of the rasterbar once it reaches
; RASTERBAR_TOP or RASTERBAR_BOTTOM.
bounce:
	cmp #RASTERBAR_TOP,d7
	blo reverse
	cmp #RASTERBAR_BOTTOM,d7
	bhi reverse
	bra no_reverse

reverse:
	neg d6
	add d6,d7
no_reverse:

; If mouse button is pressed - quit. Otherwise restart the main loop.
waitmouse:
	btst #6,CIAAPRA
	bne mainloop

	movem.l (sp)+,d0-a6
	rts



WaitRaster:
	; Wait for rasterline at d0.w.
	movem.l d0-d2/a0,-(sp)
	move.l #$1ff00,d2
	lsl.l #8,d0
	and.l d2,d0
	lea VPOSR,a0
.loop:
	move.l (a0),d1
	and.l d2,d1
	cmp.l d1,d0
	bne.s .loop

	movem.l (sp)+,d0-d2/a0
	rts


WaitRasterEnd:
	; Wait for rasterline at d0.w.
	movem.l d0-d2/a0,-(sp)
	move.l #$1ff00,d2
	lsl.l #8,d0
	and.l d2,d0
	lea VPOSR,a0
.loop:
	move.l (a0),d1
	and.l d2,d1
	cmp.l d1,d0
	beq.s .loop

	movem.l (sp)+,d0-d2/a0
	rts



;; Variables


gfxname:
	dc.b "graphics.library",0

;; Sprites

	SECTION copper,DATA_C
sprite:
	dc.w $2c40,$3c00 ; Vstart.b,Hstart/2.b,V.stop.b,%A000SEH
	dc.w %0000111111110000,%0000000000000000
	dc.w %0001111111111000,%0000000000000000
	dc.w %0011001111001100,%0000110000110000
	dc.w %0111001111001110,%0000110000110000
	dc.w %1111001111001111,%0000110000110000
	dc.w %1111111111111111,%0000000000000000
	dc.w %1111111111111111,%0000000000000000
	dc.w %1111111111111111,%0000000000000000
	dc.w %1111111111111111,%0000000000000000
	dc.w %1111111111111111,%0001000000001000
	dc.w %1111111111111111,%0001110000111000
	dc.w %1111111111111111,%0000111111110000
	dc.w %0111111111111110,%0000011111100000
	dc.w %0011111111111100,%0000000000000000
	dc.w %0001111111111000,%0000000000000000
	dc.w %0000111111110000,%0000000000000000
	dc.w 0,0

nullsprite:
	dc.w $2a20,$2b00
	dc.w 0,0
	dc.w 0,0

;; Copper


copper:
	dc.w $1fc,0 ; slow fetch mode, AGA compatibility

	dc.w $100,$0204

	; Configure the display window
	dc.w $8e,$2c81
	dc.w $90,$2cc1
	dc.w $92,$38
	dc.w $94,$d0
	; Configure bitplane modulos
	dc.w $108,0
	dc.w $10a,0
	dc.w $102,0

	dc.w COLOR17,$f00
	dc.w COLOR18,$0ff
	dc.w COLOR19,$fff
	; Configure all 8 sprite pointers
copper_sprite:
	dc.w SPRITE0,0
	dc.w SPRITE0+2,0
	dc.w SPRITE0+4,0
	dc.w SPRITE0+6,0
	dc.w SPRITE0+8,0
	dc.w SPRITE0+10,0
	dc.w SPRITE0+12,0
	dc.w SPRITE0+14,0
	dc.w SPRITE0+16,0
	dc.w SPRITE0+18,0
	dc.w SPRITE0+20,0
	dc.w SPRITE0+22,0
	dc.w SPRITE0+24,0
	dc.w SPRITE0+26,0
	dc.w SPRITE0+28,0
	dc.w SPRITE0+30,0

	dc.w $0180,$0000,$0182,$0aaa,$0184,$0ff0,$0186,$02ff
	dc.w $0188,$024f,$018a,$0e00,$018c,$00f0,$018e,$00cc
copper_pic:
	dc.w $e0,0
	dc.w $e2,0
	dc.w $e4,0
	dc.w $e6,0
	dc.w $e8,0
	dc.w $ea,0
	; Bitplane registers need to be set to specific values for this to work.
	; BPLCON0 - enable PICDEPTH planes
	dc.w $100,(PICDEPTH<<12)|$0204
	; Reduce width of the screen to place the picture in the center
	dc.b 0,$90,$2c,$c1-PICMARGIN
	dc.w $92,$38+PICMARGIN/2
	dc.w $94,$d0-PICMARGIN/2
	; Set the bitplane modulo registers so that the next bitplane line
	; gets drawn from the address in memory where it actually is.
	dc.w $108,PICBYWIDTH-PICBYPL
	dc.w $10a,PICBYWIDTH-PICBYPL

	; top frame	
	dc.w COLOR00,$349
	dc.w $2b07,$fffe
	dc.w COLOR00,$56c
	dc.w $2c07,$fffe

	; rasterbar (copperbar)
	dc.w COLOR00,$123
lineras1:
	dc.w $8007,$fffe
	dc.w COLOR00,$055
	dc.w $8107, $fffe
	dc.w COLOR00,$0aa
	dc.w $8207, $fffe
	dc.w COLOR00,$0ff
	dc.w $8307, $fffe
	dc.w COLOR00,$0aa
	dc.w $8407, $fffe
	dc.w COLOR00,$055
	dc.w $8507, $fffe
	dc.w COLOR00,$123

	dc.b RASTERBAR_BOTTOM+6,$07,$ff,$fe

	; Configure bitplane pointer for the screen
copper_bitplane:
	dc.w $e0,0
	dc.w $e2,0
	; Reset registers from the pic.
	dc.w $100,$1204
	dc.w $90,$2cc1
	dc.w $92,$38
	dc.w $94,$d0
	dc.w $108,0
	dc.w $10a,0
	dc.w COLOR01,$0f0


	; bottom frame
	dc.w $ffdf,$fffe
	dc.w $2c07,$fffe
	dc.w COLOR00,$56c
	dc.w $2d07,$fffe
	dc.w COLOR00,$349
	dc.w $ffff,$fffe

pic: INCBIN "pic_130x75x3.raw"

	SECTION bss_c,bss_c
screen:
	ds BPLSIZE/2
orig_cop1lch:
	ds.l 1
orig_dmacon:
	ds.w 1
orig_intena:
	ds.w 1


