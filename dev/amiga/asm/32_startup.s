	SECTION MainSection,CODE
	JUMPPTR Start

	INCLUDE "PhotonsMiniStartup/PhotonsMiniWrapper1.04!.S"

; Screen size
SCREEN_WIDTH = 352
SCREEN_HEIGHT = 256
SCREEN_TOP = $2c
SCREEN_VBLANK = 256 ; $138
SCREEN_START = 1
SCREEN_DEPTH = 3

SCREEN_BYPL = SCREEN_WIDTH/8
SCREEN_VIEWPORT_BYPL=320/8

SCREEN_BYWIDTH = SCREEN_BYPL*SCREEN_DEPTH


BPLSIZE = SCREEN_BYPL*SCREEN_HEIGHT


PIC_BGCOLOR=$77f
TEXT_BGCOLOR=$224


PICW      = 144 ; Pic width increased to the next 16-aligned value (next word).
PICH      = 75
PICDEPTH  = 3   ; Bitplane count.
PICCOLORS = 8
PICY      = SCREEN_TOP+10
PICMARGIN = (320-PICW)/2
PICBYPL   = PICW/8           ; Bytes-Per-Bitplane-Line
PICBYWIDTH= PICBYPL*PICDEPTH ; Picture's Byte-Width


FONTW = 288
FONTH = 100
FONTDEPTH = 3
FONTBYPL = FONTW/8
FONTBYWIDTH = FONTDEPTH*FONTBYPL

FONT_GLYPH_W = 32
FONT_GLYPH_H = 20
FONT_GLYPH_BYPL = FONT_GLYPH_W/8

FONT_LINE_BYWIDTH = FONT_GLYPH_H*FONTBYWIDTH

NUM_GLYPHS = 44


; These are the values from which the rasterbar will bounce.
RASTERBAR_TOP    = $2c
RASTERBAR_BOTTOM = $85
RASTERBAR_HEIGHT = 5
RASTERBAR_MARGIN = 2

; Speed control of bitmap elements.
SPRITE_TICKRATE = 50/12 ; 1/4 of a second for a PAL Amiga
SCROLL_TICKRATE = 1
SCROLL_SHIFT = 4  ; range: 1-f
PLOT_COUNTDOWN = FONT_GLYPH_W/SCROLL_SHIFT-1
PLOT_SHORT_COUNTDOWN = FONT_GLYPH_W/2/SCROLL_SHIFT-1


SCREEN_Y = RASTERBAR_BOTTOM+RASTERBAR_HEIGHT+RASTERBAR_MARGIN
TEXT_Y = 100


; exec.library
EXEC_BASE = 4
EXEC_OldOpenLibrary = 408
EXEC_CloseLibrary = 414

; graphics.library
GRAPHICS_COPPER_POINTER = 38


; Hardware registers.
CIAAPRA = $bfe001

; Chipset
CS_BASE = $dff000

VPOSR = $dff004
VHPOSR = $dff006
POTGOR = $dff016
BLTCON0 = $dff040
BLTAFWM = $dff044
BLTAPTH = $dff050
BLTDPTH = $dff054
BLTSIZE = $dff058
BLTAMOD = $dff064
BLTDMOD = $dff066
COP1LCH = $dff080
DMACON = $dff096
DMACONR = $dff002
INTENA = $dff09a
INTENAR = $dff01c
INTREQ = $dff09c
BPL1PTH = $dff0e0
BPL1PTL = $dff0e2

COLOR00 = $180
COLOR01 = $182
COLOR02 = $184
COLOR03 = $186
COLOR04 = $188
COLOR05 = $18a
COLOR06 = $18c
COLOR07 = $18e
COLOR17 = $1a2
COLOR18 = $1a4
COLOR19 = $1a6

SPRITE0 = $120

; P61 Player - Music

P61mode	=1	;Try other modes ONLY IF there are no Fxx commands >= 20.
		;(f.ex., P61.new_ditty only works with P61mode=1)


;;    ---  options common to all P61modes  ---

usecode	=$c00b43b
P61pl=usecode&$400000

split4	=1	;Great time gain, but INCOMPATIBLE with F03, F02, and F01
		;speeds in the song! That's the ONLY reason it's default 0.
		;So ==> PLEASE try split4=1 in ANY mode!
		;Overrides splitchans to decrunch 1 chan/frame.
		;See ;@@ note for P61_SetPosition.


splitchans=1	;#channels to be split off to be decrunched at "playtime frame"
		;0=use normal "decrunch all channels in the same frame"
		;Experiment to find minimum rastertime, but it should be 1 or 2
		;for 3-4 channels songs and 0 or 1 with less channels.

visuctrs=0	;enables visualizers in this example: P61_visuctr0..3.w
		;containing #frames (#lev6ints if cia=1) elapsed since last
		;instrument triggered. (0=triggered this frame.)
		;Easy alternative to E8x or 1Fx sync commands.

asmonereport	=0	;ONLY for printing a settings report on assembly. Use
			;if you get problems (only works in AsmOne/AsmPro, tho)

p61system=0	;1=system-friendly. Use for DOS/Workbench programs.

p61exec	=0	;0 if execbase is destroyed, such as in a trackmo.

p61fade	=0	;enable channel volume fading from your demo

channels=4	;<4 for game sound effects in the higher channels. Incompatible
		; with splitchans/split4.

playflag=0	;1=enable music on/off capability (at run-time). .If 0, you can
		;still do this by just, you know, not calling P61_Music...
		;It's a convenience function to "pause" music in CIA mode.

p61bigjtab=0	;1 to waste 480b and save max 56 cycles on 68000.

opt020	=0	;1=enable optimizations for 020+. Please be 68000 compatible!
		;splitchans will already give MUCH bigger gains, and you can
		;try the MAXOPTI mode.

p61jump	=0	;0 to leave out P61_SetPosition (size gain)
		;1 if you need to force-start at a given position fex in a game

C	=0	;If you happen to have some $dffxxx value in a6, you can
		;change this to $xxx to not have to load it before P61_Music.

clraudxdat=0	;enable smoother start of quiet sounds. probably not needed.

optjmp	=1	;0=safety check for jump beyond end of song. Clear it if you
		;play unknown P61 songs with erroneous Bxx/Dxx commands in them

oscillo	=0	;1 to get a sample window (ptr, size) to read and display for
		;oscilloscope type effects (beta, noshorts=1, pad instruments)
		;IMPORTANT: see ;@@ note about chipmem dc.w buffer.

quietstart=0	;attempt to avoid the very first click in some modules
		;IMPORTANT: see ;@@ note about chipmem dc.w buffer.

use1Fx=0	;Optional extra effect-sync trigger (*). If your module is free
		;from E commands, and you add E8x to sync stuff, this will
		;change the usecode to include a whole code block for all E
		;commands. You can avoid this by only using 1Fx. (You can
		;also use this as an extra sync command if E8x is not enough,
		;of course.)

;(*) Slideup values>116 causes bugs in Protracker, and E8 causes extra-code
;for all E-commands, so I used this. It's only faster if your song contains 0
;E-commands, so it's only useful to a few, I guess. Bit of cyclemania. :)

;Just like E8x, you will get the trigger after the P61_Music call, 1 frame
;BEFORE it's heard. This is good, because it allows double-buffered graphics
;or effects running at < 50 fps to show the trigger synced properly.



;;    ---  CIA mode options (default) ---

	ifeq P61mode-1

p61cia	=1	;call P61_Music on the CIA interrupt instead of every frame.

lev6	=1	;1=keep the timer B int at least for setting DMA.
		;0="FBI mode" - ie. "Free the B-timer Interrupt".

		;0 requires noshorts=1, p61system=0, and that YOU make sure DMA
		;is set at 11 scanlines (700 usecs) after P61_Music is called.
		;AsmOne will warn you if requirements are wrong.

		;DMA bits will be poked in the address you pass in A4 to
		;P61_init. (Update P61_DMApokeAddr during playing if necessary,
		;for example if switching Coppers.)

		;P61_Init will still save old timer B settings, and initialize
		;it. P61_End will still restore timer B settings from P61_Init.
		;So don't count on it 'across calls' to these routines.
		;Using it after P61_Init and before P61_End is fine.

noshorts=0	;1 saves ~1 scanline, requires Lev6=0. Use if no instrument is
		;shorter than ~300 bytes (or extend them to > 300 bytes).
		;It does this by setting repeatpos/length the next frame
		;instead of after a few scanlines,so incompatible with MAXOPTI

dupedec	=0	;0=save 500 bytes and lose 26 cycles - I don't blame you. :)
		;1=splitchans or split4 must be on.

suppF01	=1	;0 is incompatible with CIA mode. It moves ~100 cycles of
		;next-pattern code to the less busy 2nd frame of a notestep.
		;If you really need it, you have to experiment as the support
		;is quite complex. Basically set it to 1 and try the various
		;P61modes, if none work, change some settings.

	endc




; Program entry-point
Demo:
	movem.l d1-a6,-(sp)

	move #$87e0,DMACON
	move #$e000,INTENA ; Enable master and level 6 IRQ

; Init P61 player for music
	movem.l d0-a6,-(sp)
	lea Module1,a0
	sub.l a1,a1
	sub.l a2,a2
	moveq #0,d0
	jsr P61_Init
	movem.l (sp)+,d0-a6

	bsr Init
	move.l #copper,COP1LCH
	bsr Run

exit:
	bsr WaitBlitter
; Shutdown P61 player (music)
	jsr P61_End

	movem.l (sp)+,d1-a6
	clr d0  ; Program's exitcode.
	rts

;; Routines

Init:
	movem.l d0-a6,-(sp)

; Insert screen color palette to the copperlist.
	lea font_e-7*2,a0 ; 8 colors, each color is a word
	lea copper_bitplane_palette+2,a1
	moveq #8-1,d0
.paletteloop:
	move (a0)+,(a1)+
	add.l #2,a1
	dbf d0,.paletteloop

; Insert the 'pic' bitplanes addresses to the copperlist.
	lea pic,a0
	lea copper_pic,a1
	move #PICDEPTH-1,d1
.picloop:
	bsr PokeCopperAddress
	lea PICBYPL(a0),a0
	addq.l #8,a1
	dbf d1,.picloop

; Clear screen bitplanes.
	lea screen,a0
	move.w #SCREEN_BYWIDTH*SCREEN_HEIGHT,d0
.clrscr:
	clr.b (a0)+		; Clears the screen
	;move.b #$f0,(a0)+	; Paints vertical belts
	dbf d0,.clrscr

; Assign sprite address to the copper list.
	lea nullsprite,a0
	lea copper_sprite,a1
	bsr PokeCopperAddress

; Assign null sprite addresses to the copper list.
	lea nullsprite,a0
	lea copper_sprite+8,a1
	move.w #7-1,d0
.sprite_loop:
	bsr PokeCopperAddress
	addq.l #8,a1
	dbf d0,.sprite_loop

	bsr BounceIt
	lea copperbar_behind,a0
	bsr PokeCopperbarColors

	movem.l (sp)+,d0-a6
	rts


Run:
	movem.l d0-a6,-(sp)

; Work data:
; d3 - PlotText scroll interleave interval
; d4 - text scroll tickrate
; d5 - sprite movement tickrate
	clr d3
	move #SCROLL_TICKRATE,d4
	move #SPRITE_TICKRATE,d5


; We'll bounce back here after each "frame".
mainloop:
	move.w #SCREEN_START,d0
	bsr WaitRaster
	btst #2,POTGOR
	bne .normb
	move.w #SCREEN_START+1,d0
	bsr WaitRaster
	bra mainloop
.normb:

; Shift sprite right
	subi.b #1,d5
	bne .nospritemove
	move.b #SPRITE_TICKRATE,d5
	addi.b #1,sprite+1
.nospritemove:

; Update wait positions in the copperlist for the copper bar.
	move.l sine_idx(pc),d0
	addq.l #1,d0
	cmp #sine_end-sine,d0
	blo .no_sine_reset
	clr.l d0
.no_sine_reset:
	move.l d0,sine_idx
	lea sine(pc),a0
	clr d1
	move.b (a0,d0),d1

	cmp.l #(sine_end-sine)/2,d0
	blo .bar_behind

	lea copperbar_infront(pc),a0
	bsr PokeCopperbarColors
	bra .position_copperbar
.bar_behind:
	lea copperbar_behind(pc),a0
	bsr PokeCopperbarColors
.position_copperbar:
	move #(RASTERBAR_BOTTOM+RASTERBAR_TOP)/2,d0
	add d1,d0

	lea lineras1,a0
	move.l #RASTERBAR_HEIGHT,d1
.loop:
	addi #1,d0
	move.b d0,(a0)
	add.l #lineras2-lineras1,a0
	dbf d1,.loop


; Scroll the scroller.
	dbf d4,no_scroll
	bsr BounceIt
	bsr ScrollIt
	move #SCROLL_TICKRATE,d4

	dbf d3,no_plot
	bsr PlotText
	move PlotCountdown(pc),d3
no_scroll:
no_plot:

waitvblank:
	move.w #SCREEN_VBLANK,d0
	bsr WaitRaster

; If mouse button is pressed - quit. Otherwise restart the main loop.
waitmouse:
	btst #6,CIAAPRA
	bne mainloop

	movem.l (sp)+,d0-a6
	rts


BounceIt:
	movem.l d0-a6,-(sp)

	move.l BounceY(pc),d0
	move BounceYaccel(pc),d1

	add BounceYspeed(pc),d0
	move.l d0,BounceY

	cmp #30,BounceYspeed
	blt .nobounce
	neg BounceYspeed
	sub d1,BounceYspeed
.nobounce:
	add d1,BounceYspeed

	asr.l #3,d0
	lea screen,a0
	mulu #SCREEN_BYWIDTH,d0
	add.l d0,a0

; Insert the 'screen' address to the copperlist.
	lea copper_bitplane,a1
	move #SCREEN_DEPTH-1,d1
.screenloop:
	bsr PokeCopperAddress
	lea SCREEN_BYPL(a0),a0
	addq.l #8,a1
	dbf d1,.screenloop

	movem.l (sp)+,d0-a6
	rts

	EVEN
BounceY:
	dc.l 0
BounceYspeed:
	dc.w 0
BounceYaccel:
	dc.w 1


ScrollIt:
	; Use the blitter to scroll a part of the screen to the left.
	; To do this, we need to blit backwards. A forward-blit would
	; scroll to the right. To blit backwards, we need to start the
	; blit from the bottom-right (br) corner.
bltx = 0
blty = TEXT_Y
bltoffs = blty*(SCREEN_BYWIDTH)+bltx/8

blth = 20
bltw = 352
bltw_bywidth = bltw/8*SCREEN_DEPTH
bltwords = bltw/16
bltskip = (SCREEN_WIDTH-bltw)/8

; The bottom-right corner in terms of Amiga display is something that
; may not be very intuitive. First, we need to understand that the
; bottom-right corner of a rect is the final word of that rect, hence -2.
; Second, we need to understand that that bitplanes are laid out interleaved.
; If we have 3 bitplanes, then each line looks like this:
;
;    bitplane 1 \
;    bitplane 2  } line 1
;    bitplane 3 /
;    bitplane 1 \
;    bitplane 2  } line 2
;    bitplane 3 /
;
; The brcorner is at the final bitplane of the final line.
; Now, when we want to locate the final word of that final bitplane, we need to
; fully pass all previous bitplanes of the targeted *picture* ie. not just the
; targeted *rect*, which is a slice of the *picture*, but the whole *picture*.
;
; The following placement of brcorner allows us to freely manipulate
; bltx, blty, bltw, blth and as long as the 'x' values stay within a word
; boundary, the rect will be positioned correctly.
brcorner = (blth-1)*(SCREEN_BYWIDTH)+(SCREEN_BYWIDTH-SCREEN_BYPL)+(bltw/8)-2
	movem.l d0-a6,-(sp)

	bsr WaitBlitter

	; Use A & D, Use LF7-0 as described at bottom,
	; Use reverse blit (the 2) to scroll left.
	move.l #(SCROLL_SHIFT<<28)|$09f00002,BLTCON0

	move.l #$ffffffff,BLTAFWM
	move.l #screen+bltoffs+brcorner,BLTAPTH
	move.l #screen+bltoffs+brcorner,BLTDPTH
	move.w #bltskip,BLTAMOD
	move.w #bltskip,BLTDMOD
	; Things to know:
	;
	; 1. BLTSIZE is %hhhhhhhhhhwwwwww where if all 'w' bits are set to 0,
	;    then the actual width is 64. Width is in words.
	;
	; 2. Writing to BLTSIZE triggers the blit.
	move.w #blth*SCREEN_DEPTH*64+bltwords,BLTSIZE
	movem.l (sp)+,d0-a6
	rts


PlotText:
	movem.l d0-a6,-(sp)
	; Load the character to display.
	move.l text_pos,a1
	; See if we've crossed the boundary of the string.
	lea text_e(pc),a0
	cmp.l a0,a1
	blo .plot
	; Reset
	lea text(pc),a1
.plot:
	move.b (a1)+,d0
	bsr PlotChar
	; Store position of the next char.
	move.l a1,text_pos
	; Store countdown until the next char.
	lea PlotCountdown(pc),a0
	move #PLOT_COUNTDOWN,(a0)
	cmp.b #'I',d0
	bne.s .end
	move #PLOT_SHORT_COUNTDOWN,(a0)
.end:
	movem.l (sp)+,d0-a6
	rts

Playroutine:
	include "P6112-Play.i"

	EVEN
PlotCountdown:
	dc.w 0


PlotChar:
	; Maps the character to the glyph index using the GlyphTable.
	; GlyphTable is an array indexed by ASCII-32 (the space) where
	; values are indexes of characters in the bitmap font.
	movem.l d0-d1/a0,-(sp)
	and.l #$ff,d0
	sub #' ',d0
	cmp.b #GlyphTable_e-GlyphTable,d0
	blo .inrange
	clr.l d0
.inrange:
	lea GlyphTable(pc),a0
	move.b (a0,d0),d0
	bsr PlotGlyph
	movem.l (sp)+,d0-d1/a0
	rts


GlyphTable:
	dc.b 43,38
	dcb.b 5,0
	dc.b 42
	dcb.b 4,0
	dc.b 37,40,36,41
	dc.b 26,27,28,29,30,31,32,33,34,35 ; digits
	dcb.b 5,0
	dc.b 39,0
	; letters
	dc.b 0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25
	dcb.b 5,0
	dc.b 42
GlyphTable_e:
	EVEN


PlotGlyph:
	; Use the blitter to paint a character on screen using our bitmap font.
	; Args:
	; - d0 - long word glyph index
	; There are 44 characters (0-42 visible + 43 is space)
GLYPH_COLS = 9
GLYPH_X = SCREEN_WIDTH-FONT_GLYPH_W
GLYPH_Y = TEXT_Y
	movem.l d0-d1,-(sp)

	; Division leaves column in the low word and row in the high word.
	divu #GLYPH_COLS,d0
	move.l d0,d1

	; Calculate X-offset
	swap d0
	and.l #$ffff,d0
	mulu #FONT_GLYPH_BYPL,d0
	; Calculate Y-offset
	and.l #$ffff,d1
	mulu #FONT_LINE_BYWIDTH,d1

	add.l d1,d0
	add.l #font,d0


	bsr WaitBlitter

	move.l #$09f00000,BLTCON0
	move.l #$ffffffff,BLTAFWM
	move.l d0,BLTAPTH
	move.l #screen+(GLYPH_Y*SCREEN_BYWIDTH)+(GLYPH_X/8),BLTDPTH
	move.w #FONTBYPL-FONT_GLYPH_BYPL,BLTAMOD
	move.w #SCREEN_BYPL-FONT_GLYPH_BYPL,BLTDMOD
	move.w #FONT_GLYPH_H*SCREEN_DEPTH*64+FONT_GLYPH_BYPL/2,BLTSIZE
	movem.l (sp)+,d0-d1
	rts


WaitRasterEnd:
	; Wait for rasterline at d0.w.
	movem.l d0-d2/a0,-(sp)
	move.l #$1ff00,d2
	lsl.l #8,d0
	and.l d2,d0
	lea VPOSR,a0
.loop:
	move.l (a0),d1
	and.l d2,d1
	cmp.l d1,d0
	beq.s .loop

	movem.l (sp)+,d0-d2/a0
	rts


PokeCopperAddress:
	; Args:
	; - a0 - source address to poke
	; - a1 - copperlist pointer, pointing at the
	;        beginning of the copper instruction
	move.l d0,-(sp)
	move.l a0,d0
	move.w d0,6(a1)
	swap d0
	move.w d0,2(a1)
	move.l (sp)+,d0
	rts

PokeCopperbarColors:
	; Args:
	;   a0 - the color table
	movem.l d0-a6,-(sp)

	lea lineras1+2,a1
	move #RASTERBAR_HEIGHT-1,d0
heightloop:
	add.l #4,a1    ; skip the wait instruction
	moveq #PICCOLORS-1,d1  ; num colors
.colorloop:
	move.w (a0)+,(a1)
	add.l #4,a1
	dbra d1,.colorloop
	dbra d0,heightloop

	movem.l (sp)+,d0-a6
	rts



;; Variables

gfxname:
	dc.b "graphics.library",0

	EVEN
orig_cop1lch:
	ds.l 1
orig_dmacon:
	ds.w 1
orig_intena:
	ds.w 1
text:
	dc.b "THIS IS AN AMIGA DEMO, STRAIGHT OUTTA THE M68K. "
	dc.b "GOTTA-GO-FAST!    "
text_e:
	EVEN
text_pos: dc.l -1

copperbar_behind:
	; 80% alpha-blending with pic being having the dominant colors
	dc.w $055,$899,$cd1,$2dd
	dc.w $24d,$b11,$0d1,$f0f

	dc.w $0aa,$7aa,$ce4,$2ee
	dc.w $26e,$b22,$0e2,$f0f

	dc.w $0ff,$7bb,$cf6,$2ff
	dc.w $27f,$b33,$0f3,$f0f

	dc.w $0aa,$7aa,$ce4,$2ee
	dc.w $26e,$b22,$0e2,$f0f

	dc.w $055,$899,$cd1,$2dd
	dc.w $24d,$b11,$0d1,$f0f


copperbar_infront:
	dcb.w 8,$055
	dcb.w 8,$0aa
	dcb.w 8,$0ff
	dcb.w 8,$0aa
	dcb.w 8,$055

sine_idx:
	dc.l 0,0
sine:
	INCBIN "sine.41.200.b"
sine_end:

;; Sprites

	SECTION copper,DATA_C
sprite:
	dc.w $dc40,$ec00 ; Vstart.b,Hstart/2.b,V.stop.b,%A000SEH
	dc.w %0000111111110000,%0000000000000000
	dc.w %0001111111111000,%0000000000000000
	dc.w %0011001111001100,%0000110000110000
	dc.w %0111001111001110,%0000110000110000
	dc.w %1111001111001111,%0000110000110000
	dc.w %1111111111111111,%0000000000000000
	dc.w %1111111111111111,%0000000000000000
	dc.w %1111111111111111,%0000000000000000
	dc.w %1111111111111111,%0000000000000000
	dc.w %1111111111111111,%0001000000001000
	dc.w %1111111111111111,%0001110000111000
	dc.w %1111111111111111,%0000111111110000
	dc.w %0111111111111110,%0000011111100000
	dc.w %0011111111111100,%0000000000000000
	dc.w %0001111111111000,%0000000000000000
	dc.w %0000111111110000,%0000000000000000
	dc.w 0,0

nullsprite:
	dc.w $2a20,$2b00
	dc.w 0,0
	dc.w 0,0

;; Copper

copper:
	dc.w $1fc,0 ; slow fetch mode, AGA compatibility

	dc.w $100,$0204

	; Configure the display window
	dc.b 0,$8e,PICY,81
	dc.w $90,$2cc1
	dc.w $92,$38
	dc.w $94,$d0
	; Configure bitplane modulos
	dc.w $108,0
	dc.w $10a,0
	dc.w $102,0

	dc.w COLOR17,$f00
	dc.w COLOR18,$0ff
	dc.w COLOR19,$fff
	; Configure all 8 sprite pointers
copper_sprite:
	dc.w SPRITE0,0
	dc.w SPRITE0+2,0
	dc.w SPRITE0+4,0
	dc.w SPRITE0+6,0
	dc.w SPRITE0+8,0
	dc.w SPRITE0+10,0
	dc.w SPRITE0+12,0
	dc.w SPRITE0+14,0
	dc.w SPRITE0+16,0
	dc.w SPRITE0+18,0
	dc.w SPRITE0+20,0
	dc.w SPRITE0+22,0
	dc.w SPRITE0+24,0
	dc.w SPRITE0+26,0
	dc.w SPRITE0+28,0
	dc.w SPRITE0+30,0

	dc.w $0182,$0aaa,$0184,$0ff0,$0186,$02ff
	dc.w $0188,$024f,$018a,$0e00,$018c,$00f0,$018e,$00cc

copper_pic:
	dc.w $e0,0
	dc.w $e2,0
	dc.w $e4,0
	dc.w $e6,0
	dc.w $e8,0
	dc.w $ea,0
	; Bitplane registers need to be set to specific values for this to work.
	; BPLCON0 - enable PICDEPTH planes
	dc.w $100,(PICDEPTH<<12)|$0204
	; Reduce width of the screen to place the picture in the center
	dc.b 0,$90,$2c,$c1-PICMARGIN
	dc.w $92,$38+PICMARGIN/2
	dc.w $94,$d0-PICMARGIN/2
	; Set the bitplane modulo registers so that the next bitplane line
	; gets drawn from the address in memory where it actually is.
	dc.w $108,PICBYWIDTH-PICBYPL
	dc.w $10a,PICBYWIDTH-PICBYPL

	; Wait for some top position before setting the background color or
	; else the CPU will not have enough time to update the copperbar
	; position on the copper list.
	dc.w $0707,$fffe
	dc.w COLOR00,PIC_BGCOLOR

	; rasterbar (copperbar)
lineras1:
	dc.w $8007,$fffe
	dc.w COLOR00,$055
	dc.w COLOR01,$055
	dc.w COLOR02,$055
	dc.w COLOR03,$055
	dc.w COLOR04,$055
	dc.w COLOR05,$055
	dc.w COLOR06,$055
	dc.w COLOR07,$055
lineras2:
	dc.w $8107, $fffe
	dc.w COLOR00,$0aa
	dc.w COLOR01,$0aa
	dc.w COLOR02,$0aa
	dc.w COLOR03,$0aa
	dc.w COLOR04,$0aa
	dc.w COLOR05,$0aa
	dc.w COLOR06,$0aa
	dc.w COLOR07,$0aa
	dc.w $8207, $fffe
	dc.w COLOR00,$0ff
	dc.w COLOR01,$0ff
	dc.w COLOR02,$0ff
	dc.w COLOR03,$0ff
	dc.w COLOR04,$0ff
	dc.w COLOR05,$0ff
	dc.w COLOR06,$0ff
	dc.w COLOR07,$0ff
	dc.w $8307, $fffe
	dc.w COLOR00,$0aa
	dc.w COLOR01,$0aa
	dc.w COLOR02,$0aa
	dc.w COLOR03,$0aa
	dc.w COLOR04,$0aa
	dc.w COLOR05,$0aa
	dc.w COLOR06,$0aa
	dc.w COLOR07,$0aa
	dc.w $8407, $fffe
	dc.w COLOR00,$055
	dc.w COLOR01,$055
	dc.w COLOR02,$055
	dc.w COLOR03,$055
	dc.w COLOR04,$055
	dc.w COLOR05,$055
	dc.w COLOR06,$055
	dc.w COLOR07,$055
	dc.w $8507, $fffe

	dc.w COLOR00,PIC_BGCOLOR
	dc.w $0182,$0aaa,$0184,$0ff0,$0186,$02ff
	dc.w $0188,$024f,$018a,$0e00,$018c,$00f0,$018e,$00cc

	; Wait for the horizontal blank to have enough time to setup
	; screen bitplanes.
	dc.b SCREEN_Y-1,$bf,$ff,$fe

copper_bitplane_palette:
	dc.w COLOR01,$0f0
	dc.w COLOR02,$f23
	dc.w COLOR03,$ff3
	dc.w COLOR04,$fff
	dc.w COLOR05,$1f3
	dc.w COLOR06,$12f
	dc.w COLOR07,$1ff

	; Configure bitplane pointer for the screen
copper_bitplane:
	dc.w $e0,0
	dc.w $e2,0
	dc.w $e4,0
	dc.w $e6,0
	dc.w $e8,0
	dc.w $ea,0
	; Reset registers from the pic.
	dc.w $100,(SCREEN_DEPTH*$1000)|$204
	dc.w $90,$2cc1
	dc.w $92,$38
	dc.w $94,$d0
	dc.w $108,SCREEN_BYWIDTH-SCREEN_VIEWPORT_BYPL
	dc.w $10a,SCREEN_BYWIDTH-SCREEN_VIEWPORT_BYPL

	dc.b SCREEN_Y+1,$07,$ff,$fe
	dc.w COLOR00,$76e
	dc.b SCREEN_Y+2,$07,$ff,$fe
	dc.w COLOR00,$77f
	dc.b SCREEN_Y+3,$07,$ff,$fe
	dc.w COLOR00,$76e

	dc.b SCREEN_Y+7,$07,$ff,$fe
	dc.w COLOR00,$76d
	dc.b SCREEN_Y+8,$07,$ff,$fe
	dc.w COLOR00,$77e
	dc.b SCREEN_Y+9,$07,$ff,$fe
	dc.w COLOR00,$76d

	dc.b SCREEN_Y+14,$07,$ff,$fe
	dc.w COLOR00,$75d
	dc.b SCREEN_Y+15,$07,$ff,$fe
	dc.w COLOR00,$76d
	dc.b SCREEN_Y+16,$07,$ff,$fe
	dc.w COLOR00,$75d

	dc.b SCREEN_Y+21,$07,$ff,$fe
	dc.w COLOR00,$669
	dc.b SCREEN_Y+22,$07,$ff,$fe
	dc.w COLOR00,$66a
	dc.b SCREEN_Y+23,$07,$ff,$fe
	dc.w COLOR00,$669

	dc.b SCREEN_Y+28,$07,$ff,$fe
	dc.w COLOR00,$659
	dc.b SCREEN_Y+29,$07,$ff,$fe
	dc.w COLOR00,$669
	dc.b SCREEN_Y+30,$07,$ff,$fe
	dc.w COLOR00,$659

	dc.b SCREEN_Y+35,$07,$ff,$fe
	dc.w COLOR00,$558
	dc.b SCREEN_Y+36,$07,$ff,$fe
	dc.w COLOR00,$559
	dc.b SCREEN_Y+37,$07,$ff,$fe
	dc.w COLOR00,$558

	dc.b SCREEN_Y+42,$07,$ff,$fe
	dc.w COLOR00,$447
	dc.b SCREEN_Y+43,$07,$ff,$fe
	dc.w COLOR00,$448
	dc.b SCREEN_Y+44,$07,$ff,$fe
	dc.w COLOR00,$447

	dc.b SCREEN_Y+49,$07,$ff,$fe
	dc.w COLOR00,$336
	dc.b SCREEN_Y+50,$07,$ff,$fe
	dc.w COLOR00,$437
	dc.b SCREEN_Y+51,$07,$ff,$fe
	dc.w COLOR00,$336

	dc.b SCREEN_Y+56,$07,$ff,$fe
	dc.w COLOR00,$336
	dc.b SCREEN_Y+57,$07,$ff,$fe
	dc.w COLOR00,$337
	dc.b SCREEN_Y+58,$07,$ff,$fe
	dc.w COLOR00,$336

	dc.b SCREEN_Y+63,$07,$ff,$fe
	dc.w COLOR00,$335
	dc.b SCREEN_Y+64,$07,$ff,$fe
	dc.w COLOR00,$336
	dc.b SCREEN_Y+65,$07,$ff,$fe
	dc.w COLOR00,$335

	dc.b SCREEN_Y+70,$07,$ff,$fe
	dc.w COLOR00,TEXT_BGCOLOR
	dc.b SCREEN_Y+71,$07,$ff,$fe
	dc.w COLOR00,$225
	dc.b SCREEN_Y+72,$07,$ff,$fe
	dc.w COLOR00,TEXT_BGCOLOR

	; -- Plate --

	; line
	dc.w $f807,$fffe
	dc.w COLOR00,$336

	dc.w $f907,$fffe
	dc.w COLOR00,$335

	; line
	dc.w $fb07,$fffe
	dc.w COLOR00,$347

	dc.w $fc07,$fffe
	dc.w COLOR00,$335

	; line
	dc.w $ff07,$fffe
	dc.w COLOR00,$358

	; Mirror, mirror
	dc.w $ffdf,$fffe
	dc.w $0007,$fffe
	dc.w COLOR00,$335

	dc.w $04df,$fffe
	dc.w $108,SCREEN_BYWIDTH-SCREEN_VIEWPORT_BYPL-(SCREEN_BYWIDTH*2)
	dc.w $10a,SCREEN_BYWIDTH-SCREEN_VIEWPORT_BYPL-(SCREEN_BYWIDTH*2)
	; Mirrored textcolor
	dc.w COLOR00,$0335
	dc.w COLOR01,$0468
	dc.w COLOR02,$0235
	dc.w COLOR03,$0358
	dc.w COLOR04,$0689
	dc.w COLOR05,$069a
	dc.w COLOR06,$07bc
	dc.w COLOR07,$08dd

	; Plate below mirror

	; line
	dc.w $0707,$fffe
	dc.w COLOR00,$369
	dc.w COLOR01,$369
	dc.w COLOR02,$369
	dc.w COLOR03,$369
	dc.w COLOR04,$369
	dc.w COLOR05,$369
	dc.w COLOR06,$369
	dc.w COLOR07,$369

	dc.w $0807,$fffe
	dc.w COLOR00,$335
	dc.w COLOR01,$0468
	dc.w COLOR02,$0235
	dc.w COLOR03,$0358
	dc.w COLOR04,$0689
	dc.w COLOR05,$069a
	dc.w COLOR06,$07bc
	dc.w COLOR07,$08dd

	; line
	dc.w $1507,$fffe
	dc.w COLOR00,$37a
	dc.w COLOR01,$37a
	dc.w COLOR02,$37a
	dc.w COLOR03,$37a
	dc.w COLOR04,$37a
	dc.w COLOR05,$37a
	dc.w COLOR06,$37a
	dc.w COLOR07,$37a

	dc.w $1707,$fffe
	dc.w COLOR00,$335
	dc.w COLOR01,$0468
	dc.w COLOR02,$0235
	dc.w COLOR03,$0358
	dc.w COLOR04,$0689
	dc.w COLOR05,$069a
	dc.w COLOR06,$07bc
	dc.w COLOR07,$08dd

	; Plate side
	dc.w $2c07,$fffe
	dc.w COLOR00,$779
	dc.w $2d07,$fffe
	dc.w COLOR00,$568
	dc.w $2e07,$fffe
	dc.w COLOR00,$456
	dc.w $2f07,$fffe
	dc.w COLOR00,$335

	dc.w $3007,$fffe
	dc.w COLOR00,TEXT_BGCOLOR

	dc.w $ffff,$fffe

PIC_MARGIN_H = SCREEN_Y-(PICY+PICH)

pic: INCBIN "pic_130x75x3.raw"
pic_margin: dcb.b (PIC_MARGIN_H)*(PICBYWIDTH),0
pic_e:

font: INCBIN "FastCarFont.284x100x3"
font_e:
font_palette = font_e-8*2  ; 8 colors, each color is a word

Module1:
	incbin "P61.new_ditty"

	SECTION bss_c,bss_c
screen:
	ds SCREEN_BYWIDTH*SCREEN_HEIGHT/2
screen_e:

	END

Bit	ABC -> D
0	000    0
1	001    0
2	010    0
3	011    0
4	100    1
5	101    1
6	110    1
7	111    1
%11110000 = $f0
This goes to BLTCON0 LF7-0 bits.
