The simplest example of a multi-file project with function defined in func.s
and main program in main.s. Focal points:

  1. Main object must be linked first to be executed as the start code.
     Pass it to `vlink` as the first object file.
  
  2. Symbols that are to be visible outside of a .s file must be marked
     with either `xdef` or `public` directive.
