WaitRaster:
	; Wait for rasterline at d0.w. Modifies d0-d2/a0.
	move.l #$1ff00,d2
	lsl.l #8,d0
	and.l d2,d0
	lea $dff004,a0
.loop:
	move.l (a0),d1
	and.l d2,d1
	cmp.l d1,d0
	bne.s .loop
	rts

	public WaitRaster