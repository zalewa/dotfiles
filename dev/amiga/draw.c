#include <stdio.h>
#include <exec/types.h>
#include <graphics/gfx.h>
#include <intuition/intuition.h>
#include <dos/dos.h>

/* Prototypes for system functions. */
#include <proto/exec.h>
#include <proto/graphics.h>
#include <proto/intuition.h>
#include <proto/dos.h>

/* Prototypes for our functions. */
int program_init(void);
void program_loop(void);
void clean_up(void);

/* Global variables. */
struct GfxBase *GfxBase = NULL;
struct IntuitionBase *IntuitionBase = NULL;
struct Window *MyWindow = NULL;

int main(void)
{
	int error;

	error = program_init(); /* Open needed libraries and the main window. */

	if ( !error )
	{
		program_loop(); /* If everything was initialized, execute the main program code. */
	}

	clean_up(); /* We must clean up before we exit. */

	return 0;
}


int program_init(void)
{
	int result = 0;

	/* First open the graphics.library. */
	GfxBase = (struct GfxBase *)OpenLibrary( "graphics.library", 0L );

	if ( GfxBase != NULL)
	{
		/* Second open the intuition.library. */
		IntuitionBase = (struct IntuitionBase *)OpenLibrary( "intuition.library", 0L );

		/* Was the library opened? */
		if ( IntuitionBase != NULL )
		{
			/* Since the library was opened, we can open a window. */
			MyWindow = (struct Window *)OpenWindowTags( NULL,
				WA_Left, 20,
				WA_Top, 20,
				WA_Width, 200,
				WA_Height, 200,
				WA_Title, (ULONG)"My Window",
				WA_DepthGadget, TRUE,
				WA_CloseGadget, TRUE,
				WA_SizeGadget, TRUE,
				WA_DragBar, TRUE,
				WA_GimmeZeroZero, TRUE,
				WA_ReportMouse, TRUE,
				WA_IDCMP, IDCMP_CLOSEWINDOW | IDCMP_MOUSEBUTTONS | IDCMP_MOUSEMOVE,
				TAG_END );

			/* Was the window opened? */
			if ( MyWindow == NULL )
			{
				/* The window was not opened so display a message. */
				printf( "Unable to open the window!\n" );
				result = -1;
			}
		}
		else
		{
			/* The intuition.library was not opened so display a message. */
			printf( "Unable to open the intuition.library!\n" );
			result = -2;
		}
	}
	else
	{
		/* The graphics.library was not opened so display a message. */
		printf( "Unable to open the graphics.library!\n" );
		result = -3;
	}

	return result;
}


void clean_up(void)
{
	/* If the window is open, close it. */
	if ( MyWindow != NULL )
	{
		CloseWindow( MyWindow );
	}

	/* If the intuition.library is open, close it. */
	if ( IntuitionBase != NULL )
	{
		CloseLibrary( (struct Library *)IntuitionBase );
	}

	/* If the graphics.library is open, close it. */
	if ( GfxBase != NULL )
	{
		CloseLibrary( (struct Library *)GfxBase );
	}

	return;
}


void program_loop(void)
{
	ULONG signals;
	ULONG window_signal;
	struct IntuiMessage *message;
	UWORD msg_code;
	ULONG msg_class;
	BOOL end = FALSE;
	BOOL left_mouse_button = FALSE;

	/* Define the window signal. */
	window_signal = 1L << MyWindow->UserPort->mp_SigBit;

	/* Main program loop. */

	/* Wait until the close button is pressed. */
	while ( !end && MyWindow )
	{
		signals = Wait( window_signal );

		/* Check the signal bit for our message port. Will be true if these is a message. */
		if ( signals & window_signal )
		{
			WORD X_coord, Y_coord;

			/* There may be more than one message, so keep processing messages until there are no more. */
			while ( message = (struct IntuiMessage *)GetMsg(MyWindow->UserPort) )
			{
				/* Copy the necessary information from the message. */
				msg_class = message->Class;
				msg_code = message->Code;
				X_coord = message->MouseX - MyWindow->BorderLeft;
				Y_coord = message->MouseY - MyWindow->BorderTop;

				/* Reply as soon as possible. */
				ReplyMsg((struct Message *)message);

				/* Take the proper action in response to the message. */
				switch ( msg_class )
				{
				case IDCMP_CLOSEWINDOW: /* User pressed the close window gadget. */
					end = TRUE;
					break;
				case IDCMP_MOUSEBUTTONS: /* The status of the mouse buttons has changed. */
					switch ( msg_code )
					{
					case SELECTDOWN: /* The left mouse button has been pressed. */
						left_mouse_button = TRUE;
						break;
					case SELECTUP: /* The left mouse button has been released. */
						left_mouse_button = FALSE;
						break;
					}
				case IDCMP_MOUSEMOVE: /* The position of the mouse has changed. */
					if ( left_mouse_button == TRUE )
					{
						WritePixel( MyWindow->RPort, X_coord, Y_coord );
					}
					break;
				default:
					break;
				}
			}
		}
	}

	return;
}
