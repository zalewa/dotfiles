#include <proto/exec.h>
#include <dos/dos.h>
#include <proto/dos.h>
#include <workbench/workbench.h>

int __nocommandline=1;
int __initlibraries=0;

extern struct WBSStartup *_WBenchMsg;

int main() {
   if (_WBenchMsg == NULL) {
      if ((DOSBase=(struct DosLibrary*)OpenLibrary("dos.library",37))) {
         Write(Output(),"Hello world\n",12);
         CloseLibrary((struct Library*)DOSBase);
      }
   }
   return 0;
}

