#include <graphics/gfxbase.h>
#include <stdio.h>

void gfx() {
	struct GfxBase *gfx_base;

	printf("GFX");
	gfx_base = (struct GfxBase *) OpenLibrary("graphics.library", 33L);
	printf(" %p\n", gfx_base);
	printf("  ChipRevBits0: %x\n", gfx_base->ChipRevBits0);
	if (gfx_base->ChipRevBits0 && GFXF_HR_AGNUS)
		printf("    ECS Agnus\n");
	if (gfx_base->ChipRevBits0 && GFXF_HR_DENISE)
		printf("    ECS Denise\n");
	CloseLibrary((struct Library *) gfx_base);
}

int main() {
	gfx();
	return 0;
}
