#include <proto/intuition.h>
#include <proto/dos.h>
#include <intuition/screens.h>

#define SECOND 50

int main(void)
{
	struct NewScreen screen1;
	struct Screen *screen = 0;

	screen = OpenScreenTags(0,
		SA_Left, 10, SA_Top, 10,
		SA_Width, 620, SA_Height, 200,
		SA_Depth, 8, SA_Title, "Tagged screen",
		SA_Type, PUBLICSCREEN,
		SA_SysFont, 1,
		TAG_DONE);
	DisplayBeep(screen);
	Delay(5 * SECOND);
	if (screen)
		CloseScreen(screen);
	printf("Done\n");

	return 0;
}
