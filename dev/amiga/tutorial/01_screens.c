#include <proto/intuition.h>
#include <proto/dos.h>
#include <intuition/screens.h>
#include <stdio.h>
#include <stdlib.h>

#define SECOND 50

int main(void)
{
	struct NewScreen screen1;
	struct Screen *screen = 0;

	memset(&screen1, 0, sizeof(screen1));

	screen1.LeftEdge = 20;
	screen1.TopEdge = 20;
	screen1.Width = 400;
	screen1.Height = 100;
	screen1.Depth = 8;
	screen1.DetailPen = DETAILPEN;
	screen1.BlockPen = BLOCKPEN;
	screen1.ViewModes = HIRES;
	screen1.Type = PUBLICSCREEN;
	screen1.DefaultTitle = "My New Screen";

	printf("Creating screen\n");
	screen = (struct Screen *) OpenScreen(&screen1);
	printf("Screen addr: %p\n");
	Delay(1 * SECOND);
	DisplayBeep(screen);
	Delay(5 * SECOND);
	if (screen)
		CloseScreen(screen);
	printf("Done\n");

	return 0;
}
