#include <proto/intuition.h>
#include <proto/dos.h>
#include <intuition/intuition.h>
#include <intuition/screens.h>


int main(void)
{
	struct Window *window = 0;
	
	window = OpenWindowTags(0,
		WA_Left, 0, WA_Top, 0,
		WA_Width, 200, WA_Height, 150,
		WA_IDCMP, IDCMP_CLOSEWINDOW,
		WA_Flags, WFLG_SIZEGADGET | WFLG_DRAGBAR | WFLG_DEPTHGADGET 
			| WFLG_CLOSEGADGET | WFLG_ACTIVATE,
		WA_Title, "My Window",
		WA_PubScreenName, "Workbench",
		TAG_DONE);
	Delay(3 * 50);
	if (window)
		CloseWindow(window);
	return 0;
}
