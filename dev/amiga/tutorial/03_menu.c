#include <proto/intuition.h>
#include <proto/dos.h>
#include <intuition/intuition.h>
#include <intuition/screens.h>


struct IntuiText textHi = {0, 1, JAM2, 4, 2, 0, "Hi", 0};
struct IntuiText textQuit = {2, 3, JAM2, 4, 2, 0, "Quit", 0};

struct MenuItem itemQuit = {0, 10, 25, 48, 40, 
	ITEMTEXT | ITEMENABLED | HIGHCOMP,
	0, &textQuit, &textQuit, 0, 0, 0};
struct MenuItem itemHi = {&itemQuit, 0, 0, 48, 12,
	ITEMTEXT | ITEMENABLED | HIGHCOMP,
	0, &textHi, &textHi, 0, 0, 0};

struct Menu menu = {0, 0, 0, 48, 12, MENUENABLED, 
	"File", &itemHi, 0, 0, 0, 0};

int main(void)
{
	struct Window *window = 0;
	int keeprun = TRUE;
	struct IntuiMessage *msg;
	ULONG msgClass;

	window = OpenWindowTags(0,
		WA_Left, 0, WA_Top, 0,
		WA_Width, 200, WA_Height, 150,
		WA_IDCMP, IDCMP_CLOSEWINDOW,
		WA_Flags, WFLG_SIZEGADGET | WFLG_DRAGBAR | WFLG_DEPTHGADGET 
			| WFLG_CLOSEGADGET | WFLG_ACTIVATE,
		WA_Title, "My Window",
		WA_PubScreenName, "Workbench",
		TAG_DONE);

	/* Attach menu to window */
	SetMenuStrip(window, &menu);

	while (keeprun == TRUE) {
		/* Wait for an event */
		Wait(1L << window->UserPort->mp_SigBit);
		/* Get message data */
		msg = GT_GetIMsg(window->UserPort);
		msgClass = msg->Class;
		/* Pop message from queue */
		GT_ReplyIMsg(msg);
		if (msgClass == IDCMP_CLOSEWINDOW) {
			if (window) {
				ClearMenuStrip(window);
				CloseWindow(window);
			}
			keeprun = FALSE;
		}
	}
	return 0;
}
