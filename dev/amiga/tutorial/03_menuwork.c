#include <proto/intuition.h>
#include <proto/dos.h>
#include <intuition/intuition.h>
#include <intuition/screens.h>


struct IntuiText textHi = {0, 1, JAM2, 4, 2, 0, "Hi", 0};
struct IntuiText textQuit = {2, 3, JAM2, 4, 2, 0, "Quit", 0};

struct MenuItem itemQuit = {0, 0, 12, 48, 12, 
	ITEMTEXT | ITEMENABLED | HIGHCOMP,
	0, &textQuit, &textQuit, 0, 0, 0};
struct MenuItem itemHi = {&itemQuit, 0, 0, 48, 12,
	ITEMTEXT | ITEMENABLED | HIGHCOMP,
	0, &textHi, &textHi, 0, 0, 0};

/* for printing */
UBYTE result[255];
struct IntuiText winText = {1, 0, JAM1, 0, 0, 0, &result[0], 0};


struct Menu menu = {0, 0, 0, 48, 12, MENUENABLED, 
	"File", &itemHi, 0, 0, 0, 0};

int main(void)
{
	struct Window *window = 0;
	int keeprun = TRUE;
	struct IntuiMessage *msg;
	UWORD menuNumber;
	UWORD menuNum;
	UWORD itemNum;
	UWORD subNum;
	ULONG msgClass;
	ULONG line = 12;	

	window = OpenWindowTags(0,
		WA_Left, 0, WA_Top, 10,
		WA_Width, 300, WA_Height, 150,
		WA_IDCMP, IDCMP_CLOSEWINDOW | IDCMP_MENUPICK,
		WA_Flags, WFLG_SIZEGADGET | WFLG_DRAGBAR | WFLG_DEPTHGADGET 
			| WFLG_CLOSEGADGET | WFLG_ACTIVATE,
		WA_Title, "My Window",
		WA_PubScreenName, "Workbench",
		TAG_DONE);

	/* Attach menu to window */
	SetMenuStrip(window, &menu);

	while (keeprun == TRUE) {
		/* Wait for an event */
		Wait(1L << window->UserPort->mp_SigBit);
		/* Get message data */
		msg = GT_GetIMsg(window->UserPort);
		msgClass = msg->Class;
		/* Pop message from queue */
		GT_ReplyIMsg(msg);
		switch (msgClass) {
		case IDCMP_CLOSEWINDOW:
			keeprun = FALSE;
			break;
		case IDCMP_MENUPICK:
			menuNumber = msg->Code; /* Get menu number */
			/* Split menu number into menu coordinates */
			menuNum = MENUNUM(menuNumber);
			itemNum = ITEMNUM(menuNumber);
			subNum = SUBNUM(menuNumber);
			sprintf(result, "Menu %d Item %d Subitem %d",
				menuNum, itemNum, subNum);
			PrintIText(window->RPort, &winText, 5, line);
			line += 10; /* next line */
			if (menuNum == 0 && itemNum == 1) /* quit */
				keeprun = FALSE;
			break;
		}
	}
	Delay(50);
	if (window) {
		ClearMenuStrip(window);
		CloseWindow(window);
	}
	return 0;
}
