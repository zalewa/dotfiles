#include <proto/intuition.h>
#include <proto/gadtools.h>
#include <proto/graphics.h>
#include <proto/layers.h>
#include <proto/exec.h>
#include <proto/dos.h>
#include <intuition/intuition.h>
#include <stdio.h>
#include <stdlib.h>

enum ProgramMode
{
	PM_Rects = 1,
	PM_Corridor = 2
};

ULONG randUlong(ULONG min, ULONG max)
{
	ULONG len = max - min + 1;
	return min + (ULONG)(rand() % len);
}

long randLong(long min, long max)
{
	long len = max - min + 1;
	return min + (long)(rand() % len);
}

int drawArea(struct RastPort *rastPort, WORD width, WORD height, WORD startx, WORD starty)
{
	struct AreaInfo areaInfo = {0};
	WORD areaBuf[200];
	struct TmpRas tmpRas;
	APTR tmpBuf;
	int i;
	LONG areaSize = width * height * 8;

	/* Clear */
	memset(&areaBuf[0], 0, sizeof(areaBuf));

	/* Set AreaInfo onto RastPort */
	rastPort->AreaInfo = &areaInfo;

	/* Allocate temp area for raster to work in */
	if (!(tmpBuf = (APTR)AllocMem(areaSize, MEMF_CHIP | MEMF_CLEAR)))
		return 5;
	InitTmpRas(&tmpRas, tmpBuf, areaSize);
	rastPort->TmpRas = &tmpRas;

	/* Set foreground color */
	InitArea(&areaInfo, areaBuf, width * 2 / 5);
	SetAPen(rastPort, 3);
	SetOutlinePen(rastPort, 1);

	/* Draw a corridor */
	AreaMove(rastPort, startx + 90, starty + 12);
	AreaDraw(rastPort, startx + 90, starty + 40);
	AreaDraw(rastPort, startx + 30, starty + 135);
	AreaDraw(rastPort, startx + 30, starty + 75);
	AreaEnd(rastPort);

	SetAPen(rastPort, 6);
	SetOutlinePen(rastPort, 6);
	AreaMove(rastPort, startx + 4,  starty + 75);
	AreaDraw(rastPort, startx + 30, starty + 75);
	AreaDraw(rastPort, startx + 30, starty + 135);
	AreaDraw(rastPort, startx + 4,  starty + 135);
	AreaEnd(rastPort);

	SetAPen(rastPort, 3);
	SetOutlinePen(rastPort, 3);
	AreaMove(rastPort, startx + 110, starty + 12);
	AreaDraw(rastPort, startx + 170, starty + 75);
	AreaDraw(rastPort, startx + 170, starty + 135);
	AreaDraw(rastPort, startx + 110, starty + 40);
	AreaEnd(rastPort);

	SetAPen(rastPort, 6);
	SetOutlinePen(rastPort, 6);
	AreaMove(rastPort, startx + 170, starty + 75);
	AreaDraw(rastPort, startx + 195, starty + 75);
	AreaDraw(rastPort, startx + 195, starty + 135);
	AreaDraw(rastPort, startx + 170, starty + 135);
	AreaEnd(rastPort);

	/* Finish off with some lines */
	SetAPen(rastPort, 1);
	Move(rastPort, 90, 40);
	Draw(rastPort, 110, 40);
	Move(rastPort, 30, 135);
	Draw(rastPort, 170, 135);

	FreeMem(tmpBuf, areaSize);

	return 0;
}

void drawRects(struct RastPort *rastPort)
{
	long startx, starty, width, height;
	int num;
	for (num = 1; num <= 10; num++) {
		/* Foreground color */
		SetAPen(rastPort, randUlong(1, 7));
		/* Coordinates */
		startx = randLong(10, 60);
		starty = randLong(10, 60);
		width = height = randLong(20, 70);
		/* Draw */
		Move(rastPort, startx, starty);
		Draw(rastPort, startx + width, starty);
		Draw(rastPort, startx + width, starty + height);
		Draw(rastPort, startx, starty + height);
		Draw(rastPort, startx, starty);
	}
}

int main(int argc, char **argv)
{
	struct Window *window;
	struct RastPort *rastPort;
	int keeprun = TRUE;
	struct IntuiMessage *msg;
	ULONG msgClass;
	enum ProgramMode mode = PM_Rects;

	if (argc < 2) {
		printf("%s <1|2> -- 1 - rects, 2 - corridor\n", argv[0]);
		return 1;
	}

	if (sscanf(argv[1], "%d", &mode) != 1) {
		printf("failed to read mode\n");
		return 1;
	}

	window = OpenWindowTags(NULL,
		WA_Left, 20, WA_Top, 20,
		WA_Width, 300, WA_Height, 190,
		WA_IDCMP, IDCMP_CLOSEWINDOW | IDCMP_REFRESHWINDOW,
		WA_Flags, WFLG_SIZEGADGET | WFLG_DRAGBAR | WFLG_DEPTHGADGET
			| WFLG_CLOSEGADGET | WFLG_ACTIVATE | WFLG_SMART_REFRESH,
		WA_Title, (long)"Lines",
		WA_PubScreenName, (long)"Workbench",
		TAG_DONE);
	rastPort = window->RPort;

	if (mode == PM_Corridor) {
		WORD startx = 0;
		WORD starty = 0;
		for (; startx < 170; startx += 50, starty += 20) {
			int ec = drawArea(rastPort, window->Width, window->Height, startx, starty);
			if (ec != 0) {
				CloseWindow(window);
				return ec;
			}
		}
	} else if (mode == PM_Rects) {
		drawRects(rastPort);
	} else {
		printf("unknown mode %d\n", mode);
		CloseWindow(window);
		return 1;
	}

	while (keeprun == TRUE) {
		Wait(1L << window->UserPort->mp_SigBit);
		msg = GT_GetIMsg(window->UserPort);
		msgClass = msg->Class;
		GT_ReplyIMsg(msg);
		printf("Got me sum msg: %d\n", msgClass);
		switch (msgClass) {
		case IDCMP_CLOSEWINDOW:
			keeprun = FALSE;
			break;
		case IDCMP_REFRESHWINDOW:
			printf("RefreshWindowFrame\n");
			RefreshWindowFrame(window);
			break;
		}
	}

	CloseWindow(window);

	return 0;
}
