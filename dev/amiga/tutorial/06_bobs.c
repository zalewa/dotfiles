/*
  Warning! This program seems to crash WinUAE 3.6.0, WB 3.1, Kickstart 3.0.
  Works on actual Amiga 1200 tho, but the same
  glitches occur.
 */
#include <proto/intuition.h>
#include <proto/graphics.h>
#include <intuition/intuition.h>
#include <graphics/gels.h>
#include <stdio.h>

#include <animtools/animtools.h>

#include "allocchip.h"

/*
  Bob setup that uses two alternate images.
 */

/* number of lines in the bob */
#define GEL_SIZE 4
#define BOB_SIZE 2 * 2 * GEL_SIZE

WORD bob_data1[BOB_SIZE] = {
	/* plane 1 */
	0xffff, 0x0003, 0xfff0, 0x0003, 0xfff0, 0x0003, 0xffff, 0x0003,
	/* plane 2 */
	0x3fff, 0xfffc, 0x3ff0, 0x0ffc, 0x3ff0, 0x0ffc, 0x3fff, 0xfffc
};
WORD *bob_chip1;

WORD bob_data2[BOB_SIZE] = {
	/* plane 1 */
	0xc000, 0xffff, 0xc000, 0x0fff, 0xc000, 0x0fff, 0xc000, 0xffff,
	/* plane 2 */
	0x3fff, 0xfffc, 0x3ff0, 0x0ffc, 0x3ff0, 0x0ffc, 0x3fff, 0xfffc
};
WORD *bob_chip2;

NEWBOB new_bob = { /* NEWBOB supposedly defined in animtools.h */
	/* initial image, WORD width, line height */
	NULL, 2, GEL_SIZE,
	/* image depth, plane pick, plane on/off, VSprite flags */
	2, 3, 0, SAVEBACK | OVERLAY,
	/* dbuf (0=false), raster depth, x, y, hit mask, me mask */
	0, 2, 160, 100, 0, 0
};

void bob_draw_glist(struct RastPort *rport, struct ViewPort *vport)
{
	/* This function sorts the gel list by its x,y co-ords,
	   required before DrawGList */
	SortGList(rport);
	/* Draw all the gels including bobs and vsprites */
	DrawGList(rport, vport);
	/* Wait for next top of frame refresh interval */
	WaitTOF();
}

void process_window(struct Window *win, struct Bob *bob)
{
	struct IntuiMessage *msg;

	for (;;) {
		Wait(1 << win->UserPort->mp_SigBit);
		while (NULL != (msg = (struct IntuiMessage *) GetMsg(win->UserPort))) {
			switch (msg->Class) {
			case IDCMP_CLOSEWINDOW:
				ReplyMsg(msg);
				return;
			case IDCMP_INTUITICKS:
				bob->BobVSprite->X = msg->MouseX + 20;
				bob->BobVSprite->Y = msg->MouseY + 1;
				ReplyMsg(msg);
				break;
			}
		}
		/* Now change image data to do animation */
		bob->BobVSprite->ImageData = (bob->BobVSprite->ImageData == bob_chip1) ? bob_chip2 : bob_chip1;
		/* Set up masks for new image (do what? ~ Robikz) */
		InitMasks(bob->BobVSprite);
		bob_draw_glist(win->RPort, ViewPortAddress(win));
	}
}

int do_bob(struct Window *win)
{
	struct Bob *bob;
	struct GelsInfo *ginfo;

	/* Why 0x03? ~ Robikz*/
	if (NULL == (ginfo = (struct GelsInfo *) setupGelSys(win->RPort, 0x03)))
		return RETURN_WARN;
	else {
		if (NULL == (bob = (struct Bob *) makeBob(&new_bob)))
			return RETURN_WARN;
		else {
			/* Add the bob to the window's rastport ready for display */
			AddBob(bob, win->RPort);
			/* Display the gels (bobs) in the given window and viewport */
			bob_draw_glist(win->RPort, ViewPortAddress(win));
			/* do animation in this function */
			process_window(win, bob);

			/* Clean up */
			RemBob(bob);
			bob_draw_glist(win->RPort, ViewPortAddress(win));
			freeBob(bob, new_bob.nb_RasDepth);
		}
		cleanupGelSys(ginfo, win->RPort);
	}
}

int main(void)
{
	int ec = 0;
	struct Window *window = NULL;

	bob_chip1 = (WORD *)AllocChip(bob_data1, BOB_SIZE);
	bob_chip2 = (WORD *)AllocChip(bob_data2, BOB_SIZE);
	new_bob.nb_Image = bob_chip2;

	window = OpenWindowTags(NULL,
		WA_Left, 80, WA_Top, 20, WA_Width, 400, WA_Height, 150,
		WA_IDCMP, IDCMP_CLOSEWINDOW | IDCMP_INTUITICKS,
		WA_Flags, WFLG_ACTIVATE | WFLG_CLOSEGADGET | WFLG_DEPTHGADGET
			| WFLG_RMBTRAP | WFLG_GIMMEZEROZERO,
		WA_Title, (LONG)"Bob",
		WA_PubScreenName, (LONG)"Workbench",
		TAG_DONE);
	if (!window) {
		ec = RETURN_ERROR;
		fprintf(stderr, "cannot create window\n");
		goto mainexit;
	}

	ec = do_bob(window);

mainexit:
	/* Your standard cleanup */
	if (window)
		CloseWindow(window);
	FreeChip(bob_chip1, BOB_SIZE);
	FreeChip(bob_chip2, BOB_SIZE);

	return ec;
}
