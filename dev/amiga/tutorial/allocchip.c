/*
  Public Domain.
  Authored by Robert "Robikz" Zalewski 2018.
 */
#include "allocchip.h"

#include <exec/memory.h>

void *AllocChip(void *src, ULONG size)
{
	void *ptr = (void *)AllocMem(size, MEMF_CHIP);
	if (ptr)
		CopyMem(src, ptr, size);
	return ptr;
}

void FreeChip(void *ptr, ULONG size)
{
	FreeMem(ptr, size);
}
