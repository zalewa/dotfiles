/*
  Public Domain.
  Authored by Robert "Robikz" Zalewski 2018.
 */

/*
  AllocChip - Allocate chip memory and copy data over
  from source pointer.
 */
#include <exec/types.h>

void *AllocChip(void *src, ULONG size);
void FreeChip(void *ptr, ULONG size);
