#include <graphics/gfxbase.h>
#include <intuition/intuition.h>
#include <intuition/screens.h>

extern struct GfxBase *GfxBase;
struct Screen *screen = NULL;
struct Window *window = NULL;

BOOL createWindow() {
	struct NewScreen newScreen = {
		0, 0, STDSCREENWIDTH, STDSCREENHEIGHT, 8, 0, 0,
		HIRES, CUSTOMSCREEN | SCREENQUIET,
		NULL, NULL, NULL, NULL
	};

	screen = (struct Screen *) OpenScreen(&newScreen);
	if (!screen) {
		printf("cannot create screen\n");
		return FALSE;
	}

	window = (struct Window *) OpenWindowTags(NULL,
		WA_Left, 0, WA_Top, 0,
		WA_Width, screen->Width, WA_Height, screen->Height,
		WA_CustomScreen, screen,
		WA_IDCMP, IDCMP_CLOSEWINDOW | IDCMP_MOUSEBUTTONS | IDCMP_VANILLAKEY | IDCMP_RAWKEY,
		WA_Flags, WFLG_CLOSEGADGET | WFLG_BORDERLESS | WFLG_RMBTRAP | WFLG_ACTIVATE,
		TAG_DONE
		);
	if (!window) {
		printf("cannot create window\n");
		return FALSE;
	}

	return TRUE;
}

void closeWindow() {
	if (window) {
		CloseWindow(window);
		window = NULL;
	}
	if (screen) {
		CloseScreen(screen);
		screen = NULL;
	}
}

void run() {
	BOOL keepRunning = TRUE;
	struct IntuiMessage *msg;

	while (keepRunning) {
		while (msg = (struct IntuiMessage *) GetMsg(window->UserPort)) {
			printf("got message %d %d\n", msg->Class, msg->Code);
			switch (msg->Class) {
			case VANILLAKEY:
				switch (msg->Code) {
				case 'q':
					keepRunning = FALSE;
					break;
				}
				break;
			case CLOSEWINDOW:
				keepRunning = FALSE;
				break;
			}
			ReplyMsg((struct Message *) msg);
		}
	}
}

int main(int argc, char **argv) {
	int exitcode = 1;
	if (!createWindow()) {
		goto fail;
	}
	run();
	exitcode = 0;
fail:
	closeWindow();
	return 0;
}
