#!/usr/bin/env python3
from nose.tools import assert_equal


def manhattan(pos1, pos2):
    deltas = [abs(c1 - c2) for c1, c2 in zip(pos1, pos2)]
    return sum(deltas)


def test_manhattan():
    assert_equal(0, manhattan((4, 7), (4, 7)))
    assert_equal(1, manhattan((5, 7), (4, 7)))
    assert_equal(1, manhattan((4, 7), (4, 6)))
    assert_equal(2, manhattan((5, 7), (4, 6)))
    assert_equal(22, manhattan((-1, -1), (10, 10)))


def idx_to_coords(idx):
    maximum = (0, 0)
    directions = [(1, 0), (0, 1), (-1, 0), (0, -1)]
    direction_idx = 0
    point = (0, 0)
    point_idx = 1
    while point_idx < idx:
        direction = directions[direction_idx]
        new_point = (
            point[0] + direction[0],
            point[1] + direction[1]
            )
        if abs(new_point[0]) > maximum[0] or abs(new_point[1]) > maximum[1]:
            if direction_idx == 0:
                point = new_point
                point_idx += 1
                maximum = (maximum[0] + 1, maximum[1] + 1)
            direction_idx = (direction_idx + 1) % len(directions)
        else:
            point = new_point
            point_idx += 1
    return point


def test_idx_to_coords():
    assert_equal((0, 0), idx_to_coords(1))
    assert_equal((1, 0), idx_to_coords(2))
    assert_equal((1, 1), idx_to_coords(3))
    assert_equal((0, 1), idx_to_coords(4))
    assert_equal((1, -1), idx_to_coords(9))
    assert_equal((-2, 2), idx_to_coords(17))
    assert_equal((2, 1), idx_to_coords(12))
    assert_equal((-1, -2), idx_to_coords(22))
    assert_equal((0, -2), idx_to_coords(23))


def memory_manhattan(idx):
    return manhattan(idx_to_coords(1), idx_to_coords(idx))


def test_memory_manhattan():
    assert_equal(0, memory_manhattan(1))
    assert_equal(3, memory_manhattan(12))
    assert_equal(2, memory_manhattan(23))
    assert_equal(31, memory_manhattan(1024))


def sumgrow(minimum):
    value = 1
    idx = 1
    fields = {idx_to_coords(idx): value}
    neighbours_vectors = [
        (-1, 1), (0, 1), (1, 1),
        (-1, 0), (1, 0),
        (-1, -1), (0, -1), (1, -1)
    ]
    while value <= minimum:
        idx += 1
        coords = idx_to_coords(idx)
        neighbours = []
        for vector in neighbours_vectors:
            potential_neighbour = (
                coords[0] + vector[0],
                coords[1] + vector[1])
            if potential_neighbour in fields:
                neighbours.append(potential_neighbour)
        value = sum([fields[ncoords] for ncoords in neighbours])
        fields[coords] = value
    return value


def test_sumgrow():
    assert_equal(122, sumgrow(100))
    assert_equal(122, sumgrow(121))
    assert_equal(133, sumgrow(122))
    assert_equal(1, sumgrow(0))
    assert_equal(2, sumgrow(1))
    assert_equal(4, sumgrow(2))
    assert_equal(4, sumgrow(3))
    assert_equal(23, sumgrow(20))


def main():
    print(memory_manhattan(265149))
    print(sumgrow(265149))


if __name__ == "__main__":
    main()
