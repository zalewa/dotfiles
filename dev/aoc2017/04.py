#!/usr/bin/env python3
from nose.tools import assert_true, assert_false


def has_duplicate_word(passphrase):
    tokens = passphrase.split(" ")
    return len(tokens) != len(set(tokens))


def test_has_duplicate_word():
    assert_false(has_duplicate_word("aa bb cc dd ee"))
    assert_true(has_duplicate_word("aa bb cc dd aa"))
    assert_false(has_duplicate_word("aa bb cc dd aaa"))


def has_anagram(passphrase):
    words = passphrase.split(" ")
    counts = []
    for word in words:
        letters = list(word)
        count = {}
        for letter in letters:
            count.setdefault(letter, 0)
            count[letter] += 1
        if count in counts:
            return True
        counts.append(count)
    return False


def test_has_anagram():
    assert_false(has_anagram("abcde fghij"))
    assert_true(has_anagram("abcde xyz ecdab"))
    assert_false(has_anagram("a ab abc abd abf abj"))
    assert_false(has_anagram("iiii oiii ooii oooi oooo"))
    assert_true(has_anagram("oiii ioii iioi iiio"))


def count_valid(stream, is_valid):
    count = 0
    for line in stream:
        if is_valid(line.rstrip()):
            count += 1
    return count


def is_valid_p1(passphrase):
    return not has_duplicate_word(passphrase)


def is_valid_p2(passphrase):
    return not has_anagram(passphrase)


def main():
    with open("04.txt", "r") as f:
        print(count_valid(f, is_valid_p1))
    with open("04.txt", "r") as f:
        print(count_valid(f, is_valid_p2))


if __name__ == "__main__":
    main()
