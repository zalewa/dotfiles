#!/usr/bin/env python
from copy import copy
from nose.tools import eq_

class Jump:
    def __init__(self, jumps):
        self.jumps = copy(jumps)
        self.index = 0
        self.count = 0

    def jump(self):
        if self.is_outside():
            return False
        current_index = self.index
        offset = self.jumps[current_index]
        self.index += offset
        self.jumps[current_index] = self._nextoffset(offset)
        self.count += 1
        return True

    def is_outside(self):
        return self.index < 0 or self.index >= len(self.jumps)

    def _nextoffset(self, offset):
        return offset + 1


class StrangerJump(Jump):
    def _nextoffset(self, offset):
        if offset >= 3:
            return offset - 1
        else:
            return offset + 1


def _jump_loop(jump, hard_limit=1000):
    limit_counter = 0
    while jump.jump() and limit_counter < hard_limit:
        limit_counter += 1
    return jump


def test_jump_endstate():
    def _jump(jumps):
        j = _jump_loop(Jump(jumps))
        return j.jumps

    eq_([2, 5, 0, 1, -2], _jump([0, 3, 0, 1, -3]))
    eq_([2], _jump([0]))
    eq_([2], _jump([1]))
    eq_([0], _jump([-1]))
    eq_([999], _jump([998]))


def test_jump_count():
    def _jump(jumps):
        j = _jump_loop(Jump(jumps))
        return j.count

    eq_(5, _jump([0, 3, 0, 1, -3]))
    eq_(1, _jump([1]))
    eq_(2, _jump([0]))
    eq_(2, _jump([1, 1]))
    eq_(1, _jump([-1]))
    eq_(1, _jump([2, 1]))
    eq_(2, _jump([2, 1, -3]))


def test_stranger_jump():
    j = _jump_loop(StrangerJump([0, 3, 0, 1, -3]))
    eq_(10, j.count)
    eq_([2, 3, 2, 3, -1], j.jumps)


def readinput(stream):
    return [int(token.strip()) for token in stream]


def main():
    with open("05.txt", "r") as f:
        jumps = readinput(f)
    jump = Jump(jumps)
    while jump.jump():
        pass
    print(jump.count)

    stranger_jump = StrangerJump(jumps)
    while stranger_jump.jump():
        pass
    print(stranger_jump.count)


if __name__ == "__main__":
    main()
