#!/usr/bin/env python3
from copy import copy
from nose.tools import eq_


class Memory:
    def __init__(self, banks):
        self.banks = copy(banks)

    def rearrange(self):
        old_states = [copy(self.banks)]
        redist = Redistributor()
        while True:
            self.banks = redist.redistribute(self.banks)
            if self.banks in old_states:
                return len(old_states)
            old_states.append(copy(self.banks))


class Redistributor:
    def redistribute(self, banks):
        banks = copy(banks)
        idx = self._find_max(banks)
        blocks = banks[idx]
        banks[idx] = 0
        while blocks > 0:
            idx += 1
            banks[idx % len(banks)] += 1
            blocks -= 1
        return banks

    def _find_max(self, banks):
        indexed = enumerate(banks)
        return max(indexed, key=lambda e: e[1])[0]


def test_redistribute():
    def _redist(banks):
        return Redistributor().redistribute(banks)

    eq_([2, 4, 1, 2], _redist([0, 2, 7, 0]))
    eq_([3, 1, 2, 3], _redist([2, 4, 1, 2]))
    eq_([0, 2, 3, 4], _redist([3, 1, 2, 3]))
    eq_([1, 3, 4, 1], _redist([0, 2, 3, 4]))
    eq_([2, 4, 1, 2], _redist([1, 3, 4, 1]))


def test_memory_rearrange():
    mem = Memory([0, 2, 7, 0])
    eq_(5, mem.rearrange())


def main():
    memory = Memory(BANKS)
    print(memory.rearrange())  # part 1
    print(memory.rearrange())  # part 2


BANKS = [0, 5, 10, 0, 11, 14, 13, 4, 11, 8, 8, 7, 1, 4, 12, 11]

if __name__ == "__main__":
    main()
