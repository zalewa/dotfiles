#!/usr/bin/env python3
import re
import sys
from copy import copy
from nose.tools import eq_, assert_false, assert_true


class Program:
    def __init__(self, name, weight, children=None):
        self.name = name
        self.weight = weight
        self.children = children or []

    @classmethod
    def parse(cls, line):
        try:
            pattern = re.compile(
                r'^(?P<name>[a-z]+) \((?P<weight>\d+)\)( -> (?P<children>.*))?$')
            match = pattern.match(line)
            children_string = match.group("children")
            if children_string:
                children = [child.strip() for child in children_string.split(",")]
            else:
                children = []
            return Program(
                name=match.group('name'),
                weight=int(match.group('weight')),
                children=children
            )
        except Exception as e:
            raise RuntimeError("{}: {}".format(line, e)) from e


def test_program_parse():
    p1 = Program.parse('pbga (66)')
    eq_('pbga', p1.name)
    eq_(66, p1.weight)
    eq_([], p1.children)

    p2 = Program.parse('fwft (72) -> ktlj, cntj, xhth')
    eq_('fwft', p2.name)
    eq_(72, p2.weight)
    eq_(['ktlj', 'cntj', 'xhth'], p2.children)


class Tree:
    def __init__(self, programs):
        self.programs = programs

    @property
    def root(self):
        for program in self.programs:
            if self.parent(program) is None:
                return program
        return None

    @property
    def weight(self):
        return sum([p.weight for p in self.programs])

    def direct_subtrees(self):
        return [self.subtree(child) for child in self.root.children]

    def subtree(self, program):
        program = self.program(self._program_name(program))
        all_subprograms = [program]
        idx = 0
        while idx < len(all_subprograms):
            children = self._direct_children(all_subprograms[idx])
            all_subprograms += children
            idx += 1
        return Tree(all_subprograms)

    def _direct_children(self, program):
        children = []
        for child in program.children:
            children.append(self.program(child))
        return children

    def parent(self, program):
        program = self._program_name(program)
        for candidate in self.programs:
            if program in candidate.children:
                return candidate
        return None

    def program(self, name):
        for program in self.programs:
            if program.name == name:
                return program
        return None

    def is_balanced(self):
        prev_weight = None
        for subtree in self.direct_subtrees():
            if prev_weight is None:
                prev_weight = subtree.weight
            elif prev_weight != subtree.weight:
                return False
        return True

    def find_disbalance(self):
        for subtree in self.direct_subtrees():
            diff = subtree.find_disbalance()
            if diff[0]:
                return diff
        # Disbalance must be in this tree.
        prev_weight = None
        all_weights = []
        all_total_weights = []
        for child in self.root.children:
            child = self.program(child)
            all_weights.append(child.weight)
            all_total_weights.append(self.subtree(child).weight)
        if len(set(all_total_weights)) > 1:
            diffs = list(set(all_total_weights))
            delta = diffs[0] - diffs[1]
            return delta, all_weights, all_total_weights
        else:
            return 0, [], []

    def _program_name(self, program):
        if hasattr(program, "name"):
            return program.name
        else:
            return program


def test_find_root():
    eq_('tknk', Tree(TEST_PROGRAMS).root.name)


def test_subtree_weights():
    main_tree = Tree(TEST_PROGRAMS)
    ugml_tree = main_tree.subtree('ugml')
    eq_(251, ugml_tree.weight)

    padx_tree = main_tree.subtree('padx')
    eq_(243, padx_tree.weight)

    fwft_tree = main_tree.subtree('fwft')
    eq_(243, fwft_tree.weight)


def test_is_balanced():
    main_tree = Tree(TEST_PROGRAMS)
    assert_false(main_tree.is_balanced())
    assert_true(main_tree.subtree('ugml').is_balanced())
    assert_true(main_tree.subtree('padx').is_balanced())
    assert_true(main_tree.subtree('fwft').is_balanced())


def test_find_disbalance():
    main_tree = Tree(TEST_PROGRAMS)
    delta, all_weights, all_total_weights = main_tree.find_disbalance()
    eq_(8, abs(delta))
    eq_([68, 45, 72], all_weights)
    eq_([251, 243, 243], all_total_weights)


def main():
    with open("07.txt", "r") as f:
        lines = [line.strip() for line in f]
        programs = [Program.parse(line) for line in lines if line]
    tree = Tree(programs)
    print(tree.root.name)
    print(tree.find_disbalance())


if __name__ == "__main__":
    main()


TEST_DATA = '''
pbga (66)
xhth (57)
ebii (61)
havc (66)
ktlj (57)
fwft (72) -> ktlj, cntj, xhth
qoyq (66)
padx (45) -> pbga, havc, qoyq
tknk (41) -> ugml, padx, fwft
jptl (61)
ugml (68) -> gyxo, ebii, jptl
gyxo (61)
cntj (57)
'''

TEST_PROGRAMS = [Program.parse(line.strip())
                 for line in TEST_DATA.split('\n') if line.strip()]
