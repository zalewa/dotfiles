#!/usr/bin/env python3
import re
from nose.tools import eq_


class Cpu:
    def __init__(self):
        self.registers = {}
        self.all_time_max = None

    def get_register(self, name):
        return self.registers.get(name, 0)

    def set_register(self, name, value):
        if self.all_time_max is None:
            self.all_time_max = value
        else:
            self.all_time_max = max(self.all_time_max, value)
        self.registers[name] = value

    @property
    def maxval(self):
        return max(self.registers.values())


class Op:
    def __init__(self, register, value, condition, how):
        self.register = register
        self.value = value
        self.condition = condition
        self.how = how

    def run(self, cpu):
        if self.condition.check(cpu):
            regval = cpu.get_register(self.register)
            regval = self.how(regval, self.value)
            cpu.set_register(self.register, regval)


class Cond:
    def __init__(self, register, value, how):
        self.register = register
        self.value = value
        self.how = how

    def check(self, cpu):
        return self.how(cpu.get_register(self.register), self.value)


class Interpreter:
    pattern = re.compile(r"(?P<reg>\S+) (?P<op>\S+) (?P<value>\S+)")

    def __init__(self, code):
        self.code = code.split("\n")
        self.index = 0

    def next_op(self):
        line = self._next_line()
        if not line:
            return None
        opcode, condcode = line.split("if")
        cond = self._cond(condcode)
        return self._op(opcode, cond)

    def _next_line(self):
        line = None
        while not line and self.index < len(self.code):
            line = self.code[self.index].strip()
            self.index += 1
        return line

    def _op(self, opcode, cond):
        match = self.pattern.match(opcode.strip())
        hows = {
            "inc": lambda regval, val: regval + val,
            "dec": lambda regval, val: regval - val
        }
        return Op(
            register=match.group("reg"),
            value=int(match.group("value")),
            how=hows[match.group("op")],
            condition=cond
        )

    def _cond(self, condcode):
        match = self.pattern.match(condcode.strip())
        hows = {
            ">": lambda a, b: a > b,
            "<": lambda a, b: a < b,
            "==": lambda a, b: a == b,
            "!=": lambda a, b: a != b,
            "<=": lambda a, b: a <= b,
            ">=": lambda a, b: a >= b
        }
        return Cond(
            register=match.group("reg"),
            value=int(match.group("value")),
            how=hows[match.group("op")]
        )


def test_test_code():
    cpu = Cpu()
    interpreter = Interpreter(TEST_CODE)
    while True:
        op = interpreter.next_op()
        if not op:
            break
        op.run(cpu)
    eq_({
        'a': 1,
        'c': -10
    }, cpu.registers)
    eq_(1, cpu.maxval)


def main():
    with open("08.txt", "r") as f:
        interpreter = Interpreter(f.read())
    cpu = Cpu()
    while True:
        op = interpreter.next_op()
        if not op:
            break
        op.run(cpu)
    print(cpu.maxval)
    print(cpu.all_time_max)


if __name__ == "__main__":
    main()


TEST_CODE = """
b inc 5 if a > 1
a inc 1 if b < 5
c dec -10 if a >= 1
c inc -20 if c == 10
"""
