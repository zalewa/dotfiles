#!/usr/bin/env python3
from copy import copy

from nose.tools import eq_


class Node:
    def __init__(self, nodes, score=1):
        self.nodes = copy(nodes) or []
        self.score = score

    def total_score(self):
        total_score = self.score * len(self.nodes)
        for subnodes in self.nodes:
            subnode = Node(subnodes, self.score + 1)
            total_score += subnode.total_score()
        return total_score


class Parser:
    def __init__(self, text):
        self.text = text

    @property
    def total_garbage(self):
        return self._state.total_garbage

    def parse(self):
        state = self._state = self._State()
        for c in self.text:
            if state.escape:
                state.escape = False
                continue
            if c == "!":
                state.escape = True
            elif state.comment:
                if c == ">":
                    state.comment = False
                else:
                    self._state.total_garbage += 1
            elif not state.comment:
                if c == "{":
                    self._create_new_node(state.level)
                    state.level += 1
                elif c == "}" and state.level > 0:
                    state.level -= 1
                elif c == "<":
                    state.comment = True
        return Node(state.nodes)

    def _create_new_node(self, level):
        nodes = self._state.nodes
        for _ in range(level):
            nodes = nodes[-1]
        nodes.append([])

    class _State:
        def __init__(self):
            self.comment = False
            self.escape = False
            self.level = 0
            self.nodes = []
            self.total_garbage = 0


def test_total_score():
    def _score(text):
        return Parser(text).parse().total_score()

    eq_(1, _score("{}"))
    eq_(6, _score("{{{}}}"))
    eq_(5, _score("{{},{}}"))
    eq_(16, _score("{{{},{},{{}}}}"))
    eq_(1, _score("{<a>,<a>,<a>,<a>}"))
    eq_(9, _score("{{<ab>},{<ab>},{<ab>},{<ab>}}"))
    eq_(9, _score("{{<!!>},{<!!>},{<!!>},{<!!>}}"))
    eq_(3, _score("{{<a!>},{<a!>},{<a!>},{<ab>}}"))


def test_count_garbage():
    def _garbage(text):
        parser = Parser(text)
        parser.parse()
        return parser.total_garbage

    eq_(0, _garbage("<>"))
    eq_(17, _garbage("<random characters>"))
    eq_(3, _garbage("<<<<>"))
    eq_(2, _garbage("<{!>}>"))
    eq_(0, _garbage("<!!>"))
    eq_(0, _garbage("<!!!>>"))
    eq_(10, _garbage('<{o"i!a,<{i<a>'))

def main():
    with open("09.txt", "r") as f:
        text = f.read()
    parser = Parser(text)
    node = parser.parse()
    print(node.total_score())
    print(parser.total_garbage)


if __name__ == "__main__":
    main()
