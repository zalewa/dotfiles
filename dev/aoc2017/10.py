#!/usr/bin/env python3
from copy import copy
from nose.tools import eq_
import os
import sys

sys.path.append(os.path.basename(__file__))

from knot import Knot, ascii_knot_to_hex


def main():
    knot = Knot(INPUT)
    while knot.rounds == 0:
        knot.step()
    print(knot.seq[0] * knot.seq[1])
    print(ascii_knot_to_hex(INPUT_ASCII))


INPUT = [212, 254, 178, 237, 2, 0, 1, 54, 167,
         92, 117, 125, 255, 61, 159, 164]
INPUT_ASCII = b"212,254,178,237,2,0,1,54,167,92,117,125,255,61,159,164"


if __name__ == "__main__":
    main()
