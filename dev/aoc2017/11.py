#!/usr/bin/env python3
from enum import Enum

from nose.tools import eq_


class Direction(Enum):
    N = (0, -1)
    NE = (1, -1)
    SE = (1, 0)
    S = (0, 1)
    SW = (-1, 1)
    NW = (-1, 0)

    def __getitem__(self, index):
        return self.value[index]

    


def axial_to_cube(coords):
    x = coords[0]
    z = coords[1]
    y = -x - z
    return (x, y, z)


def cube_to_axial(coords):
    return (coords[0], coords[2])


def cube_manhattan_distance(a, b):
    return max([abs(z0 - z1) for z0, z1 in zip(a, b)])


def distance(a, b):
    return cube_manhattan_distance(
        axial_to_cube(a),
        axial_to_cube(b)
    )


def move(coords, direction):
    if not isinstance(direction, list):
        direction = [direction]
    for step in direction:
        coords = (coords[0] + step[0], coords[1] + step[1])
    return coords


def test_walk():
    def walk(expected_distance, origin, direction):
        eq_(expected_distance, distance(origin, move(origin, direction)))

    Dir = Direction
    walk(3, (0, 0), [Dir.NE, Dir.NE, Dir.NE])
    walk(3, (4, 1), [Dir.NE, Dir.NE, Dir.NE])
    walk(0, (-200, -150), [Dir.NE, Dir.NE, Dir.SW, Dir.SW])
    walk(2, (0, 1), [Dir.NE, Dir.NE, Dir.S, Dir.S])
    walk(3, (0, 0), [Dir.SE, Dir.SW, Dir.SE, Dir.SW, Dir.SW])


def main():
    with open("11.txt", "r") as f:
        rawinput = f.read()
    print(dir(Direction))
    origin = (0, 0)
    point = origin
    all_points = [origin]
    for step in rawinput.split(","):
        step = Direction[step.strip().upper()]
        point = move(point, step)
        all_points.append(point)
    print(point, distance(origin, point))
    distant = max(all_points, key=lambda p: distance(origin, p))
    print(distant, distance(origin, distant))


if __name__ == "__main__":
    main()
