#!/usr/bin/env python
from copy import copy

from nose.tools import eq_, assert_true, assert_false


class Graph:
    def __init__(self):
        self.points = set()
        self.edges = set()

    def add_point(self, point):
        self.points.add(point)

    def add_edge(self, edge):
        self.points.add(edge[0])
        self.points.add(edge[1])
        if not self.has_edge(edge):
            self.edges.add(edge)

    def has_edge(self, edge):
        reverse = (edge[1], edge[0])
        return edge in self.edges or reverse in self.edges

    def has_connection(self, pointa, pointb, omit_edges=None):
        if pointa == pointb:
            return True
        omit_edges = omit_edges if omit_edges is not None else set()
        edges = self.all_edges_with_point(pointa)
        for edge in edges:
            if edge in omit_edges:
                continue
            if pointb in edge:
                return True
            omit_edges.add(edge)
            next_point = edge[0] if edge[0] != pointa else edge[1]
            if self.has_connection(next_point, pointb, omit_edges):
                return True
        return False

    def all_edges_with_point(self, point):
        return [edge for edge in self.edges if point in edge]

    def count_connected_points(self, point):
        count = 0
        for candidate in self.points:
            if self.has_connection(point, candidate):
                count += 1
        return count

    def extract_group(self):
        group = Graph()
        if self.is_empty():
            return group
        next_points = [next(iter(self.points))]
        while next_points:
            point = next_points.pop()
            if point not in self.points:
                continue
            edges = self.all_edges_with_point(point)
            group.points.add(point)
            self.points.remove(point)
            for edge in edges:
                group.edges.add(edge)
                self.edges.remove(edge)
                next_points.append(edge[0] if edge[0] != point else edge[1])
        return group

    def is_empty(self):
        return not self.points

    def group_count(self):
        this = copy(self)
        count = 0
        while not this.is_empty():
            count += 1
            this.extract_group()
        return count


def graph_parse(text):
    graph = Graph()
    for line in text.split("\n"):
        line = line.strip()
        if not line:
            continue
        point, connections = line.split("<->")
        point = int(point.strip())
        graph.add_point(point)
        for connection in connections.split(","):
            connection = int(connection)
            graph.add_edge((point, connection))
    return graph


def test_parse():
    graph = graph_parse(TEST_TEXT)
    eq_(set(range(7)), graph.points)
    eq_(set([
        (0, 2),
        (1, 1),
        (2, 3), (2, 4),
        (3, 4),
        (4, 6),
        (5, 6)
    ]), graph.edges)


def test_has_connection():
    graph = graph_parse(TEST_TEXT)
    assert_true(graph.has_connection(0, 0))
    assert_true(graph.has_connection(2, 0))
    assert_true(graph.has_connection(3, 2))
    assert_true(graph.has_connection(4, 2))
    assert_true(graph.has_connection(5, 6))
    assert_true(graph.has_connection(5, 0))
    assert_true(graph.has_connection(6, 2))
    assert_true(graph.has_connection(1, 1))
    assert_false(graph.has_connection(1, 5))
    assert_false(graph.has_connection(1, 0))
    assert_false(graph.has_connection(0, 1))
    assert_false(graph.has_connection(5, 1))


def test_count_connected():
    graph = graph_parse(TEST_TEXT)
    eq_(1, graph.count_connected_points(1))
    for point in [0, 2, 3, 4, 5, 6]:
        eq_(6, graph.count_connected_points(point))


def test_extract_group():
    graph = graph_parse(TEST_TEXT)
    group1 = graph.extract_group()
    eq_(set([0, 2, 3, 4, 5, 6]), group1.points)
    eq_(set([
        (0, 2),
        (2, 3), (2, 4),
        (3, 4),
        (4, 6),
        (5, 6)
    ]), group1.edges)
    assert_false(graph.is_empty())

    group2 = graph.extract_group()
    eq_(set([1]), group2.points)
    eq_(set([(1, 1)]), group2.edges)
    assert_true(graph.is_empty())

    group3 = graph.extract_group()
    assert_true(group3.is_empty())
    assert_true(graph.is_empty())


def test_count_group():
    graph = graph_parse(TEST_TEXT)
    eq_(2, graph.group_count())


def main():
    with open("12.txt", "r") as f:
        rawinput = f.read()
    graph = graph_parse(rawinput)
    print(graph.count_connected_points(0))
    print(graph.group_count())


if __name__ == "__main__":
    main()


TEST_TEXT = """
0 <-> 2
1 <-> 1
2 <-> 0, 3, 4
3 <-> 2, 4
4 <-> 2, 3, 6
5 <-> 6
6 <-> 4, 5
"""
