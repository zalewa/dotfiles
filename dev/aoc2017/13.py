#!/usr/bin/env python3
from copy import deepcopy
from multiprocessing import Pool, cpu_count
import signal

from nose.tools import eq_, assert_true, assert_false


class Firewall:
    @classmethod
    def from_dict(cls, layers):
        obj = cls()
        for idx, length in layers.items():
            obj.set_layer(idx, Scanner(length))
        return obj

    def __init__(self):
        self.layers = {}

    def set_layer(self, idx, layer):
        self.layers[idx] = layer

    @property
    def first_layer(self):
        return min(self.layers.keys())

    @property
    def last_layer(self):
        return max(self.layers.keys())

    def move_scan(self):
        for layer in self.layers.values():
            layer.move()

    def is_scanned(self, layer_idx, column):
        return self.scanner_position(layer_idx) == column

    def scanner_position(self, layer_idx):
        layer = self.layers.get(layer_idx)
        return layer.index if layer else None

    def layer_severity(self, layer_idx):
        layer = self.layers.get(layer_idx)
        layer_length = layer.length if layer else 0
        return layer_length * layer_idx


class Scanner:
    def __init__(self, length):
        self.length = length
        self.index = 0
        self._move = 1

    def move(self):
        if self._move < 0 and self.index <= 0:
            self.index = 0
            self._move = 1
        elif self._move > 0 and self.index >= self.length - 1:
            self.index = self.length - 1
            self._move = -1
        self.index += self._move


def send_bug(firewall, delay=0):
    bug_layer = firewall.first_layer
    end_layer = firewall.last_layer
    total_severity = 0
    for _ in range(delay):
        firewall.move_scan()
    for bug_idx in range(bug_layer, end_layer + 1):
        if firewall.is_scanned(bug_idx, 0):
            total_severity += firewall.layer_severity(bug_idx)
        firewall.move_scan()
    return total_severity


def send_bug_until_caught(firewall, delay=0):
    bug_layer = firewall.first_layer
    end_layer = firewall.last_layer
    for _ in range(delay):
        firewall.move_scan()
    for bug_idx in range(bug_layer, end_layer + 1):
        if firewall.is_scanned(bug_idx, 0):
            return True
        firewall.move_scan()
    return False


def test_walk_severity():
    def _send_bug(delay):
        firewall = Firewall.from_dict(TEST_FIREWALL)
        return send_bug(firewall, delay=delay)

    eq_(24, _send_bug(delay=0))
    eq_(0, _send_bug(delay=10))


def test_send_bug_until_caught():
    def _send_bug(delay):
        firewall = Firewall.from_dict(TEST_FIREWALL)
        return send_bug_until_caught(firewall, delay=delay)

    for delay in range(10):
        assert_true(_send_bug(0), msg=str(delay))
    assert_false(_send_bug(10))
    assert_true(_send_bug(11))


def find_smuggled(attempt):
    delay, length, fw = attempt
    for offset in range(length):
        if not send_bug_until_caught(deepcopy(fw)):
            return delay + offset
        fw.move_scan()
    return None


def show_severity():
    firewall = Firewall.from_dict(REAL_FIREWALL)
    print(send_bug(firewall, 0))


def show_smuggle_delay():
    '''
    This solution is suboptimal.

    The hit can be found just by doing:

    for x in range(length):
        if (x + delay) % (depths[x]*2 - 2) == 0:
            return True  # we have hit
    return False
    '''
    firewall = Firewall.from_dict(REAL_FIREWALL)
    delay = 2960000
    #delay = 2
    for skippy in range(delay):
        # we know it cannot be smuggled
        if skippy % 100000 == 0:
            print("S: ", skippy)
        firewall.move_scan()


    pool = Pool()
    run = [True]
    def die(*args):
        run[0] = False
        pool.terminate()

    signal.signal(signal.SIGINT, die)


    while run[0]:
        batch = []
        print(delay)
        for cpu in range(cpu_count()):
            batch_size = 1000
            offset = cpu * batch_size
            batch.append((delay, batch_size, deepcopy(firewall)))
            for _ in range(batch_size):
                firewall.move_scan()
            delay += batch_size
        results = pool.map(find_smuggled, batch)
        delays = [d for d in results if d is not None]
        if delays:
            print("F:", delays[0])
            break


def main():
    show_severity()
    show_smuggle_delay()


REAL_FIREWALL = {
    0: 3,
    1: 2,
    2: 4,
    4: 6,
    6: 4,
    8: 6,
    10: 5,
    12: 6,
    14: 9,
    16: 6,
    18: 8,
    20: 8,
    22: 8,
    24: 8,
    26: 8,
    28: 8,
    30: 12,
    32: 14,
    34: 10,
    36: 12,
    38: 12,
    40: 10,
    42: 12,
    44: 12,
    46: 12,
    48: 12,
    50: 12,
    52: 14,
    54: 14,
    56: 12,
    62: 12,
    64: 14,
    66: 14,
    68: 14,
    70: 17,
    72: 14,
    74: 14,
    76: 14,
    82: 14,
    86: 18,
    88: 14,
    96: 14,
    98: 44
}

TEST_FIREWALL = {
    0: 3,
    1: 2,
    4: 4,
    6: 4
}


if __name__ == "__main__":
    main()
