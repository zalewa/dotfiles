#!/usr/bin/env python3
from copy import copy, deepcopy
from nose.tools import eq_
import os
import sys

sys.path.append(os.path.basename(__file__))

from knot import ascii_knot


def test_slice():
    magic = "flqrgnkx"
    expected = [
        0b11010100,
        0b01010101,
        0b00001010,
        0b10101101,
        0b01101000,
        0b11001001,
        0b01000100,
        0b11010110
    ]
    for i in range(8):
        rowmagic = "{}-{}".format(magic, i).encode("ascii")
        line = ascii_knot(rowmagic)
        eq_(expected[i], line[0],
            msg="{}: {:b} != {:b}".format(i, expected[i], line[0]))


def total_used(magic):
    count = 0
    for i in range(128):
        rowmagic = "{}-{}".format(magic, i).encode("ascii")
        line = ascii_knot(rowmagic)
        for cell in line:
            count += "{:b}".format(cell).count("1")
    return count


def test_total_used():
    eq_(8108, total_used("flqrgnkx"))


def to_matrix(magic):
    matrix = []
    for i in range(128):
        rowmagic = "{}-{}".format(magic, i).encode("ascii")
        line = ascii_knot(rowmagic)
        row = []
        for cell in line:
            row += [bool(int(bit)) for bit in "{:08b}".format(cell)]
        matrix.append(row)
    return matrix


def test_to_matrix():
    matrix = to_matrix("flqrgnkx")
    expected = [
        [True, True, False, True, False, True, False, False],
        [False, True, False, True, False, True, False, True],
        [False, False, False, False, True, False, True, False],
    ]
    for i, expected_row in enumerate(expected):
        eq_(expected_row, matrix[i][:8])


def count_regions(matrix):
    count = 0
    matrix = deepcopy(matrix)
    while True:
        coords = _find_first_true(matrix)
        if not coords:
            break
        _clear_region(matrix, coords)
        count += 1
    return count


def _find_first_true(matrix):
    for x in range(len(matrix)):
        for y in range(len(matrix[x])):
            if matrix[x][y]:
                return (x, y)
    return None


def _clear_region(matrix, coords):
    x, y = coords
    if x < 0 or x >= len(matrix):
        return
    if y < 0 or y >= len(matrix[x]):
        return
    if not matrix[x][y]:
        return
    matrix[x][y] = False
    _clear_region(matrix, (x - 1, y))
    _clear_region(matrix, (x + 1, y))
    _clear_region(matrix, (x, y - 1))
    _clear_region(matrix, (x, y + 1))


def test_count_regions():
    matrix = to_matrix("flqrgnkx")
    eq_(1242, count_regions(matrix))


def main():
    print(total_used("ljoxqyyw"))
    print(count_regions(to_matrix("ljoxqyyw")))


INPUT = [212, 254, 178, 237, 2, 0, 1, 54, 167,
         92, 117, 125, 255, 61, 159, 164]
INPUT_ASCII = b"212,254,178,237,2,0,1,54,167,92,117,125,255,61,159,164"


if __name__ == "__main__":
    main()
