#!/usr/bin/env python3
import sys

from nose.tools import eq_


FACTOR_A = 16807
FACTOR_B = 48271


class Generator:
    DIVISOR = 2147483647

    def __init__(self, start, factor, matcher=None):
        self.value = start
        self.factor = factor
        self.matcher = matcher
        if matcher is None:
            self.matcher = lambda value: True

    def next_value(self):
        while True:
            self.value = (self.value * self.factor) % self.DIVISOR
            if self.matcher(self.value):
                break
        return self.value


def test_generator():
    generator_a = Generator(start=65, factor=FACTOR_A)
    eq_(1092455, generator_a.next_value())
    eq_(1181022009, generator_a.next_value())
    eq_(245556042, generator_a.next_value())
    eq_(1744312007, generator_a.next_value())
    eq_(1352636452, generator_a.next_value())

    generator_b = Generator(start=8921, factor=FACTOR_B)
    eq_(430625591, generator_b.next_value())
    eq_(1233683848, generator_b.next_value())
    eq_(1431495498, generator_b.next_value())
    eq_(137874439, generator_b.next_value())
    eq_(285222916, generator_b.next_value())


def test_filtered_generator():
    generator_a = Generator(start=65, factor=FACTOR_A,
                            matcher=lambda value: value % 4 == 0)
    eq_(1352636452, generator_a.next_value())
    eq_(1992081072, generator_a.next_value())
    eq_(530830436, generator_a.next_value())
    eq_(1980017072, generator_a.next_value())
    eq_(740335192, generator_a.next_value())

    generator_b = Generator(start=8921, factor=FACTOR_B,
                            matcher=lambda value: value % 8 == 0)
    eq_(1233683848, generator_b.next_value())
    eq_(862516352, generator_b.next_value())
    eq_(1159784568, generator_b.next_value())
    eq_(1616057672, generator_b.next_value())
    eq_(412269392, generator_b.next_value())


def judge(generators, comparisons=40 * 1000 * 1000):
    gen_a, gen_b = generators
    count_match = 0
    print_step = comparisons / 10
    while comparisons >= 0:
        if comparisons % print_step == 0:
            print("remaining: {}".format(comparisons), file=sys.stderr)
        value_a = gen_a.next_value()
        value_b = gen_b.next_value()
        if (value_a & 0xffff) == (value_b & 0xffff):
            count_match += 1
        comparisons -= 1
    return count_match


def test_judge():
    generator_a = Generator(start=65, factor=FACTOR_A)
    generator_b = Generator(start=8921, factor=FACTOR_B)
    eq_(588, judge((generator_a, generator_b)))


def test_filtered_judge():
    generator_a = Generator(start=65, factor=FACTOR_A,
                            matcher=lambda value: value % 4 == 0)
    generator_b = Generator(start=8921, factor=FACTOR_B,
                            matcher=lambda value: value % 8 == 0)
    eq_(309, judge((generator_a, generator_b),
                   comparisons=5 * 1000 * 1000))


def _unfiltered():
    generator_a = Generator(start=277, factor=FACTOR_A)
    generator_b = Generator(start=349, factor=FACTOR_B)
    print("unfiltered judgement: {}".format(
        judge((generator_a, generator_b))))


def _filtered():
    generator_a = Generator(start=277, factor=FACTOR_A,
                            matcher=lambda value: value % 4 == 0)
    generator_b = Generator(start=349, factor=FACTOR_B,
                            matcher=lambda value: value % 8 == 0)
    print("filtered judgement: {}".format(
        judge((generator_a, generator_b),
              comparisons=5 * 1000 * 1000)))


def main():
    _unfiltered()
    _filtered()


if __name__ == "__main__":
    main()
