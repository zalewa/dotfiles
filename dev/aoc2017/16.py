#!/usr/bin/env python3
import sys
from copy import copy

from nose.tools import eq_


def spin(seq, amount):
    left = seq[:-amount]
    right = seq[-amount:]
    return right + left


def test_spin():
    def _spin(seq, amount):
        return ''.join(spin(list(seq), amount))

    eq_("cdeab", _spin("abcde", 3))
    eq_("abcde", _spin("abcde", 5))
    eq_("abcde", _spin("abcde", 0))
    eq_("abcde", _spin("abcde", 10))
    eq_("eabcd", _spin("abcde", 1))


def exchange(seq, src, dst):
    src_val, dst_val = seq[src], seq[dst]
    seq[src] = dst_val
    seq[dst] = src_val
    return seq


def test_exchange():
    def _exchange(seq, src, dst):
        return ''.join(exchange(list(seq), src, dst))

    eq_("ebcda", _exchange("abcde", 0, 4))
    eq_("abdce", _exchange("abcde", 2, 3))
    eq_("abdce", _exchange("abcde", 3, 2))


def partner(seq, p1, p2):
    return exchange(
        seq,
        seq.index(p1),
        seq.index(p2))


def test_partner():
    def _partner(seq, p1, p2):
        return ''.join(partner(list(seq), p1, p2))

    eq_("adcbe", _partner("abcde", "b", "d"))


def parse_program(program):
    return program.strip().split(",")


def parse_opcode(opcode):
    op = opcode[0:2]
    if op[0] == "s":
        return spin, (int(opcode[1:]),)
    elif op[0] == "x":
        args = opcode[1:].split("/")
        return exchange, (int(args[0]), int(args[1]))
    elif op[0] == "p":
        args = opcode[1:].split("/")
        return partner, (args[0], args[1])
    else:
        raise Exception("wtf op {}".format(opcode))


def main():
    with open("./16.txt", "r") as f:
        program_text = f.read()
    program = parse_program(program_text)
    program = [parse_opcode(opcode) for opcode in program]
    original_sequence = list("abcdefghijklmnop")
    sequence = copy(original_sequence)

    def run_program(sequence):
        for (instruction, args) in program:
            sequence = instruction(sequence, *args)
        return sequence

    # part 1
    sequence = run_program(sequence)
    print("P1:", ''.join(sequence))
    # part 2
    sequence = copy(original_sequence)
    length = 1 * 1000 * 1000 * 1000
    repeat_period = 60
    for dance_idx in range(length % repeat_period):
        sequence = run_program(sequence)
    print("P2:", ''.join(sequence))


if __name__ == "__main__":
    main()
