#!/usr/bin/env python3
from copy import copy
from nose.tools import eq_


class Knot:
    def __init__(self, lengths, size=256):
        self.lengths = copy(lengths)
        self.original_lengths = copy(lengths)
        self.seq = list(range(size))
        self.idx = 0
        self.skip = 0
        self.rounds = 0

    def step(self):
        length = self.lengths.pop(0)
        nums = []
        for offset in range(length):
            idx = self._offset(offset)
            nums.append(self.seq[idx])
        for offset, num in enumerate(reversed(nums)):
            idx = self._offset(offset)
            self.seq[idx] = num
        self.idx = (self.idx + length + self.skip) % len(self.seq)
        self.skip += 1
        if not self.lengths:
            self.lengths = copy(self.original_lengths)
            self.rounds += 1

    def _offset(self, offset):
        return (self.idx + offset) % len(self.seq)


def test_knot():
    knot = Knot([3, 4, 1, 5], 5)
    def step(exp_idx, exp_seq):
        knot.step()
        eq_(exp_seq, knot.seq)
        eq_(exp_idx, knot.idx)

    step(3, [2, 1, 0, 3, 4])
    step(3, [4, 3, 0, 1, 2])
    step(1, [4, 3, 0, 1, 2])
    step(4, [3, 4, 2, 1, 0])


def ascii_knot(ascii_text):
    lengths = list(ascii_text)
    lengths.extend([17, 31, 73, 47, 23])
    knot = Knot(lengths)
    while knot.rounds < 64:
        knot.step()
    return dense(knot.seq)


def ascii_knot_to_hex(ascii_text):
    return hexlify(ascii_knot(ascii_text))


def test_ascii_knot_to_hex():
    _knot = ascii_knot_to_hex
    eq_("a2582a3a0e66e6e86e3812dcb672a272", _knot(""))
    eq_("33efeb34ea91902bb2f59c9920caa6cd", _knot(b"AoC 2017"))
    eq_("3efbe78a8d82f29979031a4aa0b16a9d", _knot(b"1,2,3"))
    eq_("63960835bcdc130f0b66d7ff4f6a5a8e", _knot(b"1,2,4"))


def dense(sparse_hash):
    dense_hash = []
    while sparse_hash:
        num = sparse_hash[0]
        for n in range(1, 16):
            num ^= sparse_hash[n]
        dense_hash.append(num)
        sparse_hash = sparse_hash[16:]
    return dense_hash


def test_dense():
    eq_([64], dense([65, 27, 9, 1, 4, 3, 40, 50, 91, 7, 6, 0, 2, 5, 68, 22]))


def hexlify(nums):
    return "".join(["{:02x}".format(num) for num in nums])
