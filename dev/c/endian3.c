/*
  Detects the platform's endianness and shows the detail
  of how a 32-bit int is laid out in memory.

  This is endian3 because I had two similar programs before
  but they weren't as elegant.
*/
#include <stdio.h>

int main(int argc, char *argv[])
{
    unsigned long number = 0xAABBCCDD;
    unsigned char *ptr = (unsigned char *) &number;

    printf("0x%p:\n", ptr);
    printf(" Off | 0  | 1  | 2  | 3  |\n");
    printf(" Val | %X | %X | %X | %X |\n",
           ptr[0], ptr[1], ptr[2], ptr[3]);

    printf("\nYour platform is ");
    if (ptr[0] == 0xDD)
        printf("little-endian\n");
    else
        printf("big-endian\n");
    return 0;
}
