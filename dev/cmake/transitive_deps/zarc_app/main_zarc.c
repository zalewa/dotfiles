#include <libzarc.h>

#include <stddef.h>

#if defined(MSDOS) || defined(OS2) || defined(WIN32) || defined(__CYGWIN__)
#  include <fcntl.h>
#  include <io.h>
#  define SET_BINARY_MODE(file) setmode(fileno(file), O_BINARY)
#else
#  define SET_BINARY_MODE(file)
#endif

int main(int argc, char **argv)
{
	SET_BINARY_MODE(stdin);
	SET_BINARY_MODE(stdout);

	int ret = zarc(stdin, stdout, 4);
	return ret;
}
