#include <atomic>
#include <cstdint>
#include <iostream>

std::atomic<int64_t> atomic_number(0);

int main()
{
	for (int i = 0; i < 10; ++i) {
		std::cout << "[" << i << "] " << atomic_number.fetch_add(5) << std::endl;
	}
	return 0;
}
