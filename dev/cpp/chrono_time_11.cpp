#include <chrono>
#include <iostream>
#include <thread>
#include <cstdio>

int main(int argc, char **argv)
{
    std::chrono::time_point<std::chrono::system_clock> clock1 = std::chrono::system_clock::now();
    std::this_thread::sleep_for(std::chrono::milliseconds(500));
    auto clock2 = std::chrono::system_clock::now();

    auto raw1 = std::chrono::duration_cast<std::chrono::milliseconds>(clock1.time_since_epoch()).count();
    std::cout << "clock1, sizeof=" << sizeof(raw1) << ", " << raw1 << std::endl;

    auto raw2 = std::chrono::duration_cast<std::chrono::milliseconds>(clock2.time_since_epoch()).count();
    std::cout << "clock2, sizeof=" << sizeof(raw2) << ", " << raw2 << std::endl;

    std::cout << "diff " << raw2 - raw1 << std::endl;

    return 0;
}
