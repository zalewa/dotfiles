#include "object.h"
#include <cstdio>

int main(int argc, char *argv[])
{
	printf("++Context\n");
	{
		Object *obj = new Object;
		printf("\n"); // the printf in between is mandatory (???)
		obj->del_self();
	}
	printf("--Context\n");
	return 0;
}
