#ifndef OBJECT_H
#define OBJECT_H

#include <cstdio>

class SelfDeleter
{
public:
	SelfDeleter()          { printf("SelfDeleter(), this=%p\n", this); }
	virtual ~SelfDeleter() { printf("~SelfDeleter(), this=%p\n", this); }

	void del_self()
	{
		printf("SelfDeleter::del_self() - delete this=%p\n", this);
		delete this;
	}
};

class Parent
{
public:
	Parent()          { printf("Parent(), this=%p\n", this); }
	virtual ~Parent() { printf("~Parent(), this=%p\n", this); }
};

class Object : public Parent, public SelfDeleter
{
public:
	Object();
	virtual ~Object();
};

#endif
