A "free-nonheap-object" compilation warning appears on the `delete this`
statement when this example is compiled with `-O2` in GCC 11.

Usage
=====

Ensure that GCC 11 is the current compiler, then:

  cd minimal
  make

Or:

  cd minimal
  make deletethis_o2


What's here?
============

There are two examples here: "minimal" and "templated". Despite the code
differences, I believe they are fundamentally the same. The "templated"
is a bit more convoluted as it tries to provide a case more true to
something that you would use in reality.

Both examples build two binaries: `deletethis_o0` and `deletethis_o2`.

These binaries are, again, the same, and only differ by being built with
either `-O0` or `-O2`.

Both binaries build and appear to run properly as indicated by the
prints. Valgrind also doesn't detect any memory management issues.

The difference is that the `-O2` compilation emits the
`-Wfree-nonheap-object` warning in GCC 11, while clang,
GCC 9 or 10 don't.


Minimal
-------

The minimal requirements for reproduction are:

1. Two compilation units (.cpp files) must be present.

2. There must be a class that inherits from 2 parent classes.
   Here: Object inherits from Parent (1st) and SelfDeleter (2nd).

3. The 2nd parent class must provide a method where it has
   `delete this`. It is important that this is the 2nd class, not the
   1st one, in the inheritance chain.

4. The implementation of Object's constructor and destructor must
   be hidden in its own compilation unit, separate from where the
   Object instance is created.

5. In the other compilation unit, the Object instance must be created by
   `new` and then deleted by calling `obj->del_self()`. A `printf()`
   must be present in-between.


Templated
---------

The "templated" example differs from the "minimal" in two aspects:

1. The `printf()` in `main()` isn't mandatory to reproduce the issue,
   as the templated `Container<>` object is sufficient.

2. The error message produced by GCC is more specific as to where the
   error is occurring.


Root cause
==========

GCC 11 unveils an undefined behavior. The `Object` class uses
multi-inheritance. The `this` pointer in the 2nd base class,
`SelfDeleter`, is offset by the sizeof the 1st base class. When
`delete this` is called in `SelfDeleter`, it passes a pointer to this
`delete` that wasn't returned by `new`. This is because this passed
pointer is the offset pointer.

For some reasons this works, doesn't crash and isn't noticed by
valgrind, but, in accordance to cppreference.com, it is also an
undefined behavior. The only valid pointers for `delete` are nullptrs
and those returned previously by `new`, so not `new + 8`.

In this manner, GCC 11 is right to point out an error, and it's up to
us to replace `delete this` with a proper solution.
