#include "object.h"
#include <cstdio>

int main(int argc, char *argv[])
{
	printf("++Context\n");
	{
		Container<Object> obj(new Object);
		printf("\n"); // in the templated example this printf is optional
	}
	printf("--Context\n");
	return 0;
}
