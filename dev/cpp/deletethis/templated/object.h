#ifndef OBJECT_H
#define OBJECT_H

#include <cstdio>

template<typename T> class Container
{
public:
	Container(T *obj) : obj(obj) { printf("Container(obj=%p)\n", obj); }

	virtual ~Container()
	{
		printf("~Container(obj=%p)\n", obj);
		obj->del_self();
	}

private:
	T* obj;
};

template<typename T> class SelfDeleter
{
public:
	SelfDeleter()          { printf("SelfDeleter(), this=%p\n", this); }
	virtual ~SelfDeleter() { printf("~SelfDeleter(), this=%p\n", this); }

	void del_self()
	{
		printf("SelfDeleter::del_self() - delete this=%p\n", this);
		delete this;
	}
};

class Parent
{
public:
	Parent()          { printf("Parent(), this=%p\n", this); }
	virtual ~Parent() { printf("~Parent(), this=%p\n", this); }
};

class Object : public Parent, public SelfDeleter<Object>
{
public:
	Object();
	virtual ~Object();
};

#endif
