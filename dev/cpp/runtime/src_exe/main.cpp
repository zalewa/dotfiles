#include <windows.h>

#include <libapi.h>

LRESULT CALLBACK WndProc(HWND hwnd, UINT message, WPARAM wparam, LPARAM lparam)
{
	switch (message)
	{
	case WM_SYSCOMMAND:
		switch (wparam)
		{
		case SC_CLOSE:
			PostQuitMessage(0);
			return 0;
		case SC_MINIMIZE:
			show_message(hwnd, TEXT("You didn't think you could really minimize now, did you?"), TEXT("No no no"));
			return 0;
		}
	}
	return DefWindowProc(hwnd, message, wparam, lparam);
}

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int nMode)
{
	HWND hwndMain;
	MSG message;
	WNDCLASS mainclass;

	memset(&mainclass, 0, sizeof(mainclass));
	mainclass.hInstance = hInstance;
	mainclass.lpszClassName = TEXT("My Window Class");
	mainclass.lpfnWndProc = WndProc;
	mainclass.hIcon = LoadIcon(NULL, IDI_WINLOGO);
	mainclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	mainclass.hbrBackground = (HBRUSH) GetStockObject(BLACK_BRUSH);

	RegisterClass(&mainclass);

	hwndMain = CreateWindow(TEXT("My Window Class"), TEXT("My Window"), WS_OVERLAPPEDWINDOW, 300, 400, 200, 100, NULL, NULL, hInstance, 0);
	ShowWindow(hwndMain, SW_SHOW);

	while (GetMessage(&message, NULL, 0, 0))
	{
		DispatchMessage(&message);
	}

	return 0;
}