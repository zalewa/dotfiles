#include "libapi.h"

void show_message(HWND parent, LPCTSTR message, LPCTSTR title)
{
	MessageBox(parent, message, title, MB_OK | MB_ICONINFORMATION);
}