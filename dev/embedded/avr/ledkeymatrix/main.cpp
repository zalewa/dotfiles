#include <avr/io.h>
#include <util/delay.h>
#include <keyboard/dumbkeyboard.h>
#include <keyboard/keystates.h>
#include <display/digits.h>
#include <display/dumbledmatrix.h>
#include <global/global.h>


DumbKeyboard dumbKeyboard(PortAccess::PortC);
KeyStates keyStates;
unsigned char matrix[8];
int number = 0;

unsigned char extractKeycode(unsigned short& keyFlags);
bool readKeyboard();
void routineCounter();
void updatePortA(unsigned char value);

int main(void)
{
	routineCounter();
	return 0;
}

// This will extract a single keycode (1 - 16) from keyFlags flags.
// If there are no more keys to extract this will return 0.
// keyFlags will be modified to remove read key.
unsigned char extractKeycode(unsigned short& keyFlags)
{
	unsigned short keyFlagsCopy = keyFlags;

	for (unsigned bitIndex = 0; bitIndex < sizeof(unsigned short) * 8; ++bitIndex)
	{
		if ((1 & keyFlagsCopy) == 1)
		{
			keyFlags &= ~(1 << bitIndex);

			return bitIndex + 1;
		}

		keyFlagsCopy >>= 1;
	}

	return 0;
}

// Returns true if any key was pressed.
bool readKeyboard()
{
	unsigned short keys = dumbKeyboard.getKeyboardStatus(1);

	unsigned short keysPressed = 0;
	unsigned short keysUnpressed = 0;
	keyStates.update(keys, keysPressed, keysUnpressed);

	bool bAnyKeyPressed = false;

	while (1)
	{
		unsigned char keyCode = extractKeycode(keysPressed);

		if (keyCode != 0)
		{
			updatePortA(0x55);

			number += keyCode;

			if (number > 120)
			{
				number = 0;
			}

			Digits::fillMatrix(number, matrix);

			bAnyKeyPressed = true;
		}
		else
		{
			break;
		}

	}

	return bAnyKeyPressed;
}

void routineCounter()
{
	// Const definitions.
	const int LOOP_DELAY_US = 250;

	const int KEYBOARD_READ_DELAY = 64;
	const int NUMBER_AUTO_INCREMENT_DELAY = (1000 / LOOP_DELAY_US) * 1000;

	// Init display
	DDRA = 0xff;
	PORTA = 0xaa;

	delay_ms(100);
	PORTA = 0x55;
	delay_ms(100);
	PORTA = 0x00;
	delay_ms(250);
	PORTA = 0xff;

	// End of init.

	// Setup LED Matrix.
	DumbLEDMatrix dumbLEDMatrix(PortAccess::PortB);
	dumbLEDMatrix.setPixelSwitchDelay(7);
	dumbLEDMatrix.setDisplayMatrix(matrix);
	dumbLEDMatrix.setupDDR();

	Digits::fillMatrix(number, matrix);
	dumbLEDMatrix.setDisplayMatrix(matrix);

	// Setup keyboard.
	dumbKeyboard.setupDDR();

	// Setup delays.
	int numberAutoIncrementCountUp = 0;
	int keyboardReadCountUp = 0;

	while(1)
	{
		dumbLEDMatrix.litUp();

		++keyboardReadCountUp;
		if (keyboardReadCountUp >= KEYBOARD_READ_DELAY)
		{
			keyboardReadCountUp = 0;

			if (readKeyboard())
			{
				dumbLEDMatrix.setDisplayMatrix(matrix);
				numberAutoIncrementCountUp = 0;
			}
		}

		++numberAutoIncrementCountUp;
		if (numberAutoIncrementCountUp >= NUMBER_AUTO_INCREMENT_DELAY)
		{
			numberAutoIncrementCountUp = 0;

			++number;
			if (number > 100)
			{
				number = 0;
			}

			updatePortA(0x00);
			Digits::fillMatrix(number, matrix);
			dumbLEDMatrix.setDisplayMatrix(matrix);
		}

		delay_us(LOOP_DELAY_US);
	}
}

void updatePortA(unsigned char value)
{
	if (PORTA == value || PORTA == (unsigned char)~value)
	{
		PORTA = ~PORTA;
	}
	else
	{
		PORTA = value;
	}
}
