#ifndef F_CPU
#define F_CPU 16000000UL
#endif

#include "global.h"
#include <util/delay.h>
#include <stdlib.h>


void delay_ms(unsigned ms)
{
	while(ms)
	{
		_delay_ms(1);
		--ms;
	}
}

void delay_us(unsigned us)
{
	while(us)
	{
		_delay_us(1);
		--us;
	}
}

void* operator new(size_t size)
{
	return malloc(size);
}

void* operator new[](size_t size)
{
	return malloc(size);
}

void  operator delete(void* pointer)
{
	free (pointer);
}

void  operator delete[](void* pointer)
{
	free (pointer);
}
