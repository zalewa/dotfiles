#ifndef GLOBAL_H_INCLUDED
#define GLOBAL_H_INCLUDED

#include <stddef.h>

#define MAX_BITS 8

typedef unsigned char byte;

void delay_ms(unsigned ms);
void delay_us(unsigned us);

void* operator new(size_t size);
void* operator new[](size_t size);
void operator delete(void* pointer);
void operator delete[](void* pointer);

#endif // GLOBAL_H_INCLUDED
