//
//	LEDOMAT VERSION 2.0
//

#include "global.h"
#include "sequence.h"
#include "sequences.h"
#include <avr/io.h>
#include <avr/interrupt.h>

SequenceList seqList;

int main(void)
{
	// Initialize
	DDRA = 0xff;
	DDRB = 0xff;
	DDRD = 0x00;

	PORTA = 0x00;
	PORTD = 0xff;

	// Enable interrupts
	MCUCR |= (1 << ISC01);
	MCUCR |= (1 << ISC11);
	GICR |= (1 << INT1) | (1 << INT0);
	sei();

	// Construct sequences.
	seqList.addSequence(new StringPrinter("Hello World!", 200));
	seqList.addSequence(new ByteCounter(100));
	seqList.addSequence(new Snake(70, 7));
	seqList.addSequence(new Snake(10, 2));
	seqList.addSequence(new Blinker(350));
	seqList.addSequence(new Blinker(50));
	seqList.addSequence(new Blinker(25));
	seqList.addSequence(new Blinker(10));
	seqList.addSequence(new Blinker(5));
	seqList.addSequence(new Blinker(1));
	seqList.addSequence(new StarTrek(400));
	seqList.addSequence(new StarTrek(100));
	seqList.addSequence(new StarTrek(50));
	seqList.addSequence(new StarTrek(20));
	seqList.addSequence(new OddEven(40));
	seqList.addSequence(new OddEven(200));
	seqList.addSequence(new OddEven(600));

	// Leftover from previous version of the program. This blinked an
	// additional LED attached to PORTD.
//	PortDBlinker portDBlinker(4000);
//	portDBlinker.enter();

	// Main loop
	while(1)
	{
		delay_us(500);
		seqList.run();
		PORTB = PORTA;
	}

	return 0;
}

ISR(INT0_vect)
{
	seqList.gotoNext();
}

ISR(INT1_vect)
{
	seqList.gotoPrev();
}
