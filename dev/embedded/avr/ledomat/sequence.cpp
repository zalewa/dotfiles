#include "sequence.h"

Sequence::Sequence(unsigned delayInTicks)
{
	tickDelay = ticksTillZero = delayInTicks;
}

void Sequence::run()
{
	while (ticksTillZero)
	{
		--ticksTillZero;
		return;
	}

	ticksTillZero = tickDelay;

	doRun();
}

//==============================================================================

SequenceList::SequenceList()
{
	currentSequence = NULL;
	firstSequence = NULL;
}

SequenceList::~SequenceList()
{
	SeqPointer* p = firstSequence;
	while (p != NULL)
	{
		SeqPointer* del = p;
		p = p->next;
		delete del;
	}
}

void SequenceList::addSequence(Sequence* seq)
{
	SeqPointer* prev;
	SeqPointer* p = new SeqPointer();;
	if (firstSequence == NULL)
	{
		prev = NULL;
		firstSequence = p;
	}
	else
	{
		prev = lastP();
		prev->next = p;
	}

	p->prev = prev;
	p->current = seq;
}

Sequence* SequenceList::first()
{
	return firstSequence != NULL ? firstSequence->current : NULL;
}

SequenceList::SeqPointer* SequenceList::firstP()
{
	return firstSequence;
}

void SequenceList::gotoPrev()
{
	if (currentSequence != NULL)
	{
		currentSequence->current->exit();
		currentSequence = currentSequence->prev;
	}

	SeqPointer* lastSequence = lastP();
	if (currentSequence == NULL && lastSequence != NULL)
	{
		currentSequence = lastSequence;
	}

	if (currentSequence != NULL)
	{
		currentSequence->current->enter();
	}
}

void SequenceList::gotoNext()
{
	if (currentSequence != NULL)
	{
		currentSequence->current->exit();
		currentSequence = currentSequence->next;
	}

	if (currentSequence == NULL && firstSequence != NULL)
	{
		currentSequence = firstSequence;
	}

	if (currentSequence != NULL)
	{
		currentSequence->current->enter();
	}
}

Sequence* SequenceList::last()
{
	SeqPointer* last = lastP();
	return last != NULL ? last->current : NULL;
}

SequenceList::SeqPointer* SequenceList::lastP()
{
	if (firstSequence == NULL)
	{
		return NULL;
	}

	SeqPointer* p = firstSequence;
	while (p->next)
	{
		p = p->next;
	}

	return p;
}

void SequenceList::run()
{
	if (currentSequence != NULL)
	{
		currentSequence->current->run();
	}
	else
	{
		gotoNext();
		if (currentSequence != NULL)
		{
			currentSequence->current->run();
		}
	}
}
