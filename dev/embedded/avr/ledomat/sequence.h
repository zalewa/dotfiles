#ifndef SEQUENCE_H_INCLUDED
#define SEQUENCE_H_INCLUDED

#include <stdlib.h>

class Sequence
{
	public:
		Sequence(unsigned delayInTicks);
		virtual ~Sequence() {}

		unsigned delay() const { return tickDelay; }

		/**
		 * Method executed upon entering or reentering the sequence.
		 */
		virtual void enter() {};

		/**
		 * Method executed upon exitting the sequence.
		 */
		virtual void exit() {};

		/**
		 * Method executed every tick.
		 */
		void run();

		void setDelay(unsigned ticks) { ticksTillZero = tickDelay = ticks; }

	protected:
		unsigned tickDelay;
		unsigned ticksTillZero;

		/**
		 * Method executed by run() method.
		 */
		virtual void doRun() {};
};

class SequenceList
{
	public:
		SequenceList();
		~SequenceList();

		/**
		 * Sequence list takes ownership of the sequence. The sequence
		 * will be destroyed together with the list.
		 */
		void addSequence(Sequence* seq);

		Sequence* current() { return currentSequence != NULL ? currentSequence->current : NULL; }

		Sequence* first();

		/**
		 * This will immediatelly go to the last sequence when iterator goes
		 * before the first element.
		 * Sequence::enter() and Sequence::exit() methods
		 * are executed where applicable.
		 */
		void gotoPrev();

		/**
		 * This will immediatelly go to first sequence when end of the list
		 * is detected. Sequence::enter() and Sequence::exit() methods
		 * are executed where applicable.
		 */
		void gotoNext();

		Sequence* last();

		/**
		 * Calls run method of the current sequence.
		 */
		void run();

	protected:
		struct SeqPointer
		{
			SeqPointer()
			{
				current = NULL;
				next = NULL;
				prev = NULL;
			}

			~SeqPointer()
			{
				if (current != NULL)
					delete current;
			}

			Sequence* current;
			SeqPointer* next;
			SeqPointer* prev;
		};

		SeqPointer* currentSequence;
		SeqPointer* firstSequence;

		SeqPointer* firstP();
		SeqPointer* lastP();
};

#endif // SEQUENCE_H_INCLUDED
