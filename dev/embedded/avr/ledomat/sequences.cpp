#include "sequences.h"
#include <avr/io.h>
#include <string.h>

void EmptySequence::enter()
{
	PORTA = 0xff;
}

////////////////////////////////////////////////////////////////////////////////

StringPrinter::StringPrinter(const char* str, unsigned delayInTicks)
:Sequence(delayInTicks),length(strlen(str))
{
	displayString = new char[length + 1];
	strcpy(displayString, str);
}

StringPrinter::~StringPrinter()
{
	delete [] displayString;
}

void StringPrinter::doRun()
{
	if (displayString[index] == '\0')
	{
		ticksTillZero = tickDelay * 3;
		index = 0;
		PORTA = 0xff;
	}
	else
	{
		PORTA = ~displayString[index++];
	}
}

void StringPrinter::enter()
{
	ticksTillZero = tickDelay;
	index = 0;
	PORTA = 0xff;
}

////////////////////////////////////////////////////////////////////////////////

ByteCounter::ByteCounter(unsigned delayInTicks)
:Sequence(delayInTicks)
{
	counter = 0;
}

void ByteCounter::doRun()
{
	++counter;
	PORTA = ~counter;
}

void ByteCounter::enter()
{
	ticksTillZero = tickDelay;
	PORTA = ~counter;
}

void ByteCounter::exit()
{
}

////////////////////////////////////////////////////////////////////////////////

Snake::Snake(unsigned delayInTicks, char snakeLength)
:Sequence(delayInTicks), length(snakeLength < 8 ? snakeLength : 8)
{
	if (length == 0)
	{
		length = 1;
	}

	position = 0;
}

void Snake::doRun()
{
	++position;
	if (position >= MAX_BITS)
	{
		position = -(length - 1);
	}

	setPosition();
}

void Snake::enter()
{
	ticksTillZero = tickDelay;
	position = -(length - 1);
	setPosition();
}

void Snake::setPosition()
{
	byte snakeBits = 0;
	for (char x = 0; x < length; ++x)
	{
		snakeBits |= 1;
		if (x != length - 1)
		{
			snakeBits <<= 1;
		}
	}

	if (position > 0)
	{
		snakeBits <<= position;
	}
	else if (position < 0)
	{
		snakeBits >>= (position * -1);
	}

	PORTA = ~snakeBits;
}

////////////////////////////////////////////////////////////////////////////////

Blinker::Blinker(unsigned delayInTicks) : Sequence(delayInTicks)
{
	leds = 0b01010101;
}

void Blinker::doRun()
{
	leds = ~leds;
	PORTA = ~leds;
}

void Blinker::enter()
{
	ticksTillZero = tickDelay;
	PORTA = ~leds;
}

////////////////////////////////////////////////////////////////////////////////

StarTrek::StarTrek(unsigned delayInTicks)
:Sequence(delayInTicks)
{

}

void StarTrek::doRun()
{
	// Lower 4 bits of the final output
	if (hibits >= 0x08)
	{
		hibits = 0x01;
	}
	else
	{
		hibits <<= 1;
	}

	// Higher 4 bits of the final output
	if (lobits == 0x01)
	{
		lobits = 0x08;
	}
	else
	{
		lobits >>= 1;
	}

	PORTA = ~((hibits << 4) | lobits);
}

void StarTrek::enter()
{
	ticksTillZero = tickDelay;
	lobits = 0x08;
	hibits = 0x01;
	PORTA = ~((hibits << 4) | lobits);
}

////////////////////////////////////////////////////////////////////////////////

OddEven::OddEven(unsigned delayInTicks)
:Sequence(delayInTicks)
{

}

void OddEven::doRun()
{
	bit <<= 2;
	if (bit == 0)
	{
		if (!evenNow)
		{
			bit = 2;
		}
		else
		{
			bit = 1;
		}
		evenNow = !evenNow;
	}

	PORTA = ~bit;
}

void OddEven::enter()
{
	bit = 1;
	PORTA = ~bit;
	evenNow = false;
}

////////////////////////////////////////////////////////////////////////////////

PortDBlinker::PortDBlinker(unsigned delayInTicks)
: Sequence(delayInTicks)
{

}

void PortDBlinker::doRun()
{
	if (PORTD == 0x01)
	{
		PORTD = 0x00;
	}
	else
	{
		// If we're turning the LED off, we need to lower the time
		// necessary to turn it back on.
		ticksTillZero >>= 1;

		PORTD = 0x01;
	}
}

void PortDBlinker::enter()
{
	PORTD = 0x00;
}
