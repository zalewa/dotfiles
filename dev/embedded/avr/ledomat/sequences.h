#ifndef SEQUENCES_H_INCLUDED
#define SEQUENCES_H_INCLUDED

#include "global.h"
#include "sequence.h"

class EmptySequence : public Sequence
{
	public:
		EmptySequence() : Sequence(1) {}

		void enter();

	protected:
		void doRun() {}
};

class StringPrinter : public Sequence
{
	public:
		StringPrinter(const char* str, unsigned delayInTicks);
		~StringPrinter();

		void enter();

	protected:
		char* displayString;
		byte index;
		const byte length;

		void doRun();
};

class ByteCounter : public Sequence
{
	public:
		ByteCounter(unsigned delayInTicks);

		void enter();
		void exit();

	protected:
		byte counter;

		void doRun();
};

class Snake : public Sequence
{
	public:
		Snake(unsigned delayInTicks, char snakeLength);

		void enter();

	protected:
		char 	length;
		char	position;

		void doRun();
		void setPosition();
};

class Blinker : public Sequence
{
	public:
		Blinker(unsigned delayInTicks);

		void enter();

	protected:
		byte leds;

		void doRun();
};

class StarTrek : public Sequence
{
	public:
		StarTrek(unsigned delayInTicks);

		void enter();

	protected:
		byte hibits, lobits;

		void doRun();
};

class OddEven : public Sequence
{
	public:
		OddEven(unsigned delayInTicks);

		void enter();

	protected:
		bool evenNow;
		byte bit;

		void doRun();
};

/**
 * @brief Sequence that lights a LED on PORTD.
 *
 * This sequence operates on a separate port so it shouldn't be added to the
 * list. In addition in this sequence the LED is turned on with a low state and
 * turned off with a high state.
 */
class PortDBlinker : public Sequence
{
	public:
		PortDBlinker(unsigned delayInTicks);

		void enter();

	protected:
		void doRun();
};

#endif // SEQUENCES_H_INCLUDED
