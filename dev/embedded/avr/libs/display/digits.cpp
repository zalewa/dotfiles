#include "digits.h"
#include <stdlib.h>
#include <string.h>

#ifndef byte
#	define byte unsigned char
#endif

const unsigned char Digits::DIGIT_0[DIGIT_MATRIX_SIZE] =
{
	0b11111111,
	0b10000001,
	0b11111111,
	0b00000000
};

const unsigned char Digits::DIGIT_1[DIGIT_MATRIX_SIZE] =
{
	0b11111111,
	0b01100000,
	0b00110000,
	0b00000000
};

const unsigned char Digits::DIGIT_2[DIGIT_MATRIX_SIZE] =
{
	0b01110001,
	0b10001001,
	0b10000101,
	0b01000011
};

const unsigned char Digits::DIGIT_3[DIGIT_MATRIX_SIZE] =
{
	0b01101110,
	0b10010001,
	0b10000001,
	0b01000010
};

const unsigned char Digits::DIGIT_4[DIGIT_MATRIX_SIZE] =
{
	0b11111111,
	0b01001000,
	0b00101000,
	0b00011000,
};

const unsigned char Digits::DIGIT_5[DIGIT_MATRIX_SIZE] =
{
	0b10001110,
	0b10010001,
	0b11110001,
	0b00000000
};

const unsigned char Digits::DIGIT_6[DIGIT_MATRIX_SIZE] =
{
	0b10000110,
	0b10001001,
	0b01111110,
	0b00000000
};

const unsigned char Digits::DIGIT_7[DIGIT_MATRIX_SIZE] =
{
	0b11111111,
	0b10000000,
	0b11110000,
	0b00000000
};

const unsigned char Digits::DIGIT_8[DIGIT_MATRIX_SIZE] =
{
	0b01101110,
	0b10010001,
	0b01101110,
	0b00000000
};

const unsigned char Digits::DIGIT_9[DIGIT_MATRIX_SIZE] =
{
	0b01111110,
	0b10010001,
	0b01100001,
	0b00000000
};

const unsigned char* Digits::DIGITS[10] =
{
	DIGIT_0,
	DIGIT_1,
	DIGIT_2,
	DIGIT_3,
	DIGIT_4,
	DIGIT_5,
	DIGIT_6,
	DIGIT_7,
	DIGIT_8,
	DIGIT_9
};

const unsigned char Digits::ERR_MATRIX[8] =
{
	0b11100011,
	0b10010100,
	0b10011000,
	0b11111111,
	0b00000000,
	0b10000001,
	0b10010001,
	0b11111111
};

void Digits::copy(unsigned char* targetMatrix, const unsigned char* sourceMatrix, bool bIsNumberTwoDigit, int digitIndex)
{
	static const int SOURCE_MATRIX_SIZE = 4;

	int copyIndex = 0;

	if (bIsNumberTwoDigit)
	{
		copyIndex = SOURCE_MATRIX_SIZE * (1 - digitIndex);
	}
	else
	{
		copyIndex = (TARGET_MATRIX_SIZE - 1) / 2;
	}

	memcpy(&targetMatrix[copyIndex], sourceMatrix, SOURCE_MATRIX_SIZE);
}

void Digits::fillMatrix(unsigned number, unsigned char* outMatrix)
{
	// Convert the number to string.
	char numberString[16];
	itoa(number, numberString, 10);

	int iNumLen = strlen(numberString);

	// Zero the output matrix.
	memset(outMatrix, 0, TARGET_MATRIX_SIZE);

	if (iNumLen > 2)
	{
		// Display error.
		memcpy(outMatrix, ERR_MATRIX, 8);
	}
	else
	{
		bool bIsNumberTwoDigit = false;
		if (iNumLen == 2)
		{
			bIsNumberTwoDigit = true;
		}

		// Iterate through number string to extract each digits;
		for (int i = 0; i < iNumLen; ++i)
		{
			int digitValue = numberString[i] - '0';

			const byte* digitMatrix = DIGITS[digitValue];

			copy(outMatrix, digitMatrix, bIsNumberTwoDigit, i);
		}
	}
}
