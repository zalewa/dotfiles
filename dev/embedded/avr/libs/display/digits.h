#ifndef DIGITS_H_INCLUDED
#define DIGITS_H_INCLUDED

class Digits
{
	public:
		static const int TARGET_MATRIX_SIZE = 8;

		/**
		 * @brief Converts an integer unsigned number to a display matrix.
		 *
		 * @param number - number between 0 and 99 (inclusive).
		 * @param [out] matrix - An array of 8 bytes.
		 */
		static void fillMatrix(unsigned number, unsigned char* outMatrix);

	private:
		static const int DIGIT_MATRIX_SIZE = 4;

		static const unsigned char DIGIT_0[DIGIT_MATRIX_SIZE];
		static const unsigned char DIGIT_1[DIGIT_MATRIX_SIZE];
		static const unsigned char DIGIT_2[DIGIT_MATRIX_SIZE];
		static const unsigned char DIGIT_3[DIGIT_MATRIX_SIZE];
		static const unsigned char DIGIT_4[DIGIT_MATRIX_SIZE];
		static const unsigned char DIGIT_5[DIGIT_MATRIX_SIZE];
		static const unsigned char DIGIT_6[DIGIT_MATRIX_SIZE];
		static const unsigned char DIGIT_7[DIGIT_MATRIX_SIZE];
		static const unsigned char DIGIT_8[DIGIT_MATRIX_SIZE];
		static const unsigned char DIGIT_9[DIGIT_MATRIX_SIZE];

		static const unsigned char* DIGITS[10];

		static const unsigned char ERR_MATRIX[8];

		static void copy(unsigned char* targetMatrix, const unsigned char* sourceMatrix, bool bIsNumberTwoDigit, int digitIndex);
};

#endif // DIGITS_H_INCLUDED
