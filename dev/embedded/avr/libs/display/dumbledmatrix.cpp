#include "dumbledmatrix.h"

#include <string.h>

DumbLEDMatrix::Members::Members()
{
	currentPixelIndex = 0;
	currentPixelSwitchDelay = 0;
	pixelSwitchDelay = 10;
	port = PortAccess::PortA;
	numPixels = 0;
}

DumbLEDMatrix::DumbLEDMatrix()
{
}

DumbLEDMatrix::DumbLEDMatrix(PortAccess::Ports port)
{
	data.port = port;
}

uint8_t DumbLEDMatrix::generateOutputBits(uint8_t subIndex) const
{
	// What happens here is this:
	// 0babc -> 0ba0b0c

	uint8_t bit0 = (subIndex & 0x01);
	uint8_t bit2 = (subIndex & 0x02) << 1;
	uint8_t bit4 = (subIndex & 0x04) << 2;

	return bit0 | bit2 | bit4;
}

void DumbLEDMatrix::litUp()
{
	if (data.currentPixelSwitchDelay != 0)
	{
		--data.currentPixelSwitchDelay;
		return;
	}
	data.currentPixelSwitchDelay = data.pixelSwitchDelay;

	if (data.numPixels == 0)
	{
		PortAccess::setPort(data.port, SWITCH_OFF_SIGNATURE);
		return;
	}

	if (data.currentPixelIndex >= data.numPixels)
	{
		data.currentPixelIndex = 0;
	}
	uint8_t pixel = data.pixels[data.currentPixelIndex++];
	PortAccess::setPort(data.port, pixel);
}

void DumbLEDMatrix::setDisplayMatrix(const uint8_t *bitMatrix)
{
	data.currentPixelIndex = 0;
	uint8_t pixel = 0;
	uint8_t bit = 0;
	for (uint8_t row = 0; row < MATRIX_ARRAY_SIZE; ++row)
	{
		bit = bitMatrix[row];
		for (uint8_t column = 0; column < 8; ++column)
		{
			if (((bit >> column) & 1) != 0)
			{
#ifdef LEDMATRIX_LEGACY
				uint8_t columnPins = generateOutputBits(column) << 1;
				uint8_t rowPins = generateOutputBits(row) << 0;
#else
				uint8_t columnPins = generateOutputBits(7 - column); // Big endian
				uint8_t rowPins = generateOutputBits(row) << 1;
#endif

				uint8_t pixelPins = columnPins | rowPins;
				data.pixels[pixel++] = pixelPins;
			}
		}
	}
	data.numPixels = pixel;
}

void DumbLEDMatrix::setupDDR()
{
	PortAccess::setDDR(data.port, 0xff);
}
