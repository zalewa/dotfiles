#ifndef DUMBLEDMATRIX_H_INCLUDED
#define DUMBLEDMATRIX_H_INCLUDED

#include <global/atmega16.h>
#include <stdlib.h>
#include <stdint.h>

/**
 * @brief Supports a specific LED matrix device.
 *
 * @section Target device.
 *
 * A 8x8 LED matrix using two HEF4051 BP multiplexers.
 *
 * @section Connector layout
 *
 * ML10 connector on the board.
 * @code
 *
 *    |-----|
 *    |10  9|
 *    |8   7|
 *    |6   5|
 *    |4   3|
 *    |2   1|
 *    |-----|
 * @endcode
 * 1, 3, 5 - column address bits
 * 2, 4, 6 - row address bits
 * 7, 8 - column/row enable bits.
 *
 * @section Principles of working.
 *
 * - Entire port is set to output.
 * - Pins 7 and 8 are set to zero (enabled).
 * - Remaining pins are set to address the lit up LED.
 *
 * If there are no lit up LEDs (entire matrix is set to zero) then pins 7
 * and 8 are set to one (disabled).
 *
 * @section Usage
 *
 * Example:
 * @code
 *
 * // Let's draw a checkboard.
 * uint8_t matrix[8] =
 * {
 *     0b10101010,
 *     0b01010101,
 *     0b10101010,
 *     0b01010101,
 *     0b10101010,
 *     0b01010101,
 *     0b10101010,
 *     0b01010101,
 * };
 *
 * int main()
 * {
 *     DumbLEDMatrix dumbLEDMatrix(PortAccess::PortB);
 *     dumbLEDMatrix.setPixelSwitchDelay(10);
 *     dumbLEDMatrix.setDisplayMatrix(matrix);
 *     dumbLEDMatrix.setupDDR();
 *
 *     // This code will keep each pixel lit up for 10ms. Switching through
 *     // each LED should be clearly visible.
 *     while(1)
 *     {
 *          dumbLEDMatrix.litUp();
 *          delay_ms(1);
 *     }
 * }
 * @endcode
 */
class DumbLEDMatrix
{
	public:
		static const uint8_t NUM_PIXELS = 64;
		static const uint8_t MATRIX_ARRAY_SIZE = 8;

		DumbLEDMatrix();
		DumbLEDMatrix(PortAccess::Ports port);

		/**
		 * @brief Lits up next pixel if delay time has been reached.
		 *
		 * Each call to this function reduces the currentPixelSwitchDelay time
		 * by one. When zero is reached the next pixel is lit up and
		 * delay is reset to pixelSwitchDelay.
		 *
		 * @see setPixelSwitchDelay()
		 */
		void litUp();

		int pixelSwitchDelay() const { return data.pixelSwitchDelay; }

		/**
		 * @brief Sets a new bit matrix to display.
		 *
		 * Bit matrix is an array of 8 one-byte values. DumbLEDMatrix doesn't
		 * take ownership of this object and old matrix is not deleted
		 * when a new one is assigned.
		 */
		void setDisplayMatrix(const uint8_t *bitMatrix);

		void setPixelSwitchDelay(uint16_t delay)
		{
			data.currentPixelSwitchDelay = 0;
			data.pixelSwitchDelay = delay;
		}

		/**
		 * @brief Sets up DDR to use with this LED Matrix.
		 *
		 * Call this once.
		 */
		void setupDDR();

	private:
		static const uint8_t SWITCH_OFF_SIGNATURE = 0xc0;

		class Members
		{
			public:
				uint8_t currentPixelIndex;
				uint16_t currentPixelSwitchDelay;
				uint8_t numPixels;
				uint8_t pixels[NUM_PIXELS];
				uint16_t pixelSwitchDelay;
				PortAccess::Ports port;

				Members();
		};

		Members data;

		uint8_t generateOutputBits(uint8_t subIndex) const;
};

#endif // DUMBLEDMATRIX_H_INCLUDED
