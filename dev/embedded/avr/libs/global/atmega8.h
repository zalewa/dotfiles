#ifndef __ATMEGA8_H_
#define __ATMEGA8_H_

#include <avr/io.h>
#include <avr/interrupt.h>
#include "global.h"

#define PORTB_ADDRESS (byte*)0x0038
#define PORTC_ADDRESS (byte*)0x0035
#define PORTD_ADDRESS (byte*)0x0032

class PortAccess
{
	public:
		enum Ports
		{
			PortB,
			PortC,
			PortD
		};
		
		static unsigned char getDDR(Ports port)
		{
			switch(port)
			{
				case PortB:
					return DDRB;

				case PortC:
					return DDRC;

				case PortD:
					return DDRD;
			}

			return 0;
		}

		static unsigned char getPin(Ports port)
		{
			switch(port)
			{
				case PortB:
					return PINB;

				case PortC:
					return PINC;

				case PortD:
					return PIND;
			}

			return 0;
		}

		static unsigned char getPort(Ports port)
		{
			switch(port)
			{
				case PortB:
					return PORTB;

				case PortC:
					return PORTC;

				case PortD:
					return PORTD;
			}

			return 0;
		}
		
		static void setDDR(Ports port, unsigned char value)
		{
			switch(port)
			{
				case PortB:
					DDRB = value;
					break;

				case PortC:
					DDRC = value;
					break;

				case PortD:
					DDRD = value;
					break;
			}
		}

		static void	setPin(Ports port, unsigned char value)
		{
			switch(port)
			{
				case PortB:
					PINB = value;
					break;

				case PortC:
					PINC = value;
					break;

				case PortD:
					PIND = value;
					break;
			}
		}		
		
		static void	setPort(Ports port, unsigned char value)
		{
			switch(port)
			{
				case PortB:
					PORTB = value;
					break;

				case PortC:
					PORTC = value;
					break;

				case PortD:
					PORTD = value;
					break;
			}
		}
};

class Interrupts
{
	public:
		static void enableInterrups()
		{
			sei();
		}
};

class ClockInterrupts 
{
	public:
		struct ClockDelay
		{
			enum Prescaler
			{
				Scale1 		= 1,
				Scale8 		= 8,
				Scale64 	= 64,
				Scale256	= 256,
				Scale1024	= 1024,
				Fail		= 0
			};

			Prescaler prescaler;
			short cycles;
		};
		
		static void calculateClockForHzDelay(long herz, ClockDelay& outDelay)
		{
			/// 0 is End of Array.
			short availablePrescalers[] = { 1, 8, 64, 256, 1024, 0 };
		
			for(int prescalerIndex = 0; availablePrescalers[prescalerIndex] != 0; ++prescalerIndex)
			{
				short prescaler = availablePrescalers[prescalerIndex];
				float result = (float)F_CPU / (float)prescaler;
				result /= (float)herz;

				if (floor_(result) == result && result < 65535.0f && result > 0.0f)
				{
					outDelay.prescaler = (ClockDelay::Prescaler) prescaler;
					outDelay.cycles = (short)result - 1;
					return;
				}
			}

			outDelay.prescaler = ClockDelay::Fail;		
		}
	
		static void setPrescalerForR1B(short prescaler)
		{
			ClockDelay::Prescaler enumPrescaler = (ClockDelay::Prescaler)prescaler;
			byte flags = 0x00;

			switch (enumPrescaler)
			{
				case ClockDelay::Scale1:
					flags |= (1 << CS10);
					break;

				case ClockDelay::Scale8:
					flags |= (1 << CS11);
					break;

				case ClockDelay::Scale64:
					flags |= (1 << CS11) | (1 << CS10);
					break;

				case ClockDelay::Scale256:
					flags |= (1 << CS12);
					break;

				case ClockDelay::Scale1024:
					flags |= (1 << CS12) | (1 << CS10);
					break;

				default:
					// flags remain set to zero.
					break;
			}

			TCCR1B &= 0xf8;
			TCCR1B |= flags;
		}
		
		static void setupClockR1BInterrupt(ClockDelay::Prescaler scale, int cycles)
		{
			setPrescalerForR1B((short)scale);

			TCCR1B |= (1 << WGM12);

			// Enable CTC interrupt.
			TIMSK |= (1 << OCIE1A);

			OCR1A = cycles;
		}
};

#endif // __ATMEGA8_H_
