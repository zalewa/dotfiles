#include "global.h"
#include <stdlib.h>

#ifdef DEBUG
#include <stdio.h>
#else
#include <util/delay.h>
#endif

long bitFromIndex(byte bitIndex)
{
	return (1 << bitIndex);
}

void delay_ms(long ms)
{
	#ifdef DEBUG
	return;
	#else
	while(ms)
	{
		delay_us(1000);
		--ms;
	}
	#endif
}

void delay_us(long us)
{
	#ifdef DEBUG
	return;
	#else
	#define STEP 10

	while(us)
	{
		us -= STEP;
		_delay_us(STEP);
	}
	#endif
}

void delay_herz(long herz)
{
	for (; herz > 0; --herz);
}

float floor_(float f)
{
	return (float)((int)f);
}

void* operator new(size_t size)
{
	return malloc(size);
}

void* operator new[](size_t size)
{
	return malloc(size);
}

void  operator delete(void* pointer)
{
	free (pointer);
}

void  operator delete[](void* pointer)
{
	free (pointer);
}
