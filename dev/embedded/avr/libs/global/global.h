#ifndef GLOBAL_H
#define GLOBAL_H

#include <stddef.h>

#ifndef MAX_BITS
/// Maximum number of bits in a byte.
#define MAX_BITS 8
#endif

#ifndef byte
#define byte unsigned char
#endif

long bitFromIndex(byte bitIndex);

void delay_herz(long herz);
void delay_ms(long ms);
void delay_us(long us);

float floor_(float f);

void* operator new(size_t size);
void* operator new[](size_t size);
void operator delete(void* pointer);
void operator delete[](void* pointer);

#endif // GLOBAL_H
