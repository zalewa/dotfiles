#include "dumbkeyboard.h"
#include <util/delay.h>
#include <global/global.h>

#ifndef byte
#	define byte unsigned char
#endif

unsigned char DumbKeyboard::concatenate(unsigned char buttonInput)
{
	byte b1 = (buttonInput & 0x01);
	byte b2 = (buttonInput & 0x04) >> 2;
	byte b3 = (buttonInput & 0x10) >> 4;
	byte b4 = (buttonInput & 0x40) >> 6;

	byte concatenated = (b1) | (b2 << 1) | (b3 << 2) | (b4 << 3);

	return concatenated;
}

unsigned short DumbKeyboard::getKeyboardStatus(int delay)
{
	unsigned short keyboardStatus = 0;

	for (byte i = 0; i < 4; ++i)
	{
		byte keyboardShift = i * 4;
		byte rowShift = i * 2 + 1;
		byte rowOutput = ~(1 << rowShift);

		PortAccess::setPort(port, rowOutput);
		delay_ms(delay);
		byte rowInput = ~PortAccess::getPin(port);

		unsigned short columnStatus = concatenate(rowInput);
		keyboardStatus |= columnStatus << keyboardShift;
	}

	return keyboardStatus;
}

void DumbKeyboard::setupDDR()
{
	PortAccess::setDDR(port, 0xAA);
}
