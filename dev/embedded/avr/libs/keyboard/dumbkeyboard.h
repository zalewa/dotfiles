#ifndef DUMBKEYBOARD_H_INCLUDED
#define DUMBKEYBOARD_H_INCLUDED

#include <global/atmega16.h>

/**
 * @brief Supports a specific keyboard device.
 *
 * @section Connector layout
 *
 * ML10 connector on the board.
 * @code
 *
 *     |-----|
 *     |10  9|
 *     |8   7|
 *     |6   5|
 *     |4   3|
 *     |2   1|
 *     |-----|
 * @endcode
 * 1, 3, 5, 7 - button columns
 * 2, 4, 6, 8 - button rows
 * 9, 10 - nothing.
 *
 * @section Principles of working.
 *
 * Column pins are set to input pull-up and row pins are set to output.
 * Class iterates through columns setting one of the column pins to 0 and
 * others to 1. Each time a 0 appears on one of the row pins it is checked
 * which column currently had 0 set. Then button code is returned.
 */
class DumbKeyboard
{
	public:
		DumbKeyboard(PortAccess::Ports port)
		{
			this->port = port;
		}

		/**
		 * @brief Returns status of all 16 keys.
		 */
		unsigned short getKeyboardStatus(int delay);

		/**
		 * @brief Sets up DDR for use with the keyboard.
		 *
		 * Call this once.
		 */
		void setupDDR();

	protected:
		PortAccess::Ports port;

		unsigned char concatenate(unsigned char buttonInput);
};

#endif // DUMBKEYBOARD_H_INCLUDED
