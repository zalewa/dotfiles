#include "keystates.h"

KeyStates::KeyStates()
{
	currentKeyStates = 0;
}

void KeyStates::update(unsigned short newKeyStates, unsigned short& outPressed, unsigned short& outUnpressed)
{
	unsigned short diff = newKeyStates ^ currentKeyStates;

	outPressed = diff & newKeyStates;
	outUnpressed = diff & currentKeyStates;

	currentKeyStates = newKeyStates;
}
