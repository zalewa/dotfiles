#ifndef __KEYSTATES_H__
#define __KEYSTATES_H__

class KeyStates
{
	public:
		KeyStates();

		/**
		 * @brief Updates stored key states and returns state changes through
		 * outPressed and outUnpressed parameters.
		 */
		void update(unsigned short newKeyStates, unsigned short& outPressed, unsigned short& outUnpressed);

	private:
		unsigned short currentKeyStates;
};

#endif
