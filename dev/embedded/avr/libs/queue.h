#include <stdlib.h>

template<class T, size_t maxSize>
class Queue
{
public:
	Queue()
	{
		pushIndex = 0;
		popIndex = 0;
		count = 0;
	}

	bool empty() const volatile
	{
		return size() == 0;
	}

	bool full() const volatile
	{
		return size() == maxSize;
	}

	void push(const T &value) volatile
	{
		if (count < maxSize)
		{
			buffer[pushIndex++] = value;
			pushIndex %= maxSize;
			++count;
		}
	}

	void pop() volatile
	{
		if (count > 0)
		{
			popIndex = (popIndex + 1) % maxSize;
			--count;
		}
	}

	const volatile T &top() const volatile
	{
		return buffer[popIndex];
	}

	size_t size() const volatile
	{
		return (popIndex - pushIndex) % maxSize;
	}

private:
	int16_t pushIndex;
	int16_t popIndex;
	size_t count;
	T buffer[maxSize];
};
