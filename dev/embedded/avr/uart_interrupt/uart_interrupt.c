#include <stdint.h>
#include <stdlib.h>

#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/atomic.h>
#include <util/setbaud.h>


typedef struct _node_t
{
	uint8_t value;
	struct _node_t *next;
} _node;

typedef struct stack_t
{
	_node *node;
	size_t size;
} stack;


_node *allocNode()
{
	_node *st = malloc(sizeof(_node));
	st->next = NULL;
	st->value = 0;
	return st;
}

void pushStack(volatile stack *st, uint8_t value)
{
	if (st->node == NULL)
	{
		st->node = allocNode();
		st->node->value = value;
	}
	else
	{
		_node *node = st->node;
		while (node->next != NULL)
		{
			node = node->next;
		}
		node->next = allocNode();
		node->next->value = value;
	}
	++st->size;
}

uint8_t popStack(volatile stack *st)
{
	if (st->node != NULL)
	{
		_node *node = st->node;
		uint8_t value = node->value;
		st->node = node->next;
		free(node);
		--st->size;
		return value;
	}
	return 'X';
}

void uartInit()
{
	UBRRH = UBRRH_VALUE;
	UBRRL = UBRRL_VALUE;
	#if USE_2X
	UCSRA |= (_BV(U2X));
	#else
	UCSRA &= ~(_BV(U2X));
	#endif

	UCSRB = _BV(RXCIE) | _BV(TXEN) | _BV(RXEN);
	UCSRC = _BV(URSEL) | _BV(UCSZ0) | _BV(UCSZ1);
}

/*
  volatile is needed or else compiler may incorrectly
  optimise the variable out in main()
*/
volatile stack uartStack;

ISR(USART_RXC_vect)
{
	uint8_t value = UDR;
	pushStack(&uartStack, value);
}

int main()
{
	uartStack.node = NULL;
	uartStack.size = 0;

	uartInit();
	sei();
	DDRA = 0xff;
	PORTA = 0x00;
	while (1)
	{
		if (uartStack.size > 0)
		{
			while (uartStack.size > 0)
			{
				uint8_t value;
				ATOMIC_BLOCK(ATOMIC_FORCEON)
				{
					value = popStack(&uartStack);
				}
				while ((UCSRA & _BV(UDRE)) == 0); // wait
				UDR = value;
			}
		}
	}
	return 0;
}
