#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/atomic.h>
#include <util/delay.h>
#include <util/setbaud.h>
#include <display/dumbledmatrix.h>
#include <global/global.h>
#include <queue.h>
#include <string.h>


const size_t MAX_MSG_LEN = 64;

unsigned char matrix[8];
DumbLEDMatrix ledMatrix;
volatile Queue<uint8_t, MAX_MSG_LEN> uart;
bool hasCmd = false;

void interpret();
void interpretXy(char *buffer);
void interpretMap(char *buffer);
void interpretRow(char *buffer);
void uartInit();
size_t uartWrite(const uint8_t *buffer, size_t len);
void uartWriteFully(const uint8_t *buffer, size_t len);

int main(void)
{
	uartInit();
	sei();
	// Const definitions.
	const int LOOP_DELAY_US = 250;

	// Init display
	DDRA = 0xff;
	PORTA = 0xaa;

	delay_ms(100);
	PORTA = 0x55;
	delay_ms(100);
	PORTA = 0x00;
	delay_ms(250);
	PORTA = 0b01111110;

	// End of init.

	// Setup LED Matrix.
	matrix[0] = 0b00001111;
	matrix[3] = 0b11110000;
	ledMatrix = DumbLEDMatrix(PortAccess::PortB);
	ledMatrix.setPixelSwitchDelay(7);
	ledMatrix.setDisplayMatrix(matrix);
	ledMatrix.setupDDR();

	while(1)
	{
		if (hasCmd == true && !uart.empty())
		{
			interpret();
			hasCmd = false;
			const char *reply = "OK\n";
			uartWriteFully(reinterpret_cast<const uint8_t*>(reply), 3);
		}
		ledMatrix.litUp();
		delay_us(LOOP_DELAY_US);
	}
	return 0;
}

ISR(USART_RXC_vect)
{
	uint8_t value = UDR;
	if (uart.full())
	{
		uart.pop();
	}
	uart.push(value);
	if (value == '\n')
	{
		hasCmd = true;
	}
}

size_t uartWrite(const uint8_t *buffer, size_t len)
{
	size_t amount = 0;
	while ((UCSRA & _BV(UDRE)) && amount < len)
	{
		UDR = buffer[amount++];
	}
	return amount;
}

void uartWriteFully(const uint8_t *buffer, size_t len)
{
	while (len > 0)
	{
		size_t written = uartWrite(buffer, len);
		buffer += written;
		len -= written;
	}
}

void interpret()
{
	char buffer[MAX_MSG_LEN + 1];
	size_t size = 0;
	ATOMIC_BLOCK(ATOMIC_FORCEON)
	{
		for (size = 0; size < MAX_MSG_LEN && !uart.empty(); ++size)
		{
			buffer[size] = uart.top();
			uart.pop();
		}
	}
	buffer[size] = '\0';

	if (memcmp(buffer, "XY", 2) == 0)
	{
		interpretXy(buffer+2);
	}
	else if (memcmp(buffer, "MAP", 3) == 0)
	{
		interpretMap(buffer+3);
	}
	else if (memcmp(buffer, "ROW", 3) == 0)
	{
		interpretRow(buffer+3);
	}
}

void interpretXy(char *buffer)
{
	char *xBegin = strpbrk(buffer, "0123456789");
	char *xEnd = strpbrk(xBegin, " ");

	char *yBegin = strpbrk(xEnd, "0123456789");
	char *yEnd = strpbrk(yBegin, " ");

	uint8_t state = atoi(strpbrk(yEnd, "01"));

	uint8_t x = atoi(xBegin);
	uint8_t y = atoi(yBegin);

	if (state == 0)
	{
		matrix[y] &= ~(1 << x);
	}
	else if (state == 1)
	{
		matrix[y] |= (1 << x);
	}
	ledMatrix.setDisplayMatrix(matrix);
}

void interpretMap(char *buffer)
{
	for (uint8_t idx = 0; idx < 8 && buffer != NULL; ++idx)
	{
		buffer = strpbrk(buffer, "0123456789");
		if (buffer != NULL)
		{
			matrix[idx] = (uint8_t)atoi(buffer);
			buffer = strpbrk(buffer, " ");
		}
	}
	ledMatrix.setDisplayMatrix(matrix);
}

void interpretRow(char *buffer)
{
	char *begin = strpbrk(buffer, "0123456789");
	unsigned char row = atoi(begin);
	PORTA = row;
}

void uartInit()
{
	UBRRH = UBRRH_VALUE;
	UBRRL = UBRRL_VALUE;
	#if USE_2X
	UCSRA |= (_BV(U2X));
	#else
	UCSRA &= ~(_BV(U2X));
	#endif

	UCSRB = _BV(RXCIE) | _BV(TXEN) | _BV(RXEN);
	UCSRC = _BV(URSEL) | _BV(UCSZ0) | _BV(UCSZ1);
}
