/** Simplest UART implementation. Very lossy.

This implementation will sometimes lose transferred data.
It serves only as an example of simplest implementation.

References:
1. http://maxembedded.com/2013/09/the-usart-of-the-avr/

Author: Zalewa <zalewapl@gmail.com>
2016
*/
#include <stdint.h>
#include <stdlib.h>

#include <avr/io.h>

const uint16_t BAUDRATE = (uint16_t)(F_CPU / (BAUD * 16UL) - 1);

void uartInit()
{
	UBRRH = BAUDRATE >> 8;
	UBRRL = BAUDRATE;
	UCSRB |= _BV(TXEN) | _BV(RXEN);
	UCSRC |= _BV(URSEL) | _BV(UCSZ0) | _BV(UCSZ1);
}

size_t uartRead(uint8_t *buffer, size_t len)
{
	size_t amount = 0;
	while ((UCSRA & _BV(RXC)) && amount < len)
	{
		buffer[amount++] = UDR;
	}
	return amount;
}

size_t uartWrite(const uint8_t *buffer, size_t len)
{
	size_t amount = 0;
	while ((UCSRA & _BV(UDRE)) && amount < len)
	{
		UDR = buffer[amount++];
	}
	return amount;
}

void uartWriteFully(const uint8_t *buffer, size_t len)
{
	while (len > 0)
	{
		size_t written = uartWrite(buffer, len);
		buffer += written;
		len -= written;
	}
}

int main()
{
	const size_t bufLen = 64;
	uint8_t buffer[bufLen];

	uartInit();

	while (1)
	{
		size_t read = uartRead(buffer, bufLen);
		if (read > 0)
		{
			for (size_t idx = 0; idx < read; ++idx)
			{
				uint8_t packet[] = { '[', buffer[idx], ']' };
				uartWriteFully(packet, 3);
			}
		}
	}

	return 0;
}
