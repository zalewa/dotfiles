#include "mainwindow.h"

#include <QHostAddress>
#include <QPushButton>
#include <QUdpSocket>

static const int NUM_ROWS = 8;
static const int NUM_COLS = 8;

MainWindow::MainWindow()
{
	this->socket = new QUdpSocket(this);

	setupUi(this);
	addButtons();
	adjustSize();
}

void MainWindow::addButtons()
{
	for (int row = 0; row < NUM_ROWS; ++row)
	{
		for (int col = 0; col < NUM_COLS; ++col)
		{
			QPushButton *button = new QPushButton(this);
			button->setCheckable(true);
			button->setStyleSheet(
				"QPushButton:checked { background-color: #00ff00; border: none; } "
				"QPushButton { background-color: #ff0000; }");
			button->setProperty("matrixrow", row);
			button->setProperty("matrixcol", col);
			grid->addWidget(button, row, col);
		}
	}
}

QVector<uint8_t> MainWindow::matrix() const
{
	QVector<uint8_t> m;
	m.resize(NUM_ROWS);
	QList<QPushButton*> buttons = findChildren<QPushButton*>();
	foreach (const QPushButton *button, buttons)
	{
		if (button->property("matrixrow").isValid() && button->property("matrixcol").isValid())
		{
			int col = button->property("matrixcol").toInt();
			int row = button->property("matrixrow").toInt();
			if (button->isChecked())
			{
				m[row] |= 1 << (7 - col);
			}
			else
			{
				m[row] &= ~(1 << (7 - col));
			}
		}
	}
	return m;
}

void MainWindow::send()
{
	QVector<uint8_t> m = matrix();
	QString cmd = "MAP ";
	foreach (auto row, m)
	{
		cmd += QString("%1 ").arg(row);
	}
	cmd += "\n";

	QStringList addressTokens = addressInput->text().split(":");
	QString address = addressTokens[0];
	int port = addressTokens[1].toInt();
	if (port == 0)
	{
		port = 7050;
	}

	socket->writeDatagram(cmd.toUtf8(), QHostAddress(address), port);
}
