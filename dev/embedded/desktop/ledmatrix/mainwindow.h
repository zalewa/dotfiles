#pragma once

#include <QMainWindow>
#include <QUdpSocket>
#include <QVector>
#include <cstdint>

#include "ui_mainwindow.h"

class MainWindow : public QMainWindow, private Ui::MainWindow
{
	Q_OBJECT

public:
	MainWindow();

private:
	QUdpSocket *socket;

	void addButtons();
	QVector<uint8_t> matrix() const;

private slots:
	void send();
};
