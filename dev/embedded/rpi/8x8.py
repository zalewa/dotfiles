#!/usr/bin/python
import datetime
import serial
import sys
import time
import traceback
from conway import Conway


ERROR_LOG_INTERVAL = 5.0
cmd_count = 0


class TimeoutError(Exception):
    pass


class Digit(object):
    _DIGITS = None

    def __init__(self, bits):
        self._bits = bits

    def shifted(self, shift):
        d = Digit(list(self._bits))
        for i, row in enumerate(self._bits):
            d._bits[i] = row << (4 * shift)
        return d

    def pack(self):
        return list(self._bits)

    @classmethod
    def digit(cls, name):
        if cls._DIGITS is None:
            cls._DIGITS = cls._build_digits()
        return cls._DIGITS[name]

    @classmethod
    def _build_digits(cls):
        return {
            "0": cls([
                0b111,
                0b101,
                0b101,
                0b101,
                0b101,
                0b101,
                0b101,
                0b111,
            ]),
            "1": cls([
                0b0001,
                0b0011,
                0b0101,
                0b0001,
                0b0001,
                0b0001,
                0b0001,
                0b0001,
            ]),
            "2": cls([
                0b0110,
                0b1001,
                0b0001,
                0b0010,
                0b0100,
                0b1000,
                0b1000,
                0b1111
            ]),
            "3": cls([
                0b0110,
                0b1001,
                0b0001,
                0b0010,
                0b0001,
                0b0001,
                0b1001,
                0b0110,
            ]),
            "4": cls([
                0b0001,
                0b0011,
                0b0101,
                0b1111,
                0b0001,
                0b0001,
                0b0001,
                0b0001
            ]),
            "5": cls([
                0b1111,
                0b1000,
                0b1000,
                0b1110,
                0b0001,
                0b0001,
                0b0001,
                0b1110
            ]),
            "6": cls([
                0b0111,
                0b1000,
                0b1000,
                0b1110,
                0b1001,
                0b1001,
                0b1001,
                0b0110
            ]),
            "7": cls([
                0b1111,
                0b0001,
                0b0011,
                0b0010,
                0b0110,
                0b0100,
                0b1100,
                0b1000
            ]),
            "8": cls([
                0b0110,
                0b1001,
                0b1001,
                0b0110,
                0b1001,
                0b1001,
                0b1001,
                0b0110
            ]),
            "9": cls([
                0b0110,
                0b1001,
                0b1001,
                0b1001,
                0b0111,
                0b0001,
                0b0001,
                0b1110
            ])
        }


class Number(object):
    def __init__(self, number):
        self.number = number

    def pack(self):
        number = str(self.number)
        packed = []
        for ordinal, digit in enumerate(reversed(number)):
            digit_bits = Digit.digit(digit).shifted(ordinal).pack()
            if ordinal == 0:
                packed = digit_bits
            else:
                for row, rowbits in enumerate(digit_bits):
                    packed[row] |= rowbits
        return packed


def block():
    pins = [0] * 8
    for i in range(8):
        for j in range(i+1):
            for k in range(i+1):
                pins[j] |= 1 << k
        mapins(pins)
        time.sleep(0.1)
    time.sleep(1)


def full_block():
    pins = [255] * 8
    mapins(pins)
    time.sleep(5)


def swap_4_horz():
    for step in range(8):
        pins = [0] * 8
        pins[step] = 0xf0
        pins[-(step + 1)] = 0x0f
        mapins(pins)
        time.sleep(0.8)


def swap_4_vert():
    for step in range(8):
        pins = [0] * 8
        for row in range(len(pins)):
            if row < 4:
                pins[row] = 1 << (7 - step)
            else:
                pins[row] = 1 << step
        mapins(pins)
        time.sleep(0.4)


def swap_4():
    swap_4_vert()
    swap_4_horz()


def number(num, sleep=1.0):
    mapins(Number(num).pack())
    time.sleep(sleep)


def numbers():
    for i in range(100):
        number(i, sleep=0.3)


def walk(interval):
    for y in range(8):
        for x in range(8):
            bits = [0] * 8
            bits[y] |= 1 << x
            mapins(bits)
            time.sleep(interval)


def conway_game(init, generations, ticktime=0.016):
    game = Conway(size=(8, 8))
    init(game)
    for gen in range(generations):
        draw_conway_game(game)
        if game.is_dead():
            time.sleep(1.0)
            break
        game.tick()
        time.sleep(ticktime)


def conway_oscillator(game):
    game.reset()
    game.place_blinker(1, 1)
    game.place_toad(5, 5)


def draw_conway_game(game):
    bits = [0] * game.height
    for y in range(game.height):
        for x in range(game.width):
            field = 1 if game[x][y] else 0
            bits[y] |= field << x
    mapins(bits)


def mapins(pins):
    execcmd("MAP {0}".format(" ".join([str(n) for n in pins])))


def real_cmd_count():
    global cmd_count
    d = datetime.datetime.now()
    if (d.hour >= 23 and d.minute >= 30) or (d.hour < 7):
        return 0
    else:
        cmd_count += 1
        return cmd_count


def execcmd(cmd):
    ser.write("{}\n".format(cmd))
    ser.write("ROW {}\n".format(~(real_cmd_count()) & 0xff))
    response = ser.read(3)
    if response != "OK\n":
        raise TimeoutError("bad answer '{}'".format(response))


def cycle_day():
    conway_game(init=lambda game: game.randomize(), generations=300)
    conway_game(init=conway_oscillator, generations=50, ticktime=0.060)
    block()
    full_block()
    numbers()
    swap_4()


def cycle_evening():
    walk(1.0)


def cycle_night():
    mapins([0] * 8)
    time.sleep(60.0)


def cycle():
    now = datetime.datetime.now()
    if 7 <= now.hour < 23:
        cycle_day()
    elif now.hour == 23:
        cycle_evening()
    else:
        cycle_night()


last_error = 0
print >>sys.stderr, "{} starting 8x8".format(datetime.datetime.now())
while True:
    print >>sys.stderr, "open"
    try:
        ser = serial.Serial("/dev/ttyAMA0", timeout=1.0)
        while True:
            cycle()
    except TimeoutError:
        if time.time() - last_error > ERROR_LOG_INTERVAL:
            print >>sys.stderr, (
                "{} timeout occurred, resetting connection".format(
                    datetime.datetime.now()))
            traceback.print_exc()
        last_error = time.time()
        ser.close()
