#!/usr/bin/python
import serial

matrix = [
    "01110000",
    "01010000",
    "01010000",
    "01010000",
    "01110000",
    "01000000",
    "01100000",
    "01110000",
]

def apply_by_pixel():
    s = _serial()
    for column in range(len(matrix)):
        for cell in range(len(matrix[column])):
            s.write("XY {0} {1} {2}\n".format(column, cell, matrix[column][cell]))
            s.flush()


def apply_by_map():
    cmd = "MAP "
    for column in matrix:
        number = 0
        for bit in range(len(column)):
            number |= int(column[bit]) << len(column) - bit - 1
        cmd += "{} ".format(number)
    cmd += "\n"
    print len(cmd), cmd
    s = _serial()
    s.write(cmd)


def _serial():
    return serial.Serial("/dev/ttyAMA0", baudrate=9600)


apply_by_map()
