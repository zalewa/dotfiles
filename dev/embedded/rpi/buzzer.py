#!/usr/bin/python
import RPi.GPIO as GPIO
import sys
import time

maxtime =  0.001
mintime =  0.00001
steptime = 0.000001

def sleep_high(sleep, mintime=0.0000001, maxtime=0.001, steptime=0.000002):
    if steptime is None:
        steptime = mintime
    sleep -= mintime
    if sleep < mintime:
        sleep = maxtime
    return sleep

pins = [int(x) for x in sys.argv[1].split(",")]
state = True
sleep = maxtime

GPIO.setmode(GPIO.BCM)
try:
    for pin in pins:
        GPIO.setup(pin, GPIO.OUT)
    while True:
        for pin in pins:
            GPIO.output(pin, GPIO.HIGH if state else GPIO.LOW)
        if state:
            sleep = sleep_high(sleep, mintime, maxtime, steptime)
            time.sleep(sleep)
        state = not state
        time.sleep(sleep)
except KeyboardInterrupt:
    pass
finally:
    GPIO.cleanup()
