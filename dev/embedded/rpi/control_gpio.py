#!/usr/bin/python
import RPi.GPIO as GPIO
import sys
import time
import traceback

def control_pin():
    print "Pin, state: ",
    try:
        pin, state = [int(x) for x in sys.stdin.readline().strip().split(",")]
    except Exception as e:
        print e
    else:
        gpio_state = GPIO.HIGH if state else GPIO.LOW
        print "Setting pin {0} state to {1}".format(pin, gpio_state)
        if pin not in setup_pins:
            GPIO.setup(pin, GPIO.OUT)
            setup_pins.add(pin)
        GPIO.output(pin, gpio_state)


setup_pins = set()

mode = GPIO.BCM
if "board" in sys.argv[1:]:
    mode = GPIO.BOARD

GPIO.setmode(mode)
try:
    while True:
        try:
            control_pin()
        except Exception:
            traceback.print_exc()
except KeyboardInterrupt:
    pass
finally:
    GPIO.cleanup()
