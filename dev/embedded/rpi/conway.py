import copy
import random


class Conway(object):
    def __init__(self, size):
        self._board = _Board(size)
        self._debug = False
        self.randomize = self._board.randomize

    def reset(self):
        self._board.reset()

    def tick(self):
        snapshot = copy.deepcopy(self._board)
        for x in range(self.width):
            for y in range(self.height):
                alive_neighbours = snapshot.alive_neighbours(x, y)
                alive = snapshot.field(x, y)
                if alive and not (2 <= alive_neighbours <= 3):
                    alive = False
                elif not alive and alive_neighbours == 3:
                    alive = True
                self._board.set_field(x, y, alive)

    def place_blinker(self, x, y):
        proxy = _Proxy(self._board, (x - 2, y - 2))
        proxy.clear_subarea((5, 5))
        for x_ in range(1, 4):
            proxy.set_field(x_, 2, True)

    def place_toad(self, x, y):
        proxy = _Proxy(self._board, (x - 3, y - 3))
        proxy.clear_subarea((6, 6))
        proxy.set_field(1, 2, True)
        proxy.set_field(2, 2, True)
        proxy.set_field(3, 2, True)
        proxy.set_field(2, 3, True)
        proxy.set_field(3, 3, True)
        proxy.set_field(4, 3, True)

    def is_dead(self):
        return not any(self._board.fields)

    def __getitem__(self, x):
        if 0 <= x < self._board.size[0]:
            return _Navigator(self._board, x)
        else:
            raise IndexError('x ({}) out of range'.format(x))

    @property
    def size(self):
        return self._board.size

    @property
    def width(self):
        return self._board.width

    @property
    def height(self):
        return self._board.height


class _Board(object):
    def __init__(self, size):
        self.fields = []
        self.size = size
        self.wrapping = True
        self.reset()

    def randomize(self):
        self.reset()
        for field in range(len(self.fields)):
            self.fields[field] = bool(random.randint(0, 1))

    def reset(self):
        self.fields = [False] * (self.size[0] * self.size[1])

    def alive_neighbours(self, x, y):
        alive_neighbours = 0
        for x_neighbour in range(x - 1, x + 2):
            for y_neighbour in range(y - 1, y + 2):
                if x == x_neighbour and y == y_neighbour:
                    # Skip self
                    continue
                if self.wrapping:
                    x_neighbour %= self.width
                    y_neighbour %= self.height
                if 0 <= x_neighbour < self.width and \
                   0 <= y_neighbour < self.height:
                    if self.field(x_neighbour, y_neighbour):
                        alive_neighbours += 1
        return alive_neighbours

    def field(self, x, y):
        if self.wrapping:
            x %= self.width
            y %= self.height
        return self.fields[y * self.width + x]

    def set_field(self, x, y, value):
        if self.wrapping:
            x %= self.width
            y %= self.height
        self.fields[y * self.width + x] = value

    def clear_area(self, start, size):
        for x in range(start[0], start[0] + size[0]):
            for y in range(start[1], start[1] + size[1]):
                self.set_field(x, y, False)

    @property
    def width(self):
        return self.size[0]

    @property
    def height(self):
        return self.size[1]

    def __str__(self):
        rows = []
        for y in range(self.height):
            row_offset = y * self.width
            row = map(lambda alive: "1" if alive else "0",
                      self.fields[row_offset:row_offset + self.width])
            rows.append(",".join(row))
        return "\n".join(rows)


class _Proxy(object):
    def __init__(self, board, offset):
        self._board = board
        self._offset = offset

    def set_field(self, x, y, value):
        xo, yo = self._offset
        self._board.set_field(xo + x, yo + y, value)

    def field(self, x, y):
        xo, yo = self._offset
        self._board.field(xo + x, yo + y)

    def clear_subarea(self, size, start=(0, 0)):
        xo, yo = self._offset
        x, y = start
        self._board.clear_area((xo + x, yo + y), size)


class _Navigator(object):
    def __init__(self, board, x):
        self._board = board
        self._x = x

    def __getitem__(self, y):
        self._validate(y)
        return self._board.field(self._x, y)

    def __setitem__(self, y, value):
        self._validate(y)
        self._board.set_field(self._x, y, value)

    def _validate(self, y):
        if not (0 <= y < self._board.size[1]):
            raise IndexError('y ({}) out of range'.format(y))
