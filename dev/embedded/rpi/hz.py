#!/usr/bin/python
import RPi.GPIO as GPIO
import time

pin = 4
clicks = 0
start_time = time.time()
stamps = []
measure_interval = .5
max_stamps = 10
a = time.time()
b = time.time()


def handler(arg):
    global clicks
    global a
    global b
    state = GPIO.input(arg)
    clicks += 1
    a = b
    b = time.time()


def frequency():
    herzes = map(lambda x: x[1] / x[0], stamps)
    return sum(herzes) / len(herzes)


def quick_hz():
    try:
        return 1. / (b - a)
    except ZeroDivisionError:
        return -1


def measure():
    global start_time
    global stamps
    global clicks
    stamps.append((time.time() - start_time, clicks))
    start_time = time.time()
    clicks = 0
    if len(stamps) > max_stamps:
        stamps = stamps[len(stamps) - max_stamps:]
    print "{0:.2f} Hz, measures {1}, b - a {2:.3f}, 1/(b-a) {3:.2f}".format(
        frequency(), len(stamps), b - a, quick_hz())


GPIO.setmode(GPIO.BCM)
try:
    GPIO.setup(pin, GPIO.IN, GPIO.PUD_OFF)
    GPIO.add_event_detect(pin, GPIO.FALLING, handler, 1)
    while True:
        measure()
        time.sleep(measure_interval)
finally:
    GPIO.cleanup()
