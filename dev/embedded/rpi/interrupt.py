#!/usr/bin/python
import RPi.GPIO as GPIO
import time

times = 0
pin = 4
pin_out = 12
erreurs = 0
prev_state = None


def handler(arg):
    state = GPIO.input(arg)
    global times
    global erreurs
    global prev_state
    if state == prev_state:
        erreurs += 1
    prev_state = state
    times += 1
    GPIO.output(pin_out, state)
    print "handled", arg, times, state, "erreurs: {0}".format(erreurs)


GPIO.setmode(GPIO.BCM)
try:
    GPIO.setup(pin, GPIO.IN, GPIO.PUD_OFF)
    GPIO.setup(pin_out, GPIO.OUT)
    GPIO.add_event_detect(pin, GPIO.BOTH, handler, 1)

    while True:
        time.sleep(1.000)
finally:
    GPIO.cleanup()
