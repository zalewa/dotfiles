#!/usr/bin/python
import RPi.GPIO as GPIO
import sys
import time

pin = int(sys.argv[1])
state = True
sleep_high = 1.0
sleep_low = 1.0
try:
    state = bool(int(sys.argv[2]))
    sleep_high = float(sys.argv[3])
    sleep_low = float(sys.argv[4])
except IndexError, ValueError:
    pass

GPIO.setmode(GPIO.BCM)
try:
    GPIO.setup(pin, GPIO.OUT)
    while True:
        #print "Setting state to {0}".format(state)
        GPIO.output(pin, GPIO.HIGH if state else GPIO.LOW)
        #state = int(sys.stdin.readline().strip())
        if state:
            sleep = sleep_high
        else:
            sleep = sleep_low
        state = not state
        time.sleep(sleep)
except KeyboardInterrupt:
    pass
finally:
    GPIO.cleanup()
