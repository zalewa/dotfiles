#!/usr/bin/python
from datetime import datetime
import RPi.GPIO as GPIO
import time
import random

shift_clock = 12
store_clock = 16
output_disabled = 20
serial_input = 21


def tick_pin(pin, pause=0):
    GPIO.output(pin, GPIO.HIGH)
    time.sleep(pause)
    GPIO.output(pin, GPIO.LOW)


def is_bit(number, bit):
    return ((1 << bit) & number) != 0


def set_number(number):
    for bit in range(7, -1, -1):
        GPIO.output(serial_input, GPIO.HIGH if is_bit(number, bit) else GPIO.LOW)
        tick_pin(shift_clock)
    tick_pin(store_clock)


def set_random():
    set_number(random.randint(0, 255))


def random_batch():
    interval = random.uniform(0.01, 0.2)
    for _ in range(random.randint(10, 20)):
        set_random()
        time.sleep(interval)


def dance():
    interval = random.uniform(0.01, 0.2)
    for _ in range(random.randint(5, 8)):
        set_number(0b10101010)
        time.sleep(interval)
        set_number(0b01010101)
        time.sleep(interval)


def count_up():
    interval = random.uniform(0.001, 0.05)
    for number in range(256):
        set_number(number)
        time.sleep(interval)


def walk_bit(interval=None, reverse=False):
    if interval is None:
        interval = random.uniform(0.01, 0.125)
    for bit in range(8):
        if reverse:
            bit = 8 - (bit + 1)
        set_number(1 << bit)
        time.sleep(interval)


def walk_some_bits():
    for interval in [0.01, 0.01, 0.01, 0.025, 0.05, 0.075]:
        walk_bit(interval)


def cycle_day():
    funs = [random_batch, dance, count_up, walk_some_bits, walk_bit]
    for fun in funs:
        fun()


def cycle_evening():
    walk_bit(1.0)
    walk_bit(1.0, reverse=True)


def cycle_night():
    set_number(0)
    time.sleep(60.0)


def cycle():
    d = datetime.now()
    if 7 <= d.hour < 23:
        cycle_day()
    elif d.hour == 23:
        cycle_evening()
    else:
        cycle_night()


GPIO.setmode(GPIO.BCM)
try:
    GPIO.setup([shift_clock, store_clock,
                #output_disabled,
                serial_input], GPIO.OUT)
    #GPIO.output(output_disabled, 0)
    while True:
        cycle()
except KeyboardInterrupt:
    pass
finally:
    GPIO.cleanup()