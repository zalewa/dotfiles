#!/usr/bin/python
import RPi.GPIO as GPIO
import time


prev_state = 0
pin = 21
collected = []
last_show = time.time()
show_interval = 2.0


def handler(pin):
    global prev_state
    state = GPIO.input(pin)
    collected.append(time.time())
    prev_state = state


def show_rpm():
    current = time.time()
    for num, pulse in enumerate(reversed(collected)):
        if current - pulse > 2.0:
            print "{} RPM".format((num * 30) / 2)
            return
    print "Not enough data"


def show_rpm_heartbeat():
    global last_show
    if time.time() - last_show > show_interval:
        show_rpm()
        last_show = time.time()


GPIO.setmode(GPIO.BCM)
try:
    GPIO.setup(pin, GPIO.IN, GPIO.PUD_OFF)
    #GPIO.add_event_detect(pin, GPIO.FALLING, lambda arg: arg)
    prev_state = 0
    while True:
        state = GPIO.input(pin)
        if state == 0 and prev_state == 1:
            collected.append(time.time())
        prev_state = state
        show_rpm_heartbeat()
finally:
    GPIO.cleanup()