#!/usr/bin/python
import time
import RPi.GPIO as GPIO

pin = 26
dutycycle = 50.0
frequency = 25000.0
delta = 0.0

class Walk(object):
    def __init__(self, value, clamps, stepsize):
        self.value = value
        self.clamps = clamps
        self.stepsize = stepsize
        self._forward = True

    def step(self):
        self.value += self.stepsize * (1 if self._forward else -1)
        if self.value < self._min:
            self.value = self._min
            self._forward = True
        if self.value > self._max:
            self.value = self._max
            self._forward = False
        return self.value

    @property
    def _min(self):
        return self.clamps[0]

    @property
    def _max(self):
        return self.clamps[1]


GPIO.setmode(GPIO.BCM)
try:
    GPIO.setup(pin, GPIO.OUT)
    pwm = GPIO.PWM(pin, frequency)
    pwm.start(dutycycle)
    walk = Walk(dutycycle, (0.0, 100.0), delta)
    while True:
        dutycycle = walk.step()
        if dutycycle < 1.0:
            dutycycle = 1.0
        if dutycycle > 100.0:
            dutycycle = 100.0
        pwm.ChangeDutyCycle(dutycycle)
        time.sleep(0.01)
finally:
    GPIO.cleanup()


