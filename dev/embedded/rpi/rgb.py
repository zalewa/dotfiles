#!/usr/bin/env python2
import RPi.GPIO as GPIO
import random
import time
from datetime import datetime

rgb_pins = (13, 19, 26)
rgb_times = [(0, 0)] * 3
freq = 60
period = 1.0 / freq
resolution = 0.001
duty_ratios = (0.5, 0.3, 0.7)

hsv = [0, 1.0, 0.5]
hsv_period = 0.1
hsv_period_change_period = 2.0
hsv_periods = [(0.1, 60.0), (0.05, 60.0), (0.01, 60.0),
               (0.5, 240.0), (0.001, 1.0), (0.001, 20.0)]

brightness = 1.0

next_hsv = 0
next_hsv_period = 0

def hsv_to_rgb(hsv):
    '''
    h - 0-360
    s - 0-1
    v - 0-1
    '''
    h, s, v = hsv
    c = v * s
    x = c * (1 - abs(((h / 60.0) % 2.0) - 1))
    m = v - c
    if 0 <= h < 60:
        ry, gy, by = (c, x, 0)
    elif 60 <= h < 120:
        ry, gy, by = (x, c, 0)
    elif 120 <= h < 180:
        ry, gy, by = (0, c, x)
    elif 180 <= h < 240:
        ry, gy, by = (0, x, c)
    elif 240 <= h < 300:
        ry, gy, by = (x, 0, c)
    elif 300 <= h < 360:
        ry, gy, by = (c, 0, x)
    return (ry + m, gy + m, by + m)


def swap(current_state, duty_ratio):
    if duty_ratio <= 0.03:
        return time.time() + period, 0
    if duty_ratio >= 0.97:
        return time.time() + period, 1
    if current_state:
        secs = period * (1.0 - duty_ratio)
    else:
        secs = period * duty_ratio
    return (time.time() + secs, 0 if current_state else 1)


GPIO.setmode(GPIO.BCM)

try:
    for pin in rgb_pins:
        GPIO.setup(pin, GPIO.OUT)
    while True:
        for pin_idx, pin in enumerate(rgb_pins):
            until, state = rgb_times[pin_idx]
            if time.time() >= until:
                next_until, next_state = swap(state, duty_ratios[pin_idx])
                GPIO.output(pin, next_state)
                rgb_times[pin_idx] = (next_until, next_state)
        time.sleep(resolution)
        if time.time() > next_hsv:
            next_hsv = time.time() + hsv_period
            hsv[0] = (hsv[0] + 1) % 360
            now_time = datetime.now()
            if now_time.hour >= 23 or now_time.hour <= 7:
                hsv[2] = 0  # value
            else:
                hsv[2] = 1.0
            duty_ratios = [c * brightness for c in hsv_to_rgb(hsv)]
        if time.time() > next_hsv_period:
            hsv_period, hsv_period_change_period = random.choice(hsv_periods)
            next_hsv_period = time.time() + hsv_period_change_period
except KeyboardInterrupt:
    pass
finally:
    for pin in rgb_pins:
        GPIO.output(pin, 0)
