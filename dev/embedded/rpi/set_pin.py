#!/usr/bin/python
import RPi.GPIO as GPIO
import sys
import time

pin = int(sys.argv[1])
state = int(sys.argv[2])

GPIO.setmode(GPIO.BCM)
try:
    GPIO.setup(pin, GPIO.OUT)
    while True:
        print "Setting state to {0}".format(state)
        GPIO.output(pin, GPIO.HIGH if state else GPIO.LOW)
        print "Next state: ",
        state = int(sys.stdin.readline().strip())
except KeyboardInterrupt:
    pass
finally:
    GPIO.cleanup()
