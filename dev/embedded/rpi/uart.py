#!/usr/bin/python
import serial
import sys
import time


s = serial.Serial(port="/dev/ttyAMA0", timeout=2.2)
x = 0

msg = "wiadomosc"
msg = "Lorem ipsum dolor sit amet. " * 3
#msg = "o"
once = False
expected_response_len = len(msg)

try:
    while True:
        print "x", x
        x += 1
        s.write(msg)
        s.flush()
        time.sleep(0.301)
        buffer = ""
        while True and len(buffer) < expected_response_len:
            chunk = s.read(1)
            sys.stdout.write(chunk)
            if not chunk:
                break
            buffer += chunk
        print ""
        if once:
            break
finally:
    s.close()
