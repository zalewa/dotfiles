#!/usr/bin/python
import serial
import sys

while True:
    s = serial.Serial("/dev/ttyAMA0", baudrate=9600, timeout=1.0)
    print "> ",
    cmd = sys.stdin.readline()
    s.write(cmd + "\n")
    s.flush()
