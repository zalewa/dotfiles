#!/usr/bin/python
import serial
import socket

ser = serial.Serial("/dev/ttyAMA0", baudrate=9600)
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
sock.bind(("0.0.0.0", 7050))

while True:
    buf, addr = sock.recvfrom(1024)
    print "Received", buf, addr
    if buf:
        ser.write(buf)
