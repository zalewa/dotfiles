extends Panel

const PLAYER1 = 'x'
const PLAYER2 = 'o'
const DRAW = "DRAW"
const NO_WINNER = null

var currentplayer = PLAYER1
var fields = []
var score = {x = 0, o = 0}
var timer = Timer.new()

func _ready():
	_setup_new_game_timer()
	_setup_fields()
	_new_game()

func _setup_new_game_timer():
	timer.set_one_shot(true)
	timer.connect("timeout", self, "_new_game")
	add_child(timer)

func _setup_fields():
	for i in range(9):
		var node = get_node("board/field" + str(i + 1))
		fields.append(node)
		node.connect("pressed", self, "_field_pressed", [i])


func _new_game():
	for field in fields:
		field.set_disabled(false)
		var tick = field.get_node("tick")
		if tick != null:
			tick.queue_free()
	get_node("scoreboard").display_score(score)
	_show_error("")
	_show_message("")
	get_tree().set_pause(false)

func _field_pressed(field_index):
	var field = fields[field_index]
	if field.get_node("tick") != null:
		_show_error("Field already taken.")
		return
	_show_error("")
	_tick_field(field)
	_swap_player()
	_check_win_conditions()

func _tick_field(field):
	field.set_disabled(true)
	var tick = Sprite.new()
	tick.set_name("tick")
	tick.set_texture(_get_current_player_texture())
	var field_size = field.get_item_rect()
	var tick_pos = Vector2(field_size.size.width / 2, field_size.size.height / 2)
	tick.set_pos(tick_pos)
	tick.set_meta("player", currentplayer)
	field.add_child(tick)

func _swap_player():
	if currentplayer == PLAYER1:
		currentplayer = PLAYER2
	else:
		currentplayer = PLAYER1
	get_node("currentplayer").set_texture(_get_current_player_texture())

func _check_win_conditions():
	var winner = _who_won()
	if winner != NO_WINNER:
		if winner == DRAW:
			_show_message("DRAW!")
		else:
			_show_message(winner + " wins!")
			score[winner] += 1
		get_tree().set_pause(true)
		timer.set_wait_time(1.0)
		timer.start()

func _who_won():
	var all_combinations = _get_all_combinations()
	var has_empty = false
	for combination in all_combinations:
		var combination_winner = _who_won_combination(combination)
		if combination_winner == NO_WINNER:
			has_empty = true
		elif combination_winner != DRAW:
			return combination_winner
	if has_empty:
		return NO_WINNER
	else:
		return DRAW

func _who_won_combination(combination):
	var prevplayer = null
	var samecount = 0
	var has_empty = false
	for field in combination:
		var tick = field.get_node("tick")
		if tick == null:
			has_empty = true
			break
		var player = tick.get_meta("player")
		if player != prevplayer:
			prevplayer = player
			samecount = 0
		samecount += 1
	if has_empty:
		return NO_WINNER
	elif samecount == 3:
		return prevplayer
	else:
		return DRAW

func _get_all_combinations():
	var ROWS = 3
	var COLS = 3
	var row = 0
	var col = 0
	var all_combinations = []
	for row in range(ROWS):
		var combination = []
		for col in range(COLS):
			combination.append(fields[row * COLS + col])
		all_combinations.append(combination)
	for col in range(COLS):
		var combination = []
		for row in range(ROWS):
			combination.append(fields[row * COLS + col])
		all_combinations.append(combination)
	all_combinations.append([fields[0], fields[4], fields[8]])
	all_combinations.append([fields[2], fields[4], fields[6]])
	return all_combinations

func _get_current_player_texture():
	return load("res://" + currentplayer + ".png")

func _show_error(msg):
	_show_message(msg)

func _show_message(msg):
	get_node("messagelabel").set_text(msg)