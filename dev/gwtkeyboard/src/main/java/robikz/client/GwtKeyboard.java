package robikz.client;

import java.util.ArrayList;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.dom.client.NativeEvent;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.Event.NativePreviewEvent;
import com.google.gwt.user.client.Event.NativePreviewHandler;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RootPanel;

public class GwtKeyboard implements EntryPoint {
	private final static int MAX_EVENTS = 40;
	private ArrayList<String> events = new ArrayList<String>();
	private Label label;

	public void onModuleLoad() {
		label = new Label();
		label.setText("awaiting input");
		RootPanel.get().add(label);

		Event.addNativePreviewHandler(new NativePreviewHandler() {
			@Override
			public void onPreviewNativeEvent(NativePreviewEvent event) {
				handle(event);
			}
		});
	}

	private void handle(NativePreviewEvent event) {
		if ((event.getTypeInt() & Event.KEYEVENTS) == 0) {
			return;
		}
//		if (event.getTypeInt() != Event.ONKEYDOWN) {
//			return;
//		}

		NativeEvent nevent = event.getNativeEvent();

		StringBuilder eventText = new StringBuilder();

		boolean alt = nevent.getAltKey();
		boolean ctrl = nevent.getCtrlKey();
		boolean shift = nevent.getShiftKey();
		boolean meta = nevent.getMetaKey();

		eventText.append(nevent.getType());
		while (eventText.length() < 10) {
			eventText.append(' ');
		}
		eventText.append("alt=").append(alt)
			.append("|ctrl=").append(ctrl)
			.append("|shift=").append(shift)
			.append("|meta=").append(meta);
		eventText.append(" keyCode=").append(nevent.getKeyCode());
		eventText.append(" charCode=").append(nevent.getCharCode());

		events.add(eventText.toString());

		displayEvents();

		event.consume();
		event.cancel();
		nevent.preventDefault();
		nevent.stopPropagation();
	}

	private void displayEvents() {
		while (events.size() > MAX_EVENTS) {
			events.remove(0);
		}
		rebuildLabel();
	}

	private void rebuildLabel() {
		label.getElement().setInnerHTML(collectEventsAsText());
	}

	private String collectEventsAsText() {
		StringBuilder text = new StringBuilder();
		text.append("<pre>");
		for (int i = events.size() - 1; i >= 0; --i) {
			String event = events.get(i);
			text.append(event);
			text.append("\n");
		}
		text.append("</pre>");
		return text.toString();
	}
}
