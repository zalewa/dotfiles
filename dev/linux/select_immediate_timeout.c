/*
  Linux C: a select(2) example with an immediate timeout

  This example shows how select(2) behaves if the read end of the FD is
  ready and the timeout is 0. select(2) with timeout 0 effectively behaves
  as a polling mechanism that can be used to check if any FD is ready
  right now, and if not then continue the program.
*/
#include <sys/select.h>
#include <unistd.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char *argv[])
{
	int rv;
	int pipefd[2];
	fd_set rfds;
	struct timeval tv;

	// Create a pipe so that we have something to select on.
	rv = pipe(pipefd);
	if (rv != 0) {
		perror("pipe()");
		exit(EXIT_FAILURE);
	}
	fprintf(stderr, "pipe: (%d, %d)\n", pipefd[0], pipefd[1]);

	// Write to the pipe so that select will immediately detect that the read
	// end is ready.
	rv = write(pipefd[1], "anything", strlen("anything") + 1);
	if (rv != strlen("anything") + 1) {
		fprintf(stderr, "failed to write to the pipe\n");
		exit(EXIT_FAILURE);
	}
	close(pipefd[1]);

	// Set the timeout to 0.
	tv.tv_sec = 0;
	tv.tv_usec = 0;

	// Select on the read end.
	FD_ZERO(&rfds);
	FD_SET(pipefd[0], &rfds);
	rv = select(pipefd[0] + 1, &rfds, NULL, NULL, &tv);

	// Show the results.
	if (rv == -1) {
		perror("select()");
		exit(EXIT_FAILURE);
	} else if (rv == 0) {
		printf("timed out: select()\n");
	} else {
		printf("select() rv = %d\n", rv);
	}
	char buf[4096];
	rv = read(pipefd[0], buf, 4096);
	printf("read from pipe [%d]: %s\n", rv, buf);

	return EXIT_SUCCESS;
}
