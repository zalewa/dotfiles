#include <sys/types.h>
#include <unistd.h>
#include <err.h>

#include <stdio.h>

int main(int argc, char *argv[]) {
	int nread;
	fprintf(stderr, "PID=%d PGRP=%d\n", getpid(), getpgrp());

	if (setpgid(0, 0) == 0) {
		fprintf(stderr, "entered process group\n");
		fprintf(stderr, "PID=%d PGRP=%d\n", getpid(), getpgrp());
	} else {
		err(1, "setpgid()");
	}

	while (1) {
		fprintf(stderr, "sleeping ...\n");
		sleep(1);
	}
	return 0;
}
