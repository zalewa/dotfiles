#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MODE_CLIENT 0
#define MODE_SERVER 1

int g_verbose = 0;

static void usage(char *pname) {
	fprintf(stderr, "Usage: %s [-hsv] <-T|-U> <socket>\n", pname);
}

static void help() {
	fprintf(stderr, "  -h - show help\n");
	fprintf(stderr, "  -s - server mode\n");
	fprintf(stderr, "  -T - SOCK_STREAM socket type\n");
	fprintf(stderr, "  -U - SOCK_DGRAM socket type\n");
	fprintf(stderr, "  -v - verbose\n");
}

static int server_dgram(const char *addr) {
#define BUFFER_SIZE 1460
	int rv = EXIT_SUCCESS;

	char buffer[BUFFER_SIZE];
	struct sockaddr_un saddr;
	int rc;
	int sockfd;

	if (g_verbose)
		fprintf(stderr, "D: server datagram mode\n");
	sockfd = socket(AF_UNIX, SOCK_DGRAM, 0);
	if (sockfd == -1) {
		perror("socket() failed");
		return EXIT_FAILURE;
	}

	if (g_verbose)
		fprintf(stderr, "D: sockfd = %d\n", sockfd);

	saddr.sun_family = AF_UNIX;
	strncpy(saddr.sun_path, addr, sizeof(saddr.sun_path) - 1);

	rc = bind(sockfd, (struct sockaddr *) &saddr, sizeof(saddr));
	if (rc != 0) {
		perror("bind() failed");
		goto fail;
	}

	for (;;) {
		struct sockaddr_un caddr;
		int caddr_size = sizeof(caddr);
		int incoming;

		incoming = recvfrom(sockfd, buffer, BUFFER_SIZE, 0,
			(struct sockaddr *) &caddr, &caddr_size);
		if (incoming > 0) {
			if (g_verbose)
				fprintf(stderr, "D: caddr.sun_path = %s, incoming = %d\n", caddr.sun_path, incoming);
			fwrite(buffer, 1, incoming, stdout);
			fflush(stdout);
		}
	}
	goto end;
fail:
	rv = EXIT_FAILURE;
end:
	if (g_verbose)
		fprintf(stderr, "D: sockfd = %d, closing\n", sockfd);
	close(sockfd);
	return rv;
#undef BUFFER_SIZE
}

static int server_stream(const char *addr) {
#define BUFFER_SIZE 1460
	int rv = EXIT_SUCCESS;

	char buffer[BUFFER_SIZE];
	struct sockaddr_un saddr;
	int rc;
	int sockfd;

	if (g_verbose)
		fprintf(stderr, "D: server stream mode\n");
	sockfd = socket(AF_UNIX, SOCK_STREAM, 0);
	if (sockfd == -1) {
		perror("socket() failed");
		return EXIT_FAILURE;
	}

	if (g_verbose)
		fprintf(stderr, "D: sockfd = %d\n", sockfd);

	saddr.sun_family = AF_UNIX;
	strncpy(saddr.sun_path, addr, sizeof(saddr.sun_path) - 1);

	rc = bind(sockfd, (struct sockaddr *) &saddr, sizeof(saddr));
	if (rc != 0) {
		perror("bind() failed");
		goto fail;
	}

	rc = listen(sockfd, 0);
	if (rc != 0) {
		perror("listen() failed");
		goto fail;
	}

	for (;;) {
		struct sockaddr_un caddr;
		int caddr_size = sizeof(caddr);
		int clientfd;
		int incoming;

		if (g_verbose)
			fprintf(stderr, "D: accepting ...\n");

		clientfd = accept(sockfd, (struct sockaddr *) &caddr, &caddr_size);
		if (clientfd == -1) {
			perror("accept() failed");
			break;
		}

		if (g_verbose)
			fprintf(stderr, "D: accepted, clientfd = %d, addr.sun_path = %s\n", clientfd, caddr.sun_path);

		do {
			incoming = recv(clientfd, buffer, BUFFER_SIZE, 0);
			if (g_verbose)
				fprintf(stderr, "D: clientfd = %d, incoming = %d\n", clientfd, incoming);
			if (incoming > 0) {
				fwrite(buffer, 1, incoming, stdout);
				fflush(stdout);
			}
		} while (incoming == BUFFER_SIZE);
		if (incoming > 0) {
			if (g_verbose)
				fprintf(stderr, "D: clientfd = %d, sending OK\n", clientfd);
			send(clientfd, "OK\n", 4, 0);
		}

		if (g_verbose)
			fprintf(stderr, "D: clientfd = %d, closing\n", clientfd);
		close(clientfd);
	}
	goto end;
fail:
	rv = EXIT_FAILURE;
end:
	if (g_verbose)
		fprintf(stderr, "D: sockfd = %d, closing\n", sockfd);
	close(sockfd);
	return rv;
#undef BUFFER_SIZE
}

static int server(const char *addr, int socktype) {
	switch (socktype) {
	case SOCK_DGRAM:
		return server_dgram(addr);
	case SOCK_STREAM:
		return server_stream(addr);
	default:
		fprintf(stderr, "unhandled server socktype %d\n", socktype);
		return EXIT_FAILURE;
	}
}

static int client_dgram(const char *addr) {
#define BUFFER_SIZE 14600
	int rv = EXIT_SUCCESS;
	char buffer[BUFFER_SIZE];
	struct sockaddr_un saddr;
	int rc;
	int sockfd;

	if (g_verbose)
		fprintf(stderr, "D: client datagram mode\n");

	sockfd = socket(AF_UNIX, SOCK_DGRAM, 0);
	if (sockfd == -1) {
		perror("socket() failed");
		return EXIT_FAILURE;
	}

	if (g_verbose)
		fprintf(stderr, "D: sockfd = %d\n", sockfd);

	saddr.sun_family = AF_UNIX;
	strncpy(saddr.sun_path, addr, sizeof(saddr.sun_path) - 1);

	rc = connect(sockfd, (struct sockaddr *) &saddr, sizeof(saddr));
	if (rc != 0) {
		perror("connect() failed");
		goto fail;
	}

	if (g_verbose)
		fprintf(stderr, "D: sockfd = %d, connected to %s\n", sockfd, saddr.sun_path);

	for (;;) {
		int incoming, sent, replied;
		incoming = fread(buffer, 1, BUFFER_SIZE, stdin);
		if (incoming == 0) {
			if (g_verbose)
				fprintf(stderr, "D: input eof\n");
			break;
		}

		if (g_verbose)
			fprintf(stderr, "D: sending %d\n", incoming);
		sent = send(sockfd, buffer, incoming, 0);
		if (g_verbose)
			fprintf(stderr, "D: sent %d\n", sent);

		if (sent != incoming) {
			if (g_verbose)
				fprintf(stderr, "D: not all input sent\n");
			break;
		}
	}
	goto end;
fail:
	rv = EXIT_FAILURE;
end:
	if (g_verbose)
		fprintf(stderr, "D: sockfd = %d, closing\n", sockfd);
	close(sockfd);
	return rv;
#undef BUFFER_SIZE
}

static int client_stream(const char *addr) {
#define BUFFER_SIZE 14600
	int rv = EXIT_SUCCESS;
	char buffer[BUFFER_SIZE];
	struct sockaddr_un saddr;
	int rc;
	int sockfd;

	if (g_verbose)
		fprintf(stderr, "D: client stream mode\n");

	sockfd = socket(AF_UNIX, SOCK_STREAM, 0);
	if (sockfd == -1) {
		perror("socket() failed");
		return EXIT_FAILURE;
	}

	if (g_verbose)
		fprintf(stderr, "D: sockfd = %d\n", sockfd);

	saddr.sun_family = AF_UNIX;
	strncpy(saddr.sun_path, addr, sizeof(saddr.sun_path) - 1);

	rc = connect(sockfd, (struct sockaddr *) &saddr, sizeof(saddr));
	if (rc != 0) {
		perror("connect() failed");
		goto fail;
	}

	if (g_verbose)
		fprintf(stderr, "D: sockfd = %d, connected to %s\n", sockfd, saddr.sun_path);

	for (;;) {
		int incoming, sent, replied;
		incoming = fread(buffer, 1, BUFFER_SIZE, stdin);
		if (incoming == 0) {
			if (g_verbose)
				fprintf(stderr, "D: input eof\n");
			break;
		}

		if (g_verbose)
			fprintf(stderr, "D: sending %d\n", incoming);
		sent = send(sockfd, buffer, incoming, 0);
		if (g_verbose)
			fprintf(stderr, "D: sent %d\n", sent);

		replied = recv(sockfd, buffer, BUFFER_SIZE, 0);
		if (g_verbose)
			fprintf(stderr, "D: received %d\n", replied);
		if (replied > 0) {
			fwrite(buffer, 1, replied, stdout);
			fflush(stdout);
		}

		if (sent != incoming) {
			if (g_verbose)
				fprintf(stderr, "D: not all input sent\n");
			break;
		}
	}
	goto end;
fail:
	rv = EXIT_FAILURE;
end:
	if (g_verbose)
		fprintf(stderr, "D: sockfd = %d, closing\n", sockfd);
	close(sockfd);
	return rv;
#undef BUFFER_SIZE
}

static int client(const char *addr, int socktype) {
	switch (socktype) {
	case SOCK_DGRAM:
		return client_dgram(addr);
	case SOCK_STREAM:
		return client_stream(addr);
	default:
		fprintf(stderr, "unhandled client socktype %d\n", socktype);
		return EXIT_FAILURE;
	}
}

int main(int argc, char *argv[]) {
	const char *addr;
	int rv, opt;
	int mode = MODE_CLIENT;
	int socktype = 0;
	while ((opt = getopt(argc, argv, "hsTUv")) != -1) {
		switch (opt) {
		case 'h':
			usage(argv[0]);
			help();
			return EXIT_SUCCESS;
		case 's':
			mode = MODE_SERVER;
			break;
		case 'T':
			socktype = SOCK_STREAM;
			break;
		case 'U':
			socktype = SOCK_DGRAM;
			break;
		case 'v':
			++g_verbose;
			break;
		default:
			goto usage;
		}
	}

	if (optind >= argc) {
		fprintf(stderr, "%s: expected argument after options\n", argv[0]);
		goto usage;
	}

	if (socktype != SOCK_STREAM && socktype != SOCK_DGRAM) {
		fprintf(stderr, "%s: socket type was not specified\n", argv[0]);
		goto usage;
	}

	addr = argv[optind];

	switch (mode) {
	case MODE_CLIENT:
		rv = client(addr, socktype);
		break;
	case MODE_SERVER:
		rv = server(addr, socktype);
		break;
	default:
		fprintf(stderr, "%s: unknown mode %d\n", argv[0], mode);
		goto usage;
	}

	return rv;

usage:
	usage(argv[0]);
	return EXIT_FAILURE;
}
