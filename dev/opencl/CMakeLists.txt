# https://anteru.net/blog/2012/11/03/2009/index.html
# https://bitbucket.org/Anteru/opencltutorial
cmake_minimum_required(VERSION 3.0)
project(hello_opencl)

find_package(OpenCL REQUIRED)
include_directories(${OpenCL_INCLUDE_DIR})

add_executable(hello_opencl hello_opencl.cpp)
target_link_libraries(hello_opencl ${OpenCL_LIBRARY})

project(opencl_enum)
add_executable(opencl_enum opencl_enum.cpp)
target_link_libraries(opencl_enum ${OpenCL_LIBRARY})

project(pic)
add_executable(pic pic.cpp)
target_link_libraries(pic ${OpenCL_LIBRARY})
