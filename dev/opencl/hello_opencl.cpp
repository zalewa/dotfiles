#include <iostream>
#include <fstream>
#include <string>
#include <vector>

#ifdef __APPLE__
	#include "OpenCL/opencl.h"
#else
	#include "CL/cl.h"
#endif

int main(int argc, char **argv)
{
	std::cout << "hi opencl" << std::endl;

	// Enumerate platforms.
	cl_uint platformIdCount = 0;
	clGetPlatformIDs(0, nullptr, &platformIdCount);
	std::cout << "platformIdCount " << platformIdCount << std::endl;

	std::vector<cl_platform_id> platformIds(platformIdCount);
	clGetPlatformIDs(platformIdCount, platformIds.data(), nullptr);

	// Enumerate devices on a platform.
	cl_uint deviceIdCount = 0;
	clGetDeviceIDs(platformIds[0], CL_DEVICE_TYPE_ALL, 0, nullptr, &deviceIdCount);
	std::vector<cl_device_id> deviceIds(deviceIdCount);
	clGetDeviceIDs(platformIds[0], CL_DEVICE_TYPE_ALL, deviceIdCount, deviceIds.data(), nullptr);

	std::cout << "deviceIdCount " << deviceIdCount << std::endl;

	// Create device context.
	const cl_context_properties contextProperties[] = {
		CL_CONTEXT_PLATFORM,
		reinterpret_cast<cl_context_properties>(platformIds[0]),
		0, 0
	};

	cl_int error = CL_SUCCESS;
	cl_context context = clCreateContext(contextProperties, deviceIdCount,
		deviceIds.data(), nullptr, nullptr, &error);
	std::cout << "clCreateContext ec: " << error << std::endl;

	// Prepare some test data
	static const size_t testDataSize = 1 << 20;
	std::vector<float> a(testDataSize);
	std::vector<float> b(testDataSize);
	for (int i = 0; i < testDataSize; ++i)
	{
		a[i] = static_cast<float>(23 ^ i);
		b[i] = static_cast<float>(42 ^ i);
	}

	// Allocate device memory
	cl_mem aBuffer = clCreateBuffer(context,
		CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
		sizeof(float) * (testDataSize),
		a.data(), &error);
	std::cout << "clCreateBuffer ec: " << error << std::endl;

	cl_mem bBuffer = clCreateBuffer(context,
		CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
		sizeof(float) * (testDataSize),
		b.data(), &error);
	std::cout << "clCreateBuffer ec: " << error << std::endl;

	// Read in the source.
	const std::string programName = "saxpy.cl";
	std::ifstream in(programName);
	std::string programSource(
		(std::istreambuf_iterator<char>(in)),
		std::istreambuf_iterator<char>());

	// Create program from source.
	size_t lengths[] = { programSource.size() };
	const char *sources[] = { programSource.data() };
	cl_program program = clCreateProgramWithSource(context, 1, sources, lengths, &error);
	std::cout << "clCreateProgramWithSource ec: " << error << std::endl;

	// Build program & create kernel.
	error = clBuildProgram(program, deviceIdCount, deviceIds.data(),
		nullptr, nullptr, nullptr);
	std::cout << "clBuildProgram ec: " << error << std::endl;
	cl_kernel kernel = clCreateKernel(program, "SAXPY", &error);
	std::cout << "clCreateKernel ec: " << error << std::endl;

	// Create commmand queue.
	cl_command_queue queue = clCreateCommandQueue(context, deviceIds[0],
		0, &error);
	std::cout << "clCreateCommandQueue ec: " << error << std::endl;

	// Bind arguments to kernel.
	clSetKernelArg(kernel, 0, sizeof(cl_mem), &aBuffer);
	clSetKernelArg(kernel, 1, sizeof(cl_mem), &aBuffer);
	static const float two = 2.0f;
	clSetKernelArg(kernel, 2, sizeof(float), &two);

	// Tell OpenCL our work domain's dimensions and their sizes.
	const size_t globalWorkSize[] = { testDataSize, 0, 0 };
	error = clEnqueueNDRangeKernel(queue, kernel,
		1, // One dimension
		nullptr, globalWorkSize,
		nullptr, 0,
		nullptr, nullptr);
	std::cout << "clEnqueueNDRangeKernel ec: " << error << std::endl;

	// Get the results back.
	error = clEnqueueReadBuffer(queue, bBuffer, CL_TRUE, 0,
		sizeof(float) * testDataSize, b.data(), 0, nullptr, nullptr);
	std::cout << "clEnqueueReadBuffer ec: " << error << std::endl;

	clReleaseCommandQueue(queue);
	clReleaseMemObject(bBuffer);
	clReleaseMemObject(aBuffer);
	clReleaseKernel(kernel);
	clReleaseProgram(program);
	clReleaseContext(context);
}
