#include <iostream>
#include <string>
#include <vector>

#ifdef __APPLE__
	#include "OpenCL/opencl.h"
#else
	#include "CL/cl.h"
#endif

std::string GetPlatformName (cl_platform_id id)
{
	size_t size = 0;
	clGetPlatformInfo (id, CL_PLATFORM_NAME, 0, nullptr, &size);

	std::string result;
	result.resize (size);
	clGetPlatformInfo (id, CL_PLATFORM_NAME, size,
		const_cast<char*> (result.data ()), nullptr);

	return result;
}

std::string GetDeviceName (cl_device_id id)
{
	size_t size = 0;
	clGetDeviceInfo (id, CL_DEVICE_NAME, 0, nullptr, &size);

	std::string result;
	result.resize (size);
	clGetDeviceInfo (id, CL_DEVICE_NAME, size,
		const_cast<char*> (result.data ()), nullptr);

	return result;
}

void EnumeratePlatform(cl_platform_id platformId)
{
	cl_uint deviceIdCount = 0;
	clGetDeviceIDs(platformId, CL_DEVICE_TYPE_ALL, 0, nullptr, &deviceIdCount);

	std::cout << "  Device IDs count: " << deviceIdCount << std::endl;
	std::vector<cl_device_id> deviceIds(deviceIdCount);
	clGetDeviceIDs(platformId, CL_DEVICE_TYPE_ALL, deviceIdCount,
		deviceIds.data(), nullptr);
	for (cl_uint i = 0; i < deviceIdCount; ++i)
	{
		cl_device_id deviceId = deviceIds[i];
		std::cout << "  [" << i << "] Device " << deviceId << ": "
			<< GetDeviceName(deviceId) << std::endl;
	}
}

int main(int argc, char **argv)
{
	cl_uint platformIdCount = 0;
	clGetPlatformIDs(0, nullptr, &platformIdCount);
	std::cout << "Platform IDs count: " << platformIdCount << std::endl;

	std::vector<cl_platform_id> platformIds(platformIdCount);
	clGetPlatformIDs(platformIdCount, platformIds.data(), nullptr);

	for (cl_uint i = 0; i < platformIdCount; ++i)
	{
		cl_platform_id platformId = platformIds[i];
		std::cout << "[" << i << "] Platform " << platformId << ": "
			<< GetPlatformName(platformId) << std::endl;
		EnumeratePlatform(platformId);
	}
}
