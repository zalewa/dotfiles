#include <GL/glew.h>
#include <SDL.h>
#include <iostream>

static const int WIDTH = 640;
static const int HEIGHT = 480;

int main(int argc, char **argv)
{
	bool quit = false;
	int ec = 0;
	int success = 0;
	char info[512];

	// SDL stuff
	SDL_GLContext glcontext;
	SDL_Window *window = nullptr;
	SDL_Event e;

	// GL stuff
	unsigned int vbo = 0, vao = 0;
	const float vertices[] = {
		-0.5f, -0.5f, 0.0f,
		0.5f, -0.5f, 0.0f,
		0.0f,  0.5f, 0.0f
	};
	GLenum glewStatus = 0;
	unsigned int vertexShader = 0;
	const std::string vertexShaderCode = "\
#version 330 core\n\
layout (location = 0) in vec3 aPos;\n\
\n\
void main()\n\
{\n\
    gl_Position = vec4(aPos.x, aPos.y, aPos.z, 1.0);\n\
}\n\
";
	const GLchar *vsrc[] = {vertexShaderCode.c_str()};
	unsigned int fragmentShader = 0;
	const std::string fragmentShaderCode ="\
#version 330 core\n\
out vec4 FragColor;\n\
\n\
void main()\n\
{\n\
    FragColor = vec4(1.0f, 0.5f, 0.2f, 1.0f);\n\
}\n\
";
	const GLchar *fsrc[] = {fragmentShaderCode.c_str()};
	unsigned int shaderProgram = 0;

	// Init
	if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_EVENTS) != 0) {
		std::cerr << "SDL_Init fail" << std::endl;
		goto error;
	}

	// Window
	window = SDL_CreateWindow("SDL + OpenGL + GLEW test app",
		SDL_WINDOWPOS_CENTERED_DISPLAY(0),
		SDL_WINDOWPOS_CENTERED_DISPLAY(0),
		WIDTH, HEIGHT,
		SDL_WINDOW_SHOWN | SDL_WINDOW_OPENGL);
	if (!window) {
		std::cerr << "SDL_CreateWindow fail" << std::endl;
		goto error;
	}

	// OpenGL
	glcontext = SDL_GL_CreateContext(window);
	if (!glcontext) {
		std::cerr << "SDL_GL_CreateContext fail" << std::endl;
		goto error;
	}

	glewStatus = glewInit();
	if (glewStatus != GLEW_OK) {
		std::cerr << "glewInit fail: " << glewGetErrorString(glewStatus) << std::endl;
		goto error;
	}

	// Create triangle
	glGenVertexArrays(1, &vao);
	glGenBuffers(1, &vbo);
	glBindVertexArray(vao);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
	glEnableVertexAttribArray(0);

	// Create shader
	vertexShader = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vertexShader, 1, vsrc, NULL);
	glCompileShader(vertexShader);
	glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &success);
	if (!success)
	{
		glGetShaderInfoLog(vertexShader, 512, NULL, info);
		std::cerr << "compile vertexShader fail: " << info << std::endl;
		goto error;
	}

	fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fragmentShader, 1, fsrc, NULL);
	glCompileShader(fragmentShader);
	glGetShaderiv(fragmentShader, GL_COMPILE_STATUS, &success);
	if (!success)
	{
		glGetShaderInfoLog(fragmentShader, 512, NULL, info);
		std::cerr << "compile fragmentShader fail: " << info << std::endl;
		goto error;
	}

	shaderProgram = glCreateProgram();
	glAttachShader(shaderProgram, vertexShader);
	glAttachShader(shaderProgram, fragmentShader);
	glLinkProgram(shaderProgram);
	glGetProgramiv(shaderProgram, GL_LINK_STATUS, &success);
	if (!success) {
		glGetProgramInfoLog(shaderProgram, 512, NULL, info);
		std::cerr << "link shader fail: " << info << std::endl;
		goto error;
	}

	// Use shader
	glUseProgram(shaderProgram);
	glBindVertexArray(vao);

	while (!quit) {
		while(SDL_PollEvent(&e) > 0) {
			if (e.type == SDL_QUIT)
				quit = true;
		}

		glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT);
		glDrawArrays(GL_TRIANGLES, 0, 3);
		SDL_GL_SwapWindow(window);
	}

	goto cleanup;
error:
	ec = 1;
cleanup:
	if (vertexShader) {
		glDeleteShader(vertexShader);
	}
	if (fragmentShader) {
		glDeleteShader(fragmentShader);
	}
	if (glcontext) {
		SDL_GL_DeleteContext(glcontext);
	}
	if (window) {
		SDL_DestroyWindow(window);
	}
	SDL_Quit();
	return ec;
}
