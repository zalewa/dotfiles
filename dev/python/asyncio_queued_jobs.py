#!/usr/bin/env python3
import argparse
import asyncio
import logging


logger = logging.getLogger(__file__)


async def producer(queue, element, delay=None):
    if delay:
        await asyncio.sleep(delay)
    logger.info("producer: %s", element)
    await queue.put(element)


async def consumer(queue):
    logger.debug("++consumer()")
    while True:
        element = await queue.get()
        logger.info("consumer: ++%s", element)
        if element is None:
            break
        elif isinstance(element, (int, float)):
            await asyncio.sleep(element)
        logger.info("consumer: --%s", element)
    logger.debug("--consumer()")


async def amain():
    logger.debug("++amain()")
    queue = asyncio.Queue()
    await asyncio.gather(
        consumer(queue),
        producer(queue, 1, delay=2),
        producer(queue, 2, delay=1),
        producer(queue, 3),
        producer(queue, 4, delay=0.5),
        producer(queue, None, delay=4)
    )
    logger.debug("--amain()")


def main():
    argp = argparse.ArgumentParser()
    argp.add_argument("-d", "--debug", action="store_true",
                      help="debugs", default=False)
    args = argp.parse_args()

    logger.setLevel(logging.DEBUG if args.debug else logging.INFO)
    logger.addHandler(logging._StderrHandler())
    logger.debug("++main()")
    asyncio.run(amain())
    logger.debug("--main()")


if __name__ == "__main__":
    main()
