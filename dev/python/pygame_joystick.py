#!/usr/bin/env python3
from pygame import joystick
import pygame
import sys

def init():
    joystick.init()


def list_joysticks():
    for joy_idx in range(joystick.get_count()):
        joy = joystick.Joystick(joy_idx)
        print(joy.get_id(), ": ", joy.get_name())


def control(joy_id):
    JOYEVENTS = [
        pygame.JOYAXISMOTION,
        pygame.JOYBALLMOTION,
        pygame.JOYBUTTONDOWN,
        pygame.JOYBUTTONUP,
        pygame.JOYHATMOTION
    ]
    pygame.init()
    try:
        clock = pygame.time.Clock()
        done = False
        joy = joystick.Joystick(joy_id)
        joy.init()
        while not done:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    done = True
                if event.type in JOYEVENTS:
                    print(event)
            clock.tick(60)
    finally:
        pygame.quit()


def run():
    try:
        init()
        if len(sys.argv) <= 1 or sys.argv[1] == 'p':
            list_joysticks()
        else:
            control(int(sys.argv[1]))
    finally:
        joystick.quit()

run()
