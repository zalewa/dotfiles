import sys

import pygame


def run():
    MOUSEEVENTS = [
        pygame.MOUSEBUTTONDOWN,
        pygame.MOUSEBUTTONUP,
        pygame.MOUSEWHEEL,
    ]
    pygame.init()
    screen = pygame.display.set_mode((640, 480))
    try:
        clock = pygame.time.Clock()
        done = False
        while not done:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    done = True
                if event.type in MOUSEEVENTS:
                    print(event)
            if "-f" in sys.argv:
                pygame.mouse.set_pos((320, 240))
            clock.tick(60)
    finally:
        pygame.quit()


if __name__ == "__main__":
    run()
