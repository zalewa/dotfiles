#include <QApplication>
#include <QMainWindow>
#include <QVBoxLayout>
#include <QLineEdit>
#include <QComboBox>
#include <QCompleter>
#include <QDirModel>

int main(int argc, char **argv)
{
	QApplication app(argc, argv);
	QMainWindow window;
	window.setCentralWidget(new QWidget(&window));

	QVBoxLayout *layout = new QVBoxLayout();
	window.centralWidget()->setLayout(layout);

	QLineEdit *lineEdit = new QLineEdit();
	lineEdit->setCompleter(new QCompleter(new QDirModel()));
	layout->addWidget(lineEdit);

	QComboBox *comboBox = new QComboBox();
	comboBox->setEditable(true);
	
	// This block:
	// Because of QDirModel completer,
	// addItem() will cause floppy drive click if you have one.
	comboBox->setCompleter(new QCompleter(new QDirModel()));
	comboBox->addItem("something anything");
	// End of that block.

	layout->addWidget(comboBox);

	window.show();
	return app.exec();
}