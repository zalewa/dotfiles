#include <QCoreApplication>
#include <QDebug>
#include <QFile>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QString>
#include <iostream>

class Recv : public QObject
{
	Q_OBJECT

public:
	Recv(QNetworkReply *reply, QIODevice *out)
	: reply(reply), out(out)
	{
		connect(reply, SIGNAL(readyRead()), SLOT(flushChunk()));
		connect(reply, SIGNAL(finished()), SLOT(finish()));
	}

signals:
	void finished();

private:
	QNetworkReply *reply;
	QIODevice *out;

private slots:
	void flushChunk()
	{
		std::cerr << ".";
		out->write(reply->readAll());
	}

	void finish()
	{
		emit finished();
	}
};

int main(int argc, char **argv)
{
	QCoreApplication app(argc, argv);

	if (argc < 2)
	{
		qDebug() << "Usage:" << argv[0] << " <url> <target>";
		return 2;
	}
	QString url = argv[1];
	QString target = argv[2];

	QFile outFile(target);
	outFile.open(QIODevice::WriteOnly);

	QNetworkAccessManager nam;
	QNetworkRequest req;
	req.setUrl(url);

	QNetworkReply *reply = nam.get(req);
	Recv recv(reply, &outFile);
	app.connect(&recv, SIGNAL(finished()), SLOT(quit()));

	return app.exec();
}

#include "geturl.moc"
