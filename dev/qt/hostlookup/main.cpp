#include <QApplication>
#include <QDebug>
#include <QHostInfo>
#include <QLineEdit>
#include <QMainWindow>
#include <QPlainTextEdit>
#include <QPushButton>
#include <QString>
#include <QVBoxLayout>

class Handler : public QObject
{
	Q_OBJECT

public:
	Handler(QMainWindow *window)
	{
		QWidget *pane = new QWidget(window);
		QVBoxLayout *layout = new QVBoxLayout(pane);
		pane->setLayout(layout);
		window->setCentralWidget(pane);

		hostInput = new QLineEdit(window);
		this->connect(hostInput, SIGNAL(returnPressed()), SLOT(lookUp()));
		layout->addWidget(hostInput);

		infoDisplay = new QPlainTextEdit(window);
		layout->addWidget(infoDisplay);

		quitButton = new QPushButton("Quit", window);
		connect(quitButton, SIGNAL(clicked()), window, SLOT(close()));
		layout->addWidget(quitButton);
	}

public slots:
	void lookUp()
	{
		QString host = hostInput->text();
		QString msg = tr("Looking up host %1").arg(host);
		infoDisplay->appendPlainText(msg);
		qDebug() << msg;
		QHostInfo::lookupHost(host, this, SLOT(onLookedUp(QHostInfo)));
	}

	void onLookedUp(QHostInfo host)
	{
		QString msgDone = tr("Host looked up: %1").arg(host.hostName());
		infoDisplay->appendPlainText(msgDone);
		qDebug() << msgDone;
		if (host.error() != QHostInfo::NoError)
		{
			infoDisplay->appendPlainText(tr("Error: %1").arg(host.errorString()));
			return;
		}
		foreach (const QHostAddress &address, host.addresses())
		{
			infoDisplay->appendPlainText(QString("  %1").arg(address.toString()));
		}
		infoDisplay->appendPlainText("");
	}

	void windowClose()
	{
		qDebug() << "Window closed";
	}

private:
	QLineEdit *hostInput;
	QPlainTextEdit *infoDisplay;
	QPushButton *quitButton;
};


int main(int argc, char **argv)
{
	QApplication *app = new QApplication(argc, argv);
	QMainWindow *window = new QMainWindow();
	Handler *handler = new Handler(window);
	window->setAttribute(Qt::WA_DeleteOnClose);
	window->show();
	int exitcode = app->exec();

	/*
	  This 'delete' is very important!
	  Without this delete, the following scenario will get the process
	  stuck running forever:

	  1. Lookup not existing host like "lolcathost".
	  2. Press "Quit" before lookup times out.
	*/
	delete app;
	qDebug() << "returning exitcode" << exitcode;
	return exitcode;
}

#include "main.moc"
