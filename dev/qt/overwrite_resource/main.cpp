#include <QApplication>
#include <QDebug>
#include <QDir>
#include <QMainWindow>
#include <QLabel>
#include <QVBoxLayout>

QLabel *ilabel(QPixmap icon)
{
	QLabel *l = new QLabel();
	l->setPixmap(icon);
	return l;
}

int main(int argc, char **argv)
{
	QApplication app(argc, argv);
	QMainWindow window;
	window.show();

	QVBoxLayout *layout = new QVBoxLayout();
	QWidget *centralWidget = new QWidget();
	centralWidget->setLayout(layout);
	window.setCentralWidget(centralWidget);

	QPixmap iconResource(":ball");
	layout->addWidget(new QLabel("original"));
	layout->addWidget(ilabel(iconResource));

	QDir::addSearchPath("", QDir::currentPath() + "/overwrite");

	QPixmap iconOverwrite(":ballz.png");
	layout->addWidget(new QLabel("overwrite"));
	layout->addWidget(ilabel(iconOverwrite));

	QDir::addSearchPath("prephixz", QDir::currentPath() + "/overwrite");
	qDebug() << QDir::searchPaths("prephixz");
	QPixmap iconPrefixed("prephixz:ballz.png");
	qDebug() << QIcon("prephixz:ballz.png");
	layout->addWidget(new QLabel("prefixed"));
	layout->addWidget(ilabel(iconPrefixed));

	return app.exec();
}
