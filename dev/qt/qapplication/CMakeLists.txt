cmake_minimum_required(VERSION 3.5.1)
project(qapplication)

find_package(Qt5 COMPONENTS Widgets REQUIRED)

add_executable(qapplication main.cpp)
target_link_libraries(qapplication PRIVATE Qt5::Widgets)
