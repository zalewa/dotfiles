cmake_minimum_required(VERSION 3.5.1)
set(NAME qt)
project(${NAME})

set(CMAKE_CXX_STANDARD 14)
set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTOUIC ON)
set(CMAKE_INCLUDE_CURRENT_DIR ON)

find_package(Qt5Widgets REQUIRED)

set(SOURCES
	main.cpp
	)

include_directories(${CMAKE_SOURCE_DIR})

add_executable(${NAME} ${SOURCES})
target_link_libraries(${NAME} Qt5::Widgets)
