#include <QApplication>
#include <QDialog>
#include <QMainWindow>
#include <QTimer>

class Emitter : public QObject
{
	Q_OBJECT;
public:
	void doit()
	{
		emit done();
	}

signals:
	void done();
};

class Main : public QObject
{
	Q_OBJECT;
public:
	Main()
	{
	}

	void run()
	{
		Emitter emitter;
		this->connect(&emitter, SIGNAL(done()), SLOT(mySlot()));
		emitter.doit();
	}

private slots:
	void mySlot()
	{
		QDialog dialog;
		dialog.exec();
		qApp->quit();
	}
};

int main(int argc, char **argv)
{
	QApplication app(argc, argv);
	Main m;
	m.run();
	return qApp->exec();
}

#include "main.moc"
