#include <QApplication>
#include <QMainWindow>
#include <QMetaEnum>
#include <QPlainTextEdit>
#include <QStandardPaths>
#include <QVBoxLayout>

QString paths()
{
	QMetaEnum metaEnum = QMetaEnum::fromType<QStandardPaths::StandardLocation>();
	QString str;
	for (int i = 0; i <= QStandardPaths::AppConfigLocation; ++i)
	{
		str += metaEnum.valueToKey(i);
		str += ": ";
		str += QStandardPaths::standardLocations(static_cast<QStandardPaths::StandardLocation>(i)).join("; ");
		str += "\n\n";
	}
	return str;
}

int main(int argc, char **argv)
{
	QApplication app(argc, argv);
	QMainWindow window;
	window.resize(640, 480);

	window.setCentralWidget(new QWidget(&window));
	window.centralWidget()->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
	window.centralWidget()->setLayout(new QVBoxLayout());

	auto textView = new QPlainTextEdit(window.centralWidget());
	textView->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
	textView->setPlainText(paths());
	window.centralWidget()->layout()->addWidget(textView);

	window.show();
	return app.exec();
}
