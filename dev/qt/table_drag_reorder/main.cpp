#include <QApplication>
#include <QCheckBox>
#include <QDropEvent>
#include <QHeaderView>
#include <QItemDelegate>
#include <QMainWindow>
#include <QProxyStyle>
#include <QTableWidget>
#include <QTimer>
#include <QVBoxLayout>

class ReorderableTableStyle : public QProxyStyle
{
public:
	using QProxyStyle::QProxyStyle;

	void drawPrimitive(QStyle::PrimitiveElement element, const QStyleOption *option, QPainter *painter, const QWidget *widget = nullptr) const override;
};

class ReorderableTable : public QTableWidget
{
	Q_OBJECT;

	friend class ReorderableTableStyle;
public:
	ReorderableTable(QWidget *parent = nullptr);

	void dropEvent(QDropEvent *event) override;

private:
	int determineDragRow(int y) const;
};

void ReorderableTableStyle::drawPrimitive(QStyle::PrimitiveElement element, const QStyleOption *option, QPainter *painter, const QWidget *widget) const
{
	if (element == PE_IndicatorItemViewItemDrop && !option->rect.isNull() && widget)
	{
		auto *table = static_cast<const ReorderableTable*>(widget);
		QPoint cursorPos = QCursor::pos();
		QPoint tablePos = table->mapFromGlobal(cursorPos);
		tablePos.setX(tablePos.x() - table->verticalHeader()->width());
		tablePos.setY(tablePos.y() - table->horizontalHeader()->height());
		int targetRow = table->determineDragRow(tablePos.y());
		QStyleOption newOption = *option;
		if (targetRow < table->rowCount())
		{
			newOption.rect.setTop(table->rowViewportPosition(targetRow));
		}
		else
		{
			int lastRow = table->rowCount() - 1;
			if (lastRow < 0)
				return;
			int bottom = table->rowViewportPosition(lastRow) + table->rowHeight(lastRow);
			newOption.rect.setTop(bottom);
		}
		newOption.rect.setHeight(0);
		newOption.rect.setLeft(0);
		if (widget)
			newOption.rect.setRight(widget->width());
		QProxyStyle::drawPrimitive(element, &newOption, painter, widget);
	}
	else
	{
		QProxyStyle::drawPrimitive(element, option, painter, widget);
	}
}

ReorderableTable::ReorderableTable(QWidget *parent)
	: QTableWidget(parent)
{
	this->setStyle(new ReorderableTableStyle());
}

void ReorderableTable::dropEvent(QDropEvent *event)
{
	if (event->source() != this)
	{
		event->ignore();
		return;
	}
	int newRow = determineDragRow(event->pos().y());
	QList<QTableWidgetItem*> selectedItems = this->selectedItems();
	QSet<int> uniqueRows;
	for (QTableWidgetItem *item : selectedItems)
		uniqueRows.insert(item->row());
	for (int n = 0; n < uniqueRows.count(); ++n)
		this->insertRow(newRow);

	int currentOldRow = -1;
	int currentNewRow = newRow - 1;
	QList<int> deleteRows;
	for (QTableWidgetItem *selectedItem : selectedItems)
	{
		int column = selectedItem->column();
		if (selectedItem->row() != currentOldRow)
		{
			currentOldRow = selectedItem->row();
			deleteRows.append(currentOldRow);
			currentNewRow++;
		}
		QWidget *cellWidget = this->cellWidget(currentOldRow, column);
		this->takeItem(currentOldRow, column);
		this->setItem(currentNewRow, column, selectedItem);
		this->setCellWidget(currentNewRow, column, cellWidget);
	}

	std::sort(deleteRows.begin(), deleteRows.end());
	for (int i = deleteRows.count() - 1; i >= 0; --i)
		this->removeRow(deleteRows[i]);
	event->ignore();
}

int ReorderableTable::determineDragRow(int y) const
{
	int directRow = this->rowAt(y);
	if (directRow < 0)
		return this->rowCount();
	int rowPosition = this->rowViewportPosition(directRow);
	int rowHeight = this->rowHeight(directRow);
	int rowLocalY = y - rowPosition;
	return (rowLocalY < rowHeight / 2) ? directRow : directRow + 1;
}

/*
  Example program.
*/

/// Prevents drawing the dotted focus lines inside the checkbox cell.
class NoRectangleDelegate : public QItemDelegate
{
	Q_OBJECT
public:
	using QItemDelegate::QItemDelegate;

	virtual void paint(QPainter* painter, const QStyleOptionViewItem& option, const QModelIndex& index) const
	{
		if (index.column() == 2)
		{
			QStyleOptionViewItem newOption = option;
			newOption.showDecorationSelected = false;
			QItemDelegate::paint(painter, newOption, index);
		}
		else
		{
			QItemDelegate::paint(painter, option, index);
		}
	}
};

struct Row
{
	QString name;
	QString value;
	bool checked;
};

int main(int argc, char **argv)
{
	QApplication app(argc, argv);

	QWidget *central = new QWidget();
	QVBoxLayout *layout = new QVBoxLayout(central);
	central->setLayout(layout);

	QTableWidget *table = new ReorderableTable(central);
	table->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
	layout->addWidget(table);

	// Setup dragging
	table->setDragDropMode(QTableWidget::InternalMove);
	table->setDragDropOverwriteMode(false);
	table->setSelectionBehavior(QTableWidget::SelectRows);
	table->setSelectionMode(QTableWidget::ExtendedSelection);
	// Dragging configured.

	table->setColumnCount(3);
	table->setHorizontalHeaderLabels(QStringList({"name", "value", "checkbox"}));
	table->setItemDelegate(new NoRectangleDelegate(table));

	Row rows[] = {
		{"a", "1", false},
		{"b", "2", true},
		{"c", "3", false},
		{"d", "4", false},
		{"e", "5", false},
		{"f", "6", false},
		{"g", "7", true},
		{"h", "8", false},
		{"i", "9", false},
		{"j", "10", false}
	};
	for (Row row : rows) {
		int rowIdx = table->rowCount();
		table->insertRow(rowIdx);
		table->setItem(rowIdx, 0, new QTableWidgetItem(row.name));
		table->setItem(rowIdx, 1, new QTableWidgetItem(row.value));
		// The empty item needs to be here because the drag model relies
		// on it being selected. Also, drawing a QCheckBox as a cell widget
		// resulted in widgets becoming painted with a skewed offset after
		// drag operation when the viewport was scrolled.
		auto *checkbox = new QTableWidgetItem;
		checkbox->setCheckState(row.checked ? Qt::Checked : Qt::Unchecked);
		checkbox->setFlags(checkbox->flags() & ~(Qt::ItemIsEditable));
		table->setItem(rowIdx, 2, checkbox);
	}

	QMainWindow window;
	window.setCentralWidget(central);
	window.resize(800, 640);
	window.show();
	return app.exec();
}

#include "main.moc"
