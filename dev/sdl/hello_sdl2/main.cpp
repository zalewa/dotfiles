#include <iostream>
#include <string>
#include <SDL.h>

int main(int argc, char *argv[])
{
	int exitcode = 0;
	int i;
	SDL_Window *window = nullptr;
	SDL_Renderer *renderer = nullptr;
	SDL_Surface *picture = nullptr;
	SDL_Texture *texture = nullptr;
	std::string picturePath;

	if (SDL_Init(SDL_INIT_VIDEO) != 0)
	{
		std::cerr << "SDL_Init Error: " << SDL_GetError() << std::endl;
		return 1;
	}

	window = SDL_CreateWindow("Hello, SDL2!",
		100, 100, 640, 480,
		SDL_WINDOW_SHOWN);
	if (window == nullptr)
	{
		std::cerr << "SDL_CreateWindow Error: " << SDL_GetError() << std::endl;
		goto error_quit;
	}

	renderer = SDL_CreateRenderer(window, -1,
		SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
	if (renderer == nullptr)
	{
		std::cerr << "SDL_CreateRenderer Error: " << SDL_GetError() << std::endl;
		goto error_quit;
	}

	std::cerr << "base path " << SDL_GetBasePath() << std::endl;

	picturePath = SDL_GetBasePath() + std::string("hello.bmp");
	picture = SDL_LoadBMP(picturePath.c_str());
	if (picture == nullptr)
	{
		std::cerr << "SDL_LoadBMP Error: " << SDL_GetError() << std::endl;
		goto error_quit;
	}

	texture = SDL_CreateTextureFromSurface(renderer, picture);
	SDL_FreeSurface(picture);
	picture = nullptr;
	if (texture == nullptr)
	{
		std::cerr << "SDL_CreateTextureFromSurface Error: " << SDL_GetError() << std::endl;
		goto error_quit;
	}

	// Run
	for (i = 0; i < 3; ++i)
	{
		SDL_RenderClear(renderer);
		SDL_RenderCopy(renderer, texture, NULL, NULL);
		SDL_RenderPresent(renderer);
		SDL_Delay(1000);
	}

	goto quit;

error_quit:
	exitcode = 1;

quit:
	std::cerr << "Quitting..." << std::endl;
	if (texture != nullptr)
		SDL_DestroyTexture(texture);
	if (picture != nullptr)
		SDL_FreeSurface(picture);
	if (renderer != nullptr)
		SDL_DestroyRenderer(renderer);
	if (window != nullptr)
		SDL_DestroyWindow(window);
	SDL_Quit();
	return exitcode;
}
