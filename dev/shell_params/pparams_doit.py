#!/usr/bin/env python3
import platform
import sys
import subprocess

args = sys.argv[1:]
if sys.argv[1] == "-p":
    # predefined
    args = [
        "1",
        "2",
        " ",
        " 4",
        "   ",
        "   8",
        "\t4",
        "hash",
        "ppa*",
        "alice had a cat",
        'arg"quoted',
        'arg " quo " ted',
        'escaped arg \\" quo \\" ted'
    ]


if platform.system() == "Windows":
    cmd = []
    scripts = [".\\pparams_star.bat"]
else:
    cmd = ["bash"]
    scripts = ["./pparams_star.sh", "./pparams_star_quote.sh", "./pparams_at.sh", "./pparams_at_quote.sh"]

for script in scripts:
    print(script, flush=True)
    script_cmd = cmd + [script]
    script_cmd.extend(args)
    p = subprocess.Popen(script_cmd)
    p.wait()
    print("")
