cmake_minimum_required(VERSION 3.5.1)
set(NAME gamma_winapi)
project(${NAME})

set(CMAKE_CXX_STANDARD 14)
set(CMAKE_INCLUDE_CURRENT_DIR ON)

include_directories(${CMAKE_SOURCE_DIR})

add_executable(${NAME} gamma.c)
