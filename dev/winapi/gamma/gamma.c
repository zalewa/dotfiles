#include <Windows.h>
#include <stdio.h>

static const int N_VALUES = 256 * 3;

HWND hwnd = NULL;

static void print_display_devices()
{
	DISPLAY_DEVICE display;
	display.cb = sizeof(DISPLAY_DEVICE);
	int i = 0;
	while (1) {
		int ok = EnumDisplayDevicesA(NULL, i, &display, 0);
		if (ok == 0)
			break;
		printf("Device: %s\n"
			"\tDeviceString: %s\n"
			"\tStateFlags: %lx\n"
			"\tDeviceID: %s\n"
			"\tDeviceKey: %s\n",
			display.DeviceName, display.DeviceString, display.StateFlags,
			display.DeviceID, display.DeviceKey);
		printf("\tAttached to desktop: %s\n",
			(display.StateFlags & DISPLAY_DEVICE_ATTACHED_TO_DESKTOP) ? "Y" : "N");
		fflush(stdout);
		++i;
	}
}

static int num_displays()
{
	DISPLAY_DEVICE display;
	int i, ok = 1;

	display.cb = sizeof(DISPLAY_DEVICE);
	for (i = 0; ok != 0 ; ++i)
		ok = EnumDisplayDevicesA(NULL, i, &display, 0);
	return i;
}

static int has_attached_screen(int idx)
{
	DISPLAY_DEVICE display;
	display.cb = sizeof(DISPLAY_DEVICE);
	int ok = EnumDisplayDevicesA(NULL, idx, &display, 0);
	return ok != 0 && display.StateFlags & DISPLAY_DEVICE_ATTACHED_TO_DESKTOP;
}

static BOOL set_gamma(HDC hdc, float factor)
{
	WORD values[N_VALUES];
	WORD *r = &values[0];
	WORD *g = &values[256];
	WORD *b = &values[256 * 2];

	for (int i = 0; i < 256; ++i) {
		/* This ignores the BT.709 / REC.601 color luminance conversions
		   and simply assumes that all color components are equal to the
		   human eye. These assumptions are WRONG but this tool is made
		   to be as stupid as possible, not color-correct.
		*/
		int color = ((int)(i * factor)) << 8;
		color = color > 0xffff ? 0xffff : color;
		*(r + i) = color;
		*(g + i) = color;
		*(b + i) = color;
	}
	return SetDeviceGammaRamp(hdc, (void *)values);
}

// Works only if you have single screen.
static void example1_GetDC()
{
	HDC hdc = GetWindowDC(hwnd);

	printf("example1_GetDC()\n");
	printf("hwnd = %p, hdc() => %p\n", hwnd, hdc);
	BOOL result = set_gamma(hdc, 1.5f);
	printf("#1 SetDeviceGammaRamp() => %s\n", result == TRUE ? "TRUE" : "FALSE");
	fflush(stdout);

	if (result) {
		Sleep(1000);
		result = set_gamma(hdc, 1.0f);
		printf("#2 SetDeviceGammaRamp() => %s\n", result == TRUE ? "TRUE" : "FALSE");
		fflush(stdout);
	}

	ReleaseDC(hwnd, hdc);
	printf("~example1_GetDC()\n");
	fflush(stdout);
}

// Works in any case
static void example2_CreateDC()
{
	const int num_displays_ = num_displays();
	int screen_idx;

	printf("example2_CreateDC()\n");
	fflush(stdout);

	// Create HDCs for each attached screen.
	HDC *hdcs = malloc(num_displays_ * sizeof(HDC));
	for (screen_idx = 0; screen_idx < num_displays_; ++screen_idx) {
		if (has_attached_screen(screen_idx)) {
			DISPLAY_DEVICE display;
			display.cb = sizeof(DISPLAY_DEVICE);
			EnumDisplayDevicesA(NULL, screen_idx, &display, 0);
			hdcs[screen_idx] = CreateDC(NULL, display.DeviceName, NULL, 0);
			printf("hdc(%d) => %p\n", screen_idx, hdcs[screen_idx]);
		} else {
			hdcs[screen_idx] = NULL;
		}
	}
	fflush(stdout);

	// Make all screen brighter
	for (screen_idx = 0; screen_idx < num_displays_; ++screen_idx) {
		if (hdcs[screen_idx]) {
			BOOL result = set_gamma(hdcs[screen_idx], 1.5f);
			printf("#1 SetDeviceGammaRamp(%d) => %s\n", screen_idx,
				result == TRUE ? "TRUE" : "FALSE");
		}
	}
	fflush(stdout);
	Sleep(1000);

	// Return all screens to normal
	for (screen_idx = 0; screen_idx < num_displays_; ++screen_idx) {
		if (hdcs[screen_idx]) {
			BOOL result = set_gamma(hdcs[screen_idx], 1.0f);
			printf("#2 SetDeviceGammaRamp(%d) => %s\n", screen_idx,
				result == TRUE ? "TRUE" : "FALSE");
		}
	}
	fflush(stdout);

	// Release HDCs
	for (screen_idx = 0; screen_idx < num_displays_; ++screen_idx) {
		if (hdcs[screen_idx]) {
			DeleteDC(hdcs[screen_idx]);
		}
	}
	free(hdcs);
	printf("~example2_CreateDC()\n");
	fflush(stdout);
}

#define ID_GETDC_BUTTON 101
#define ID_CREATEDC_BUTTON 102
#define ID_DEBUGS_BUTTON 103

static LRESULT CALLBACK WindowProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	switch (uMsg) {
	case WM_COMMAND:
		switch (wParam) {
		case ID_GETDC_BUTTON:
			example1_GetDC(); // <-- doesn't work with multiple screens
			break;
		case ID_CREATEDC_BUTTON:
			example2_CreateDC();
			break;
		case ID_DEBUGS_BUTTON:
			print_display_devices();
			fflush(stdout);
			break;
		}
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	case WM_QUIT:
		break;
	default:
		return DefWindowProc(hWnd, uMsg, wParam, lParam);
	}
	return 0L;
}

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance,
	LPSTR lpCmdLine, int nCmdShow)
{
	// Create main window.
	WNDCLASS wndclass;
	memset(&wndclass, 0, sizeof(wndclass));
	wndclass.style = CS_HREDRAW | CS_VREDRAW;
	wndclass.hInstance = hInstance;
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = GetSysColorBrush(COLOR_3DFACE);
	wndclass.lpfnWndProc = WindowProc;
	wndclass.lpszClassName = "Gamma Window";

	if (!RegisterClass(&wndclass)) {
		fprintf(stderr, "cannot register wndclass\n");
		return 1;
	}

	int flags = WS_OVERLAPPEDWINDOW;
	hwnd = CreateWindow(wndclass.lpszClassName,
		"Gamma Window",
		flags,
		CW_USEDEFAULT, CW_USEDEFAULT,
		640, 480,
		NULL, NULL, hInstance, NULL);
	if (!hwnd) {
		fprintf(stderr, "cannot create window\n");
		return 1;
	}

	CreateWindow("BUTTON", "GetDC Gamma (single screen only)",
		WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON,
		10, 10,
		270, 44,
		hwnd, (HMENU) ID_GETDC_BUTTON, hInstance, NULL);
	CreateWindow("BUTTON", "CreateDC Gamma (multi-screen)",
		WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON,
		10, 60,
		270, 44,
		hwnd, (HMENU) ID_CREATEDC_BUTTON, hInstance, NULL);
	CreateWindow("BUTTON", "Debugs (stdout)",
		WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON,
		10, 110,
		270, 44,
		hwnd, (HMENU) ID_DEBUGS_BUTTON, hInstance, NULL);

	ShowWindow(hwnd, SW_SHOW);

	MSG msg;
	while (GetMessage(&msg, NULL, 0, 0)) {
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	return 0;
}
