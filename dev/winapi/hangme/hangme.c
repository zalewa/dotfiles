#include <stdio.h>
#include <stdlib.h>
#include <windows.h>

#define ID_HANGME_BUTTON 101
#define ID_HANGSOME_BUTTON 102
#define ID_SEGMENTATIONFAULT_BUTTON 103

void NeverendingStory()
{
	for(;;);
}

void HangSome()
{
	Sleep(10000);
}

void SegmentationFault()
{
	char *c = 0;
	*c = 5;
}

LRESULT CALLBACK WindowProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	switch (uMsg) {
	case WM_COMMAND:
		switch (wParam) {
		case ID_HANGME_BUTTON:
			NeverendingStory();
			break;
		case ID_HANGSOME_BUTTON:
			HangSome();
			break;
		case ID_SEGMENTATIONFAULT_BUTTON:
			SegmentationFault();
			break;
		}
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	case WM_QUIT:
		break;
	default:
		return DefWindowProc(hWnd, uMsg, wParam, lParam);
	}
	return 0L;
}

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance,
	LPSTR lpCmdLine, int nCmdShow)
{
	/* Register the main window */
	WNDCLASS wndclass;
	memset(&wndclass, 0, sizeof(wndclass));
	wndclass.style = CS_HREDRAW | CS_VREDRAW;
	wndclass.hInstance = hInstance;
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);

	/* hbrBackground causes the window's background to be
	   redrawn properly when window is resized */
	wndclass.hbrBackground = GetSysColorBrush(COLOR_3DFACE);

	wndclass.lpfnWndProc = WindowProc;
	wndclass.lpszClassName = "WellHungWindow";

	if (!RegisterClass(&wndclass)) {
		fprintf(stderr, "cannot register wndclass\n");
		return 1;
	}

	int flags = WS_OVERLAPPEDWINDOW;
	HWND hWnd = CreateWindow(wndclass.lpszClassName,
		"Hang me!",
		flags,
		CW_USEDEFAULT, CW_USEDEFAULT,
		210, 204,
		NULL, NULL, hInstance, NULL);
	if (!hWnd) {
		fprintf(stderr, "cannot create window\n");
		return 1;
	}

	/* Create and add buttons */
	CreateWindow("BUTTON", "Hang me!",
		WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON,
		10, 10,
		170, 44,
		hWnd, (HMENU) ID_HANGME_BUTTON, hInstance, NULL);
	CreateWindow("BUTTON", "Hang for 10 seconds!",
		WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON,
		10, 60,
		170, 44,
		hWnd, (HMENU) ID_HANGSOME_BUTTON, hInstance, NULL);
	CreateWindow("BUTTON", "Segmentation Fault",
		WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON,
		10, 110,
		170, 44,
		hWnd, (HMENU) ID_SEGMENTATIONFAULT_BUTTON, hInstance, NULL);

	ShowWindow(hWnd, SW_SHOW);

	MSG msg;
	while (GetMessage(&msg, NULL, 0, 0)) {
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	return 0;
}
