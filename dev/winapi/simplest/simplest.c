#include <stdio.h>
#include <stdlib.h>
#include <windows.h>


LRESULT CALLBACK WindowProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	switch (uMsg) {
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	case WM_QUIT:
		break;
	default:
		return DefWindowProc(hWnd, uMsg, wParam, lParam);
	}
	return 0L;
}

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance,
	LPSTR lpCmdLine, int nCmdShow)
{
	WNDCLASS wndclass;
	memset(&wndclass, 0, sizeof(wndclass));
	wndclass.style = CS_HREDRAW | CS_VREDRAW;
	wndclass.hInstance = hInstance;
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);

	/* hbrBackground causes the window's background to be
	   redrawn properly when window is resized */
	wndclass.hbrBackground = GetSysColorBrush(COLOR_3DFACE);

	wndclass.lpfnWndProc = WindowProc;
	wndclass.lpszClassName = "SimplestWindow";

	if (!RegisterClass(&wndclass)) {
		fprintf(stderr, "cannot register wndclass\n");
		return 1;
	}

	int flags = WS_OVERLAPPEDWINDOW;
	HWND hWnd = CreateWindow(wndclass.lpszClassName,
		"Simplest Window",
		flags,
		CW_USEDEFAULT, CW_USEDEFAULT,
		640, 480,
		NULL, NULL, hInstance, NULL);
	if (!hWnd) {
		fprintf(stderr, "cannot create window\n");
		return 1;
	}

	ShowWindow(hWnd, SW_SHOW);

	MSG msg;
	while (GetMessage(&msg, NULL, 0, 0)) {
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	return 0;
}
