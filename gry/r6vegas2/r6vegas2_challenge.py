#!/usr/bin/python
"""
Tom Clancy's Rainbow Six Vegas 2 broadcast reroute
to fix Windows 8 UDP broadcast problems.
"""

import socket
import time
import traceback
from threading import Thread

BROADCAST_LISTEN_ADDR = '0.0.0.0' # Probably no reason to change.
BROADCAST_LISTEN_PORT = 45000 # Must be 45000 as this is R6Vegas2 broadcast port.
TARGET_ADDR = '127.0.0.1' # Change this (may be localhost).
TARGET_BROADCAST_PORT = 50000 # Change this.


_server_reply_buffer = ""


def timestamp():
    return time.strftime("[%H:%M:%S]", time.localtime())


def hexify(buffer, colwidth=16, indent=0):
    hexed = ""
    column = 0
    for c in buffer:
        hexed += "\\x{0:02x}".format(ord(c))
        column += 1
        if colwidth and column >= colwidth:
            hexed += "\n" + (" " * indent)
            column = 0
    return (" " * indent) + hexed.strip()


def broadcast_response(challenge):
    # Copy buffer reference to avoid parallel write-read operations
    # between threads.
    server_state = _server_reply_buffer
    stamp = challenge[1:-1]
    server_state_meaningful = server_state[(len(stamp) + 1):]
    if not server_state_meaningful:
        return ""
    return '\x36' + stamp + server_state_meaningful


def broadcast_response_debug(challenge):
    """Debug stuff for reference."""
    stamp = challenge[1:-1]
    reply = '\x36' + stamp + '\x66\x53\x9f' \
        '\x87\x4b\x7d\xb2\x21\x2b\x35\x85\xc6\x82\xf7\x24\x5d\xb7\x95\x40' \
        '\xfd\x8a\xb2\xc7\x6c\x80\x51\x01\x1a\x04\x18\x80\x51\x71\x02\x04' \
        '\x18\x00\xfe\x01\xfe\x01\x00\x00\xfe\x01\xfe\x01\x00\x02\xc2\xcc' \
        '\xc4\x70\x64\xca\x64\x68\x60\xc4\x68\xca\x72\xc4\x70\xc2\x6e\x60' \
        '\xca\xc6\xc8\xcc\xca\x62\x62\x70\x70\xc6\x72\xc6\x70\x72\x00\xa0' \
        '\x98\x82\xb2\x8a\xa4\x62\x00\x08\x00\x02\x00\x00\x00\x00\x00\x00' \
        '\x00\x00\x00\x08\x00\x00\x00\x00\x00\x00\x00\x90\xb1\xcc\xce\xfe' \
        '\xff\xff\xff\x03\x00\x00\x00\x00\x00\x00\x00\x80\x51\x01\x1a\x96' \
        '\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00' \
        '\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00' \
        '\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x02' \
        '\x00\x00\x00\x08\x00\x00\x00\x00\x00\x00\x00\x08\x00\x00\x00\x02' \
        '\x00\x00\x00\x02\x00\x00\x00\xca\x00\x00\x00\x00\x00\x00\x00\xfe' \
        '\xff\xff\xff\x01\x00\x00\x00\x00\x00\x00\x00\x02\x00\x00\x00\x02' \
        '\x00\x04\x00\x0a\x00\x00\x00\x02\x00\x00\x00\x00\x00\x00\x00\x00' \
        '\x00\x00\x00\x10\x00\x00\x00\xa0\x98\x82\xb2\x8a\xa4\x62\x00\x00' \
        '\x00\x00\x00\x00\x00\x00\x00\x00\x00'
    return reply


def broadcast_response_static():
    """Debug stuff."""
    return '\x36\x24\x5a\x2f\x84\x8a\x95\x0b\x7c\x84\x2c\x61\x41\x66\x53\x9f' \
        '\x87\x4b\x7d\xb2\x21\x2b\x35\x85\xc6\x82\xf7\x24\x5d\xb7\x95\x40' \
        '\xfd\x8a\xb2\xc7\x6c\x80\x51\x01\x1a\x04\x18\x80\x51\x71\x02\x04' \
        '\x18\x00\xfe\x01\xfe\x01\x00\x00\xfe\x01\xfe\x01\x00\x02\xc2\xcc' \
        '\xc4\x70\x64\xca\x64\x68\x60\xc4\x68\xca\x72\xc4\x70\xc2\x6e\x60' \
        '\xca\xc6\xc8\xcc\xca\x62\x62\x70\x70\xc6\x72\xc6\x70\x72\x00\xa0' \
        '\x98\x82\xb2\x8a\xa4\x62\x00\x08\x00\x02\x00\x00\x00\x00\x00\x00' \
        '\x00\x00\x00\x08\x00\x00\x00\x00\x00\x00\x00\x90\xb1\xcc\xce\xfe' \
        '\xff\xff\xff\x03\x00\x00\x00\x00\x00\x00\x00\x80\x51\x01\x1a\x96' \
        '\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00' \
        '\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00' \
        '\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x02' \
        '\x00\x00\x00\x08\x00\x00\x00\x00\x00\x00\x00\x08\x00\x00\x00\x02' \
        '\x00\x00\x00\x02\x00\x00\x00\xca\x00\x00\x00\x00\x00\x00\x00\xfe' \
        '\xff\xff\xff\x01\x00\x00\x00\x00\x00\x00\x00\x02\x00\x00\x00\x02' \
        '\x00\x04\x00\x0a\x00\x00\x00\x02\x00\x00\x00\x00\x00\x00\x00\x00' \
        '\x00\x00\x00\x10\x00\x00\x00\xa0\x98\x82\xb2\x8a\xa4\x62\x00\x00' \
        '\x00\x00\x00\x00\x00\x00\x00\x00\x00'


def challenge():
    return "\x34\x24\x5a\x2f\x84\x8a\x95\x0b\x7c\x84\x2c\x61\x41\x00"


def send_challenge_to_target(target):
    print "{0} sending challenge to target...".format(timestamp())
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.settimeout(5)
    try:
        s.sendto(challenge(), target)
        reply, addr = s.recvfrom(1024)
        print "{0} reply: {1} (size={2})\n{3}".format(
            timestamp(), addr, len(reply), hexify(reply, colwidth=16, indent=4))
        return reply
    finally:
        s.close()


def bind_socket_to_broadcast():
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    s.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
    s.bind((BROADCAST_LISTEN_ADDR, BROADCAST_LISTEN_PORT))
    return s


def challenger_loop():
    global _server_reply_buffer
    while True:
        try:
            _server_reply_buffer = send_challenge_to_target((TARGET_ADDR, TARGET_BROADCAST_PORT))
        except Exception:
            traceback.print_exc()
        time.sleep(5)


def broadcast_listen_loop():
    s = bind_socket_to_broadcast()
    while True:
        try:
            message, address = s.recvfrom(10240)
            print "{0} got broadcast message from: {1} (size={2}):\n{3}".format(
                timestamp(), address, len(message), hexify(message, colwidth=16, indent=4))
            reply = broadcast_response(message)
            if reply:
                s.sendto(reply, address)
                print "{0} replied to broadcast: {1}, reply size={2}".format(
                    timestamp(), address, len(reply))
            else:
                print "{0} can't reply as there's no server data".format(timestamp())
        except Exception:
            traceback.print_exc()
            time.sleep(1)


class Challenger(Thread):
    def __init__(self):
        Thread.__init__(self)
        self.daemon = True

    def run(self):
        challenger_loop()


class BroadcastListener(Thread):
    def __init__(self):
        Thread.__init__(self)
        self.daemon = True

    def run(self):
        broadcast_listen_loop()


challenger = Challenger()
challenger.start()
broadcast_listener = BroadcastListener()
broadcast_listener.start()

print "Started. Press ENTER to quit ..."
n = raw_input()
